#!/bin/bash
cd src/a3 ; ant clean tests.all ; cd -
cd src/jndi2 ; ant clean tests.all ; cd -
cd src/jms ; ant clean tests.all ; cd -
cd src/jakarta ; ant clean tests.all ; cd -
cd src/joram ; ant clean tests.all ; cd -
grep FAILED src/*/report.txt
