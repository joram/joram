/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C) 2002 - 2007 ScalAgent Distributed Technologies
 * Copyright (C) 2002 INRIA
 * Contact: joram-team@objectweb.org
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): Jeff Mesnil (Inria)
 * Contributor(s): Nicolas Tachker (ScalAgent D.T.)
 */

package jakarta.providers.admin;

import java.net.ConnectException;

import jakarta.jms.ConnectionFactory;
import jakarta.jms.JMSException;
import jakarta.jms.QueueConnectionFactory;
import jakarta.jms.TopicConnectionFactory;

import jakarta.admin.Admin;

import org.ow2.joram.jakarta.jms.Queue;
import org.ow2.joram.jakarta.jms.Topic;
import org.ow2.joram.jakarta.jms.admin.AdminException;
import org.ow2.joram.jakarta.jms.admin.AdminModule;
import org.ow2.joram.jakarta.jms.admin.User;
import org.ow2.joram.jakarta.jms.tcp.TcpConnectionFactory;

public class JoramAdmin implements Admin {
  private String name = "JORAM";

  public JoramAdmin() {
    try {
      AdminModule.connect("root", "root", 30);
      User.create("anonymous", "anonymous");
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public String getName() {
    return name;
  }

  public ConnectionFactory createConnectionFactory(String name) throws ConnectException {
    return TcpConnectionFactory.create();
  }

  public QueueConnectionFactory createQueueConnectionFactory(String name) throws ConnectException {
    return TcpConnectionFactory.create();
  }

  public TopicConnectionFactory createTopicConnectionFactory(String name) throws ConnectException {
    return TcpConnectionFactory.create();
  }

  public Queue createQueue(String name) throws ConnectException, AdminException {
    Queue queue = null;
    queue = Queue.create(name);
    queue.setFreeWriting();
    queue.setFreeReading();
    return queue;
  }

  public Topic createTopic(String name) throws ConnectException, AdminException {
    Topic topic = Topic.create(name);
    topic.setFreeWriting();
    topic.setFreeReading();
    return topic;
  }

  public void deleteQueue(jakarta.jms.Destination queue) throws ConnectException, AdminException, JMSException {
    ((org.ow2.joram.jakarta.jms.Destination)queue).delete();
  }

  public void deleteTopic(jakarta.jms.Destination topic) throws ConnectException, AdminException, JMSException {
    ((org.ow2.joram.jakarta.jms.Destination)topic).delete();
  }

  public void deleteConnectionFactory(String name) {
  }

  public void deleteTopicConnectionFactory(String name) {
  }

  public void deleteQueueConnectionFactory(String name) {
  }

  public void disconnect() {
    AdminModule.disconnect();
  }
}
