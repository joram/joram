/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C) 2013 - 2025 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): ScalAgent D.T.
 * Contributor(s): 
 */
package jndi2.base;

import java.util.Hashtable;
import java.util.Random;

import javax.naming.Context;
import javax.naming.InitialContext;

import fr.dyade.aaa.agent.AgentServer;
import framework.TestCase;

/**
 * Measures JNDI lookup and bind costs with multiples contexts.
 */

public class Perf2 extends TestCase {
  public final static int NbContext = 100;
  public final static int NbRegisteredObject = 10000;

  public static void main(String[] args) {
    new Perf2().run();
  }
  
  static void startServer() throws Exception {
    AgentServer.init((short) 0, "./s0", null);
    AgentServer.start();

    Thread.sleep(2000L);
  }

  public void run() {
    try {
      if (Boolean.getBoolean("fr.dyade.aaa.jndi2.ENCRYPT_CONFIGURATION"))
        System.out.println("JNDI storage encrypted.");
      startServer();
      Thread.sleep(1000);

      Hashtable properties = new Hashtable();
      properties.put("java.naming.factory.initial", "fr.dyade.aaa.jndi2.client.NamingContextFactory");
      properties.put("java.naming.factory.host", "localhost");
      properties.put("java.naming.factory.port", "16600");
      Context ctx = new InitialContext(properties);

      long start, end;
      
      Exception excep = null;
      try {
        start = System.nanoTime();
        for (int i=0; i<NbContext; i++) {
//          System.out.println("/context" + i);
          ctx.createSubcontext("/context" + i);
          for (int j=0; j<(NbRegisteredObject/NbContext); j++) {
//            System.out.println("/context" + i + "/object" + j);
            ctx.bind("/context" + i + "/object" + j, "hello#" + j);
          }
        }
        end = System.nanoTime();
        System.out.println("binding (ms) = " + ((end-start)/1000000L));
      } catch (Exception exc) {
        excep = exc;
        exc.printStackTrace();
      }
      assertNull(excep);
      if (excep != null) throw excep;
      
      Random rand = new Random();
      try {
        start = System.nanoTime();
        for (int i=0; i<100000; i++) {
          int index1 = rand.nextInt(NbContext);
          int index2 = rand.nextInt(NbRegisteredObject/NbContext);
          String hello = (String) ctx.lookup("/context" + index1 + "/object" + index2);
          assertTrue("Bind-lookup failure: " + index1 + ", " + index2 + " => " + hello, hello.equals("hello#" + index2));
        }
        end = System.nanoTime();
        System.out.println("lookup (ms) = " + ((end-start)/1000000L));
      } catch (Exception exc) {
        excep = exc;
        exc.printStackTrace();
      }
      assertNull(excep);
      if (excep != null) throw excep;
      
    } catch (Exception exc) {
      exc.printStackTrace();
      error(exc);
    } finally {
      AgentServer.stop();
      endTest();
    }
  }

}
