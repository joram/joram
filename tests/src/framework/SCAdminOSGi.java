/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C) 2009 - 2024 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): ScalAgent Distributed Technologies
 * Contributor(s):
 */
package framework;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.objectweb.util.monolog.api.BasicLevel;

import fr.dyade.aaa.agent.osgi.Activator;

public class SCAdminOSGi extends SCBaseAdmin {
  // use stop 0 to shutdown ! (available in felix and gogo)
  private static byte [] halt = "stop 0\n".getBytes();
  
  protected byte[] getHaltCommand() {
    return halt;
  }
    
  public void startAgentServer(short sid, String[] jvmargs,
                               PrintStream out, PrintStream err) throws Exception {
    logmon.log(BasicLevel.DEBUG, "SCAdmin: run AgentServer#" + sid);

    Server server = (Server) launchedServers.get(Short.valueOf(sid));

    if (server != null) {
      try {
        int exitValue = server.process.exitValue();
        if (logmon.isLoggable(BasicLevel.DEBUG)) {
          logmon.log(BasicLevel.DEBUG, "SCAdmin: AgentServer#" + sid + " -> " + exitValue);
        }
      } catch (IllegalThreadStateException exc) {
        if (logmon.isLoggable(BasicLevel.WARN)) {
          logmon.log(BasicLevel.WARN, "SCAdmin: AgentServer#" + sid + " already running.");
        }
        throw new IllegalStateException("AgentServer#" + sid + " already running.");
      }
    }

    // Find felix jar and put it on the classpath
    File felixbin = new File(System.getProperty("felix.dir") + "/felix.jar");
    if (!felixbin.exists()) {
      throw new Exception("Felix framework not found.");
    }
    
    Properties a3props = new Properties();
    try {
      FileInputStream fis = new FileInputStream("a3server.props");
      a3props.load(fis);
    } catch (IOException exc) {
      logmon.log(BasicLevel.WARN, "SCAdmin: AgentServer#" + sid + " cannot load a3server.props.");
      a3props = null;
    }
    List<String> argv = new ArrayList<String>();
    if (a3props != null) {
      for (Object key : a3props.keySet())
        argv.add("-D" + (String)key + "=" + a3props.getProperty((String) key));
    }    
    if (jvmargs != null) {
      for (int i = 0; i < jvmargs.length; i++)
        argv.add(jvmargs[i]);
    }

    // Choose a random telnet port if unspecified
    int port = Integer.getInteger("osgi.shell.telnet.port", getFreePort());
    if (logmon.isLoggable(BasicLevel.DEBUG)) {
      logmon.log(BasicLevel.DEBUG, "SCAdmin: AgentServer#" + sid + " telnet port: " + port);
    }

    // Get felix configuration file.
    URI configFile = new URI(System.getProperty("felix.config.properties", "file:config.properties"));
    argv.add("-Dfelix.config.properties=" + configFile);

    // Assign AgentServer properties for server id, storage directory and cluster id.
    argv.add("-Dorg.osgi.framework.storage=" + 's' + sid + "/felix-cache");
    argv.add("-Dosgi.shell.telnet.port=" + port);
    argv.add("-D" + Activator.AGENT_SERVER_ID_PROPERTY + '=' + sid);
    argv.add("-D" + Activator.AGENT_SERVER_STORAGE_PROPERTY + "=s" + sid);
//    argv.add("-XX:+UnlockDiagnosticVMOptions");
//    argv.add("-XX:+UnsyncloadClass");
    argv.add("-Dgosh.args=--nointeractive"); // need with gogo

    if (logmon.isLoggable(BasicLevel.DEBUG)) {
      logmon.log(BasicLevel.DEBUG, "SCAdmin" + ": launches AgentServer#" + sid + " with: " + argv);
    }
    
    if (logserver.isLoggable(BasicLevel.DEBUG)) {
      File targetDir = new File(".");
      if (out == null)
        out = new PrintStream(File.createTempFile("stdout", ".log", targetDir));
      if (err == null)
        err = new PrintStream(File.createTempFile("stderr", ".log", targetDir));
    }
    
    Process p = BaseTestCase.startProcess(
        "org.apache.felix.main.Main",
        "." + File.pathSeparatorChar + felixbin.getAbsolutePath(),
        (String[]) argv.toArray(new String[argv.size()]),
        null, null,
        out, err);
    
    launchedServers.put(Short.valueOf(sid), new Server(port, p));
  }
}