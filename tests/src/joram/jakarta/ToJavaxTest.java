/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C)  2023 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): ScalAgent Distributed Technologies
 * Contributor(s): Badolle Fabien (ScalAgent D.T.)
 */
package joram.jakarta;

import java.util.Enumeration;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.RefAddr;
import javax.naming.Reference;
import javax.naming.StringRefAddr;

import framework.TestCase;

/**
 * This test checks the conversion of administered objects created from the Jakarta/JMS API to
 * be used with the Javax/JMS API.
 */
public class ToJavaxTest extends TestCase {

  public static void main(String[] args) {
    new ToJavaxTest().run();
  }

  public void run() {
    try {
      startAgentServer((short)0, new String[]{"-DTransaction=fr.dyade.aaa.util.NullTransaction"});
      Thread.sleep(4000);

      createJakartaJMSObjects();
      
      Context ctx = new InitialContext();

      javax.jms.ConnectionFactory cf = (javax.jms.ConnectionFactory) toJavax(ctx.lookup("cf"));
      assertNotNull(cf);
      System.out.println(((org.objectweb.joram.client.jms.admin.AbstractConnectionFactory) cf).getParameters());
      javax.jms.Queue queue = (javax.jms.Queue) toJavax(ctx.lookup("queue"));
      System.out.println("queue: " + queue);
      assertNotNull(queue);
      javax.jms.Topic topic = (javax.jms.Topic) toJavax(ctx.lookup("topic"));
      System.out.println("topic: " + topic);
      assertNotNull(topic);

      ctx.close();
      
      javax.jms.Connection connection = cf.createConnection("anonymous", "anonymous");
      javax.jms.Session session = connection.createSession(false, javax.jms.Session.AUTO_ACKNOWLEDGE);
      
      javax.jms.MessageConsumer consumer = session.createConsumer(queue);
      javax.jms.MessageProducer producer = session.createProducer(queue);
      
      connection.start();
      
      javax.jms.TextMessage msg1 = session.createTextMessage("adminTest1_msg#1");
      msg1.setJMSDestination(queue);
      msg1.setJMSReplyTo(topic);
      producer.send(msg1);
      
      javax.jms.TextMessage msg2 = (javax.jms.TextMessage) consumer.receive();
      assertNotNull(msg2);
      assertTrue("adminTest1_msg#1".equals(msg2.getText()));
      javax.jms.Destination from = (org.objectweb.joram.client.jms.Queue) msg2.getJMSDestination();
      assertNotNull(from);
      assertTrue(from.equals(queue));
      javax.jms.Destination replyTo = (org.objectweb.joram.client.jms.Topic) msg2.getJMSReplyTo();
      assertNotNull(replyTo);
      assertTrue(replyTo.equals(topic));
      
      System.out.println(msg2 + " from " + queue + ", replyto=" + replyTo);

      connection.close();
    } catch (Throwable exc) {
      exc.printStackTrace();
      error(exc);
    } finally {
      stopAgentServer((short)0);
      endTest();     
    }
  }
  
  private void createJakartaJMSObjects() throws Exception {
    Context ctx = new InitialContext();

    jakarta.jms.ConnectionFactory cf = org.ow2.joram.jakarta.jms.tcp.TcpConnectionFactory.create("localhost", 2560);
    ((org.ow2.joram.jakarta.jms.tcp.TcpConnectionFactory) cf).getParameters().connectingTimer = 60;
    ctx.bind("cf", cf);
    org.ow2.joram.jakarta.jms.admin.AdminModule.connect(cf, "root", "root");
    
    org.ow2.joram.jakarta.jms.admin.User user = org.ow2.joram.jakarta.jms.admin.User.create("anonymous", "anonymous", 0);

    // Get the reference of an existing queue
    jakarta.jms.Queue queue = org.ow2.joram.jakarta.jms.Queue.create(0, "queue");
    ((org.ow2.joram.jakarta.jms.Queue) queue).setFreeReading();
    ((org.ow2.joram.jakarta.jms.Queue) queue).setFreeWriting();
    ctx.bind("queue", queue);
    // Create a topic
    jakarta.jms.Topic topic = org.ow2.joram.jakarta.jms.Topic.create(0, "topic");
    ((org.ow2.joram.jakarta.jms.Topic) topic).setFreeReading();
    ((org.ow2.joram.jakarta.jms.Topic) topic).setFreeWriting();
    ctx.bind("topic", topic);

    org.ow2.joram.jakarta.jms.admin.AdminModule.disconnect();
    ctx.close();
  }
  
  private Object toJavax(Object obj) throws Exception {
    if (obj instanceof org.ow2.joram.jakarta.jms.ConnectionFactory) {
      org.ow2.joram.jakarta.jms.ConnectionFactory cf1 = (org.ow2.joram.jakarta.jms.ConnectionFactory) obj;
      javax.naming.Reference ref1 = cf1.getReference();
      
      javax.naming.Reference ref2 = new Reference(this.getClass().getName(), org.objectweb.joram.client.jms.admin.ObjectFactory.class.getName(), null);

      System.out.println("######\n# Initial reference:");
      Enumeration<RefAddr> refAddrs = ref1.getAll();
      while (refAddrs.hasMoreElements()) {
        RefAddr ra = refAddrs.nextElement();
        System.out.println(ra.getType() + " : " + ra.getContent());
        if ("cf.reliableClass".equals(ra.getType())) {
          ref2.add(new StringRefAddr(ra.getType(), ((String) ra.getContent()).replace("ow2.joram.jakarta", "objectweb.joram.client")));
        } else {
          ref2.add(ra);
        }
      }
//      String rcn = ((String) ref1.get("cf.reliableClass").getContent()).replace("ow2.joram.jakarta", "objectweb.joram.client");
      
      String cn = ref1.getClassName().replace("ow2.joram.jakarta", "objectweb.joram.client");
      org.objectweb.joram.client.jms.ConnectionFactory cf2 = (org.objectweb.joram.client.jms.ConnectionFactory) Class.forName(cn).newInstance();
      cf2.fromReference(ref2);
      return cf2;
    } else if (obj instanceof org.ow2.joram.jakarta.jms.Destination) {
      org.ow2.joram.jakarta.jms.Destination dest1 = (org.ow2.joram.jakarta.jms.Destination) obj;
      javax.naming.Reference ref = dest1.getReference();
      String cn = ref.getClassName().replace("ow2.joram.jakarta", "objectweb.joram.client");
      org.objectweb.joram.client.jms.Destination dest2 = (org.objectweb.joram.client.jms.Destination) Class.forName(cn).newInstance();
      dest2.fromReference(ref);
      return dest2;
    }
    return obj;
  }
}
