/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C) 2019 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): ScalAgent Distributed Technologies
 * Contributor(s): 
 */
package joram.local;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.objectweb.joram.client.jms.Queue;
import org.objectweb.joram.client.jms.admin.AdminModule;
import org.objectweb.joram.client.jms.admin.User;
import org.objectweb.joram.client.jms.local.LocalConnectionFactory;

import fr.dyade.aaa.agent.AgentServer;

/**
 * Test local connection with timeout (ConnectingTimer) and Transaction freezes.
 */
public class Test3 extends framework.TestCase {
  public static void main(String args[]) throws Exception {
    new Test3().run();
  }

  public void run() {
    try {
      AgentServer.init((short) 0, "s0", null);
      AgentServer.start();
      Thread.sleep(1000L);

      LocalConnectionFactory cf = LocalConnectionFactory.create();
      cf.getParameters().connectingTimer = 1;
      AdminModule.connect(cf, "root", "root");
      User.create("anonymous", "anonymous", 0);
      Queue queue = Queue.create(0);
      queue.setFreeReading();
      queue.setFreeWriting();
      AdminModule.disconnect();

      Exception expectedExc = null;
      Connection cnx = null;
      try {
        System.out.println("Should not throw an Exception.");
        cnx = cf.createConnection();
      } catch (Exception exc) {
        exc.printStackTrace();
        expectedExc = exc;
      }
      assertNotNull(cnx);
      assertNull(expectedExc);

      cnx.close();
      cnx = null;

      new Thread() {
        public void run() {
          try {
            AgentServer.getTransaction().freeze(1500L);
          } catch (InterruptedException exc) {}
        }
      }.start();
      Thread.sleep(100L);

      expectedExc = null;
      cnx = null;
      long start = System.currentTimeMillis();
      try {
        System.out.println("Should throw an Exception.");
        cnx = cf.createConnection();
      } catch (Exception exc) {
        System.out.println("Throw an Exception: " + (System.currentTimeMillis() - start) + "ms -> " + exc.getMessage());
        //        exc.printStackTrace();
        expectedExc = exc;
      }
      assertNull(cnx);
      assertNotNull(expectedExc);

      expectedExc = null;
      cnx = null;
      try {
        System.out.println("Should not throw an Exception.");
        cnx = cf.createConnection();
      } catch (Exception exc) {
        exc.printStackTrace();
        expectedExc = exc;
      }
      assertNotNull(cnx);
      assertNull(expectedExc);

      Session sess1 = cnx.createSession(false, Session.AUTO_ACKNOWLEDGE);
      MessageProducer producer = sess1.createProducer(null);

      Session sess2 = cnx.createSession(false, Session.AUTO_ACKNOWLEDGE);
      MessageConsumer qconsumer = sess2.createConsumer(queue);

      cnx.start();

      TextMessage msg1 = sess1.createTextMessage();
      msg1.setText("Test Queue with LocalConnectionFactory");
      producer.send(queue, msg1);

      TextMessage msg2 = (TextMessage) qconsumer.receive(1000L);
      assertTrue(msg1.getText().equals(msg2.getText()));

      msg1 = sess1.createTextMessage();
      msg1.setText("Test Queue with LocalConnectionFactory");
      producer.send(queue, msg1);

      new Thread() {
        public void run() {
          try {
            AgentServer.getTransaction().freeze(5000L);
          } catch (InterruptedException exc) {}
        }
      }.start();
      Thread.sleep(100L);

      msg2 = (TextMessage) qconsumer.receive(1000L);
      assertNull(msg2);
      
      msg2 = (TextMessage) qconsumer.receive(10000L);
      assertTrue(msg1.getText().equals(msg2.getText()));

      cnx.close();
    } catch (Throwable exc) {
      exc.printStackTrace();
      error(exc);
    } finally {
      System.out.println("AgentServer.stop()");
      AgentServer.stop();
      endTest();
    }
  }
}
