/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C) 2021 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s):  ScalAgent Distributed Technologies
 * Contributor(s): 
 */
package joram.reconf;

import org.objectweb.joram.client.jms.admin.AdminModule;

import fr.dyade.aaa.agent.AgentServer;
import framework.TestCase;

/**
 * Tests the update of the stored configuration.
 */
public class RestartTest extends TestCase {
  public static void main(String[] args) {
    new RestartTest().run();
  }
  
  public void run() {
    try {
      AgentServer.init(new String[] {"0", "./s0"});
      AgentServer.start();

      // Check root credential's and listening port
      AdminModule.connect("localhost", 2560, "root", "root", 30);
      AdminModule.disconnect();
      
      AgentServer.stop();
      AgentServer.reset();
      
      AgentServer.init(new String[] {"0", "./s0"});
      AgentServer.start();

      // Check root credential's and listening port
      AdminModule.connect("localhost", 2560, "root", "root", 30);
      AdminModule.disconnect();

      AgentServer.stop();
    } catch (Throwable exc) {
      exc.printStackTrace();
      error(exc);
    } finally {
      endTest();
    }
  }
}

