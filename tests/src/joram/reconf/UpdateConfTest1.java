/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C) 2021 - 2024 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s):  ScalAgent Distributed Technologies
 * Contributor(s): 
 */
package joram.reconf;

import java.io.File;
import java.io.FileWriter;
import java.util.StringJoiner;

import org.objectweb.joram.client.jms.admin.AdminModule;

import fr.dyade.aaa.agent.AgentServer;
import framework.TestCase;

/**
 * Tests the update of the stored configuration.
 */
public class UpdateConfTest1 extends TestCase {
  public static void main(String[] args) {
    new UpdateConfTest1().run();
  }
  
  public void run() {
    try {
      // Starts the server with default configuration (listening port is 2560).
      System.out.println("server start");
      startAgentServer((short) 0);
      Thread.sleep(2000);

      // Check root credential's and listening port
      AdminModule.connect("localhost", 2560, "root", "root", 30);
      AdminModule.disconnect();
      
      System.out.println("Server stop ");
      stopAgentServer((short) 0);

      // Changes the configuration (listening port is now 2561), and restarts the server.
      String config = generateA3Config("root", "admin", 2561);
      new File("./a3servers.xml").delete();
      FileWriter writer = new FileWriter("./a3servers.xml");
      writer.write(config);
      writer.close();
      AgentServer.externalUpdateConfFromFile("./a3servers.xml", "./s0");
      
      System.out.println("server start");
      startAgentServer((short) 0);
      Thread.sleep(2000);

      // Check root credential's and listening port
      AdminModule.connect("localhost", 2561, "root", "admin", 30);
      AdminModule.disconnect();

      System.out.println("Server stop ");
      stopAgentServer((short) 0);
    } catch (Throwable exc) {
      exc.printStackTrace();
      error(exc);
    } finally {
      endTest();
    }
  }

  public static String generateA3Config(String admin, String password, int port) {
    StringJoiner config = new StringJoiner(System.lineSeparator());

    config.add("<config>");
    config.add("<property name=\"Transaction\" value=\"fr.dyade.aaa.ext.NGTransaction\"/>");
    config.add("<server id=\"0\" name=\"S0\" hostname=\"localhost\">");
    config.add("<service class=\"org.objectweb.joram.mom.proxies.ConnectionManager\" args=\"" + admin + " " + password + "\"/>");
    config.add("<service class=\"org.objectweb.joram.mom.proxies.tcp.TcpProxyService\" args=\"" + port + "\"/>");

    config.add("</server>");
    config.add("</config>");
    return config.toString();
  }

}

