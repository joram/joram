/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C) 2013 - 2024 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): 
 */
package joram.jms2;

import jakarta.jms.CompletionListener;
import jakarta.jms.ConnectionFactory;
import jakarta.jms.JMSContext;
import jakarta.jms.JMSException;
import jakarta.jms.JMSProducer;
import jakarta.jms.Message;
import jakarta.jms.MessageListener;
import jakarta.jms.TextMessage;

import org.ow2.joram.jakarta.jms.Queue;
import org.ow2.joram.jakarta.jms.admin.AdminModule;
import org.ow2.joram.jakarta.jms.admin.User;
import org.ow2.joram.jakarta.jms.tcp.TcpConnectionFactory;

import framework.TestCase;

/**
 * Test the completion order when sending in asynchronous mode.
 */
public class CompletionListenerTest2 extends TestCase implements MessageListener, CompletionListener {
  public static void main(String[] args) {
    new CompletionListenerTest2().run();
  }
  
  static final int nbmsgTosend = 10;
  
  public void run()  {
    try {
      startAgentServer((short) 0);
      Thread.sleep(1000);

      ConnectionFactory cf = TcpConnectionFactory.create("localhost", 2560);
      AdminModule.connect(cf, "root", "root");   
      User.create("anonymous", "anonymous", 0);
      Queue queue = Queue.create("queue");
      queue.setFreeReading();
      queue.setFreeWriting();
      AdminModule.disconnect();

      JMSContext context = cf.createContext();
      JMSProducer producer = context.createProducer();
      producer.setAsync(this);
      context.start();

      TextMessage msg = null;
      for (int i=0; i < nbmsgTosend; i++) {
        msg = context.createTextMessage("msg#" + i);
        msg.setIntProperty("order", i);
        producer.send(queue, msg);
      }
      System.out.println("Messages sent: " + nbmsgTosend);
      Thread.sleep(1000L);
      
      context.close();
      System.out.println("Context closed: " + sent);
      assertTrue("Send completed " + sent + " should be #"  + nbmsgTosend, (sent == nbmsgTosend));
    } catch (Throwable exc) {
      exc.printStackTrace();
      error(exc);
    } finally {
      stopAgentServer((short) 0);
      endTest();     
    }
  }

  volatile int nbmsg = 0;
  
  @Override
  public void onMessage(Message message) {
    nbmsg += 1;
  }

  volatile int sent = 0;
  
  @Override
  public void onCompletion(Message message) {
    int n = -1;
    try {
      n = message.getIntProperty("order");
      System.out.println("Received " + n);
    } catch (JMSException e) {
      assertTrue("Unexpected exception: " + e.getMessage(), false);
    }
    assertTrue("Received " + n + " should be #"  + sent, (n == sent));
    sent += 1;
  }

  @Override
  public void onException(Message message, Exception exc) {
    exc.printStackTrace();
  }
}
