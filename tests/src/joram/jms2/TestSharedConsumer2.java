/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C) 2013 - 2024 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): ScalAgent Distributed Technologies
 */
package joram.jms2;

import jakarta.jms.Connection;
import jakarta.jms.ConnectionFactory;
import jakarta.jms.JMSConsumer;
import jakarta.jms.JMSContext;
import jakarta.jms.JMSException;
import jakarta.jms.Message;
import jakarta.jms.MessageConsumer;
import jakarta.jms.MessageListener;
import jakarta.jms.MessageProducer;
import jakarta.jms.Session;
import jakarta.jms.TextMessage;

import org.ow2.joram.jakarta.jms.Topic;
import org.ow2.joram.jakarta.jms.admin.AdminModule;
import org.ow2.joram.jakarta.jms.admin.User;
import org.ow2.joram.jakarta.jms.tcp.TcpConnectionFactory;

import framework.TestCase;

/**
 * Create a Topic, JMSContext, Connection and User.
 * 
 * On main thread:
 * - create 2 shared durable consumers ("sharedConsumerTest") on the topic
 * - create 2 non-shared consumer on the topic
 * - Set message listener for the 4 consumers
 * 
 * - send 3 messages on the topic
 * - verify that the shared consumers shares the messages and the non-shared consumer receives each message.
 * 
 * - send 30 messages on the topic
 * - verify that the sum of received messages by the shared consumers is 30.
 * - verify that the non-shared consumer received 30 messages each one.
 */
public class TestSharedConsumer2 extends TestCase {
  public static void main(String[] args) {
    new TestSharedConsumer2().run();
  }
  
  final String subName = "sharedConsumerTest";

  public void run()  {
    try {
      startAgentServer((short) 0);
      Thread.sleep(1000);
      
      final ConnectionFactory cf = TcpConnectionFactory.create("localhost", 2560);
      AdminModule.connect(cf, "root", "root");   
      User.create("anonymous", "anonymous", 0);
      final Topic topic = Topic.create("topic");
      topic.setFreeReading();
      topic.setFreeWriting();
      AdminModule.disconnect();
      
      // create non-shared consumer
      Connection cnx = cf.createConnection();
      
      Session sess = cnx.createSession(false, Session.AUTO_ACKNOWLEDGE);
      MessageConsumer receiver3 = sess.createConsumer(topic);
      MsgListener mainML1 = new MsgListener("Main-1-Listener on topic");
      receiver3.setMessageListener(mainML1);
      
      Session sess1 = cnx.createSession(false, Session.AUTO_ACKNOWLEDGE);
      MessageConsumer receiver4 = sess1.createConsumer(topic);
      MsgListener mainML2 = new MsgListener("Main-2-Listener on topic");
      receiver4.setMessageListener(mainML2);
      
      cnx.start();
      
      // create shared consumer
      JMSContext context = cf.createContext();
      JMSConsumer receiver1 = context.createSharedDurableConsumer(topic, subName);
      JMSConsumer receiver2 = context.createSharedDurableConsumer(topic, subName);
      
      MsgListener sharedML1 = new MsgListener("Shared-1-Listener on topic");
      receiver1.setMessageListener(sharedML1);
      
      MsgListener sharedML2 = new MsgListener("Shared-2-Listener on topic");
      receiver2.setMessageListener(sharedML2);
      
      // send 3 messages
      System.out.println("== send 3");
      sendMsg(cnx, topic, 3);
      Thread.sleep(100);
      assertEquals("shared Listeners : ", 3, sharedML1.getNbMsgReceived()+sharedML2.getNbMsgReceived());
      assertEquals("mainML1-Listener : ", 3, mainML1.getNbMsgReceived());
      assertEquals("mainML2-Listener : ", 3, mainML2.getNbMsgReceived());
      sharedML1.reset();
      sharedML2.reset();
      mainML1.reset();
      mainML2.reset();
      
      // send 30 message
      int nbMsgTosend = 30;
      System.out.println("\n== send " + nbMsgTosend);
      sendMsg(cnx, topic, nbMsgTosend);
      Thread.sleep(500);
      assertEquals("mainML1-Listener : ", nbMsgTosend, mainML1.getNbMsgReceived());
      assertEquals("mainML2-Listener : ", nbMsgTosend, mainML2.getNbMsgReceived());
      assertEquals("Shared consumers : ", nbMsgTosend, sharedML1.getNbMsgReceived() + sharedML2.getNbMsgReceived());
      sharedML1.reset();
      sharedML2.reset();
      mainML1.reset();
      mainML2.reset();
      
      System.out.println("Close.");
      receiver1.close();
      receiver2.close();
      receiver3.close();
      receiver4.close();
      System.out.println("unsubscribe");
      sess.unsubscribe("sharedConsumerTest");
      context.close();
      cnx.close();
    } catch (Throwable exc) {
      exc.printStackTrace();
      error(exc);
    } finally {
      stopAgentServer((short) 0);
      endTest();     
    }
  }
  
  public void sendMsg(Connection cnx, Topic topic, int nbMsg) throws JMSException {
    System.out.println("send " + nbMsg + " messages on topic");
    Session sess = cnx.createSession(false, Session.AUTO_ACKNOWLEDGE);
    MessageProducer producer = sess.createProducer(topic);
    for (int i = 0; i < nbMsg; i++) {
      TextMessage msg = sess.createTextMessage("Test number " + i);
      producer.send(msg);
    }
  }
  
  public void consumer(ConnectionFactory cf, Topic topic, MsgListener ml) {
  }
  
  class MsgListener implements MessageListener {
    private String ident = null;
    private int nbMsgReceived = 0;

    public MsgListener(String ident) {
      this.ident = ident;
    }

    public void onMessage(Message msg) {
      try {
        if (msg instanceof TextMessage) {
          System.out.println(ident + ": " + ((TextMessage) msg).getText());
          nbMsgReceived++;
        }        
      } catch (JMSException e) {
        System.err.println("Exception in listener: " + e);
        error(e);
      }
    }
    
    public int getNbMsgReceived() {
      return nbMsgReceived;
    }
    
    public void reset() {
      nbMsgReceived = 0;
    }
  }
}
