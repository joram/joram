/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C) 2013 - 2024 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): 
 */
package joram.jms2;

import jakarta.jms.ConnectionFactory;
import jakarta.jms.InvalidDestinationRuntimeException;
import jakarta.jms.JMSContext;

import org.ow2.joram.jakarta.jms.admin.AdminModule;
import org.ow2.joram.jakarta.jms.admin.User;
import org.ow2.joram.jakarta.jms.tcp.TcpConnectionFactory;

import framework.TestCase;

/**
 * Testing JMSContext.unsubscribe(String) for InvalidDestinationRuntimeException.
 */
public class Test11 extends TestCase {
  public static void main(String[] args) {
    new Test11().run();
  }

  public void run()  {
    try {
      startAgentServer((short) 0);
      Thread.sleep(1000);

      ConnectionFactory cf = TcpConnectionFactory.create("localhost", 2560);
      AdminModule.connect(cf, "root", "root");   
      User.create("anonymous", "anonymous", 0);
      AdminModule.disconnect();

      JMSContext context = cf.createContext(JMSContext.CLIENT_ACKNOWLEDGE);

      try {
        context.unsubscribe("toto");
        assertTrue("Should throw an InvalidDestinationRuntimeException", false);
      } catch (Exception exc) {
        assertTrue("Expect an InvalidDestinationRuntimeException, got " + exc,
                   exc instanceof InvalidDestinationRuntimeException);
      }

      context.close();
    } catch (Throwable exc) {
      exc.printStackTrace();
      error(exc);
    } finally {
      stopAgentServer((short) 0);
      endTest();     
    }
  }

}
