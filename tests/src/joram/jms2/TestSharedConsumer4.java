/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C) 2024 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): ScalAgent Distributed Technologies
 */
package joram.jms2;

import jakarta.jms.Connection;
import jakarta.jms.ConnectionFactory;
import jakarta.jms.JMSConsumer;
import jakarta.jms.JMSContext;
import jakarta.jms.JMSException;
import jakarta.jms.Message;
import jakarta.jms.MessageListener;
import jakarta.jms.MessageProducer;
import jakarta.jms.Session;
import jakarta.jms.TextMessage;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.ow2.joram.jakarta.jms.Topic;
import org.ow2.joram.jakarta.jms.admin.AdminModule;
import org.ow2.joram.jakarta.jms.admin.User;
import org.ow2.joram.jakarta.jms.tcp.TcpConnectionFactory;

import framework.TestCase;

/**
 * Test the distribution of messages between several shared subscribers and flow control activated.
 * Each subscriber consumes messages at a different rate. 
 */
public class TestSharedConsumer4 extends TestCase {
  public static void main(String[] args) {
    new TestSharedConsumer4().run();
  }
  
  final String subName = "sharedConsumerTest";
  final int nbmsg = 1000;
  
  Set<Integer> received = Collections.synchronizedSet(new HashSet<>(nbmsg));

  MsgListener sharedML1, sharedML2, sharedML3;
  
  public void run()  {
    try {
      startAgentServer((short) 0, new String[] {"-Dorg.ow2.joram.subcription.maxNumberOfMessagePerRequest=3"});
      Thread.sleep(1000);
      
      final TcpConnectionFactory cf = TcpConnectionFactory.create("localhost", 2560);
//      cf.getParameters().queueMessageReadMax = 0;
      cf.getParameters().topicActivationThreshold = 1;
      cf.getParameters().topicPassivationThreshold = 2;
      
      AdminModule.connect(cf, "root", "root");   
      User.create("anonymous", "anonymous", 0);
      final Topic topic = Topic.create("topic");
      topic.setFreeReading();
      topic.setFreeWriting();
      AdminModule.disconnect();

      // create non-shared consumer
      Connection cnx = cf.createConnection();
      cnx.start();
      
      // create shared consumer
      JMSContext context1 = cf.createContext();
      JMSConsumer receiver1 = context1.createSharedDurableConsumer(topic, subName);
      sharedML1 = new MsgListener("Shared-1-Listener on topic", 15);
      receiver1.setMessageListener(sharedML1);
      
      JMSContext context2 = cf.createContext();
      JMSConsumer receiver2 = context2.createSharedDurableConsumer(topic, subName);
      sharedML2 = new MsgListener("Shared-2-Listener on topic", 30);
      receiver2.setMessageListener(sharedML2);

      JMSContext context3 = cf.createContext();
      JMSConsumer receiver3 = context3.createSharedDurableConsumer(topic, subName);
      sharedML3 = new MsgListener("Shared-3-Listener on topic", 60);
      receiver3.setMessageListener(sharedML3);
      
      Session sess = cnx.createSession(false, Session.AUTO_ACKNOWLEDGE);
      MessageProducer producer = sess.createProducer(topic);
      int i = 0;
      for (; i < nbmsg; i++) {
        System.out.println("message #" + i + " sent.");
        TextMessage msg = sess.createTextMessage("Test number " + i);
        msg.setIntProperty("index", i);
        producer.send(msg);
        // Waits allowing flow control..
        Thread.sleep(4);
      }
      
      int nbtry = 0;
      while ((sharedML1.getNbMsgReceived() + sharedML2.getNbMsgReceived() +  sharedML3.getNbMsgReceived()) <nbmsg) {
        System.out.println("waits #" + nbtry);
        Thread.sleep(1000);
        nbtry += 1;
        if (nbtry >30) break;
      }

      for (i=0; i<nbmsg; i++) {
        if (!received.contains(i))
          System.out.println("Should received: " + i);
        assertTrue("Should received: " + i, received.contains(i));
      }
      System.out.println("Check#1 : " + sharedML1.getNbMsgReceived() + '/' + sharedML2.getNbMsgReceived() + '/' +  sharedML3.getNbMsgReceived());
      assertTrue(sharedML1.getName() + " must receive twice as many messages as " + sharedML2.getName() + ": " + sharedML1.getNbMsgReceived() + '/' + sharedML2.getNbMsgReceived(),
                 verify(sharedML1.getNbMsgReceived(), sharedML2.getNbMsgReceived()));
      assertTrue(sharedML2.getName() + " must receive twice as many messages as " + sharedML3.getName() + ": " + sharedML2.getNbMsgReceived() + '/' + sharedML3.getNbMsgReceived(),
                 verify(sharedML2.getNbMsgReceived(), sharedML3.getNbMsgReceived()));
      
      System.out.println("Close.");
      context1.close();
      context2.close();
      context3.close();
      cnx.close();
    } catch (Throwable exc) {
      exc.printStackTrace();
      error(exc);
    } finally {
      stopAgentServer((short) 0);
      endTest();     
    }
  }
  
  /**
   * Verify that a is twice as large as b with a margin of error less of 10%.
   */
  private boolean verify(int a, int b) {
    int x = ((a*100)/b) -200;
    return (x>-10)&&(x<10);
  }
  
  private String mlStatus() {
    return " " + sharedML1.getNbMsgReceived() + '/' + sharedML2.getNbMsgReceived() + '/' +  sharedML3.getNbMsgReceived();
  }
  
  public void consumer(ConnectionFactory cf, Topic topic, MsgListener ml) {
    // create shared consumer
    JMSContext context = cf.createContext();
    JMSConsumer receiver = context.createSharedDurableConsumer(topic, subName);
    receiver.setMessageListener(ml);
  }
  
  class MsgListener implements MessageListener {
    private String ident = null;

    public String getName() {
      return ident;
    }
    
    private int nbMsgReceived = 0;

    public int getNbMsgReceived() {
      return nbMsgReceived;
    }

    long pause = 0;
    
    public void setPause(long pause) {
        this.pause = pause;
    }

    public MsgListener(String ident, long pause) {
      this.ident = ident;
      this.pause = pause;
    }

    public void onMessage(Message msg) {
      try {
        nbMsgReceived++;
        received.add(msg.getIntProperty("index"));
        if (msg instanceof TextMessage) {
          System.out.println(ident + '(' + nbMsgReceived + "): " + ((TextMessage) msg).getText() + mlStatus());
          if (pause > 0L) Thread.sleep(pause);
        }
      } catch (JMSException e) {
        System.err.println("Exception in listener: " + e);
        error(e);
      } catch (InterruptedException exc) {
        exc.printStackTrace();
      }
    }
  }
}
