/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C) 2013 - 2024 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): 
 */
package joram.jms2;

import jakarta.jms.CompletionListener;
import jakarta.jms.ConnectionFactory;
import jakarta.jms.IllegalStateRuntimeException;
import jakarta.jms.JMSConsumer;
import jakarta.jms.JMSContext;
import jakarta.jms.JMSException;
import jakarta.jms.JMSProducer;
import jakarta.jms.Message;
import jakarta.jms.TextMessage;

import org.ow2.joram.jakarta.jms.Queue;
import org.ow2.joram.jakarta.jms.admin.AdminModule;
import org.ow2.joram.jakarta.jms.admin.User;
import org.ow2.joram.jakarta.jms.tcp.TcpConnectionFactory;

import framework.TestCase;

/**
 * Test calling stop and close from CompletionListener (expect IllegalStateRuntimeException).
 */
public class CompletionListenerTest3 extends TestCase implements CompletionListener{
  public static void main(String[] args) {
    new CompletionListenerTest3().run();
  }

  public static final boolean cfAsync = Boolean.getBoolean("CF_ASYNC");
  
  JMSContext context = null;
  
  public void run()  {
    try {
      startAgentServer((short) 0);
      Thread.sleep(1000);

      ConnectionFactory cf = TcpConnectionFactory.create("localhost", 2560);
      AdminModule.connect(cf, "root", "root");   
      User.create("anonymous", "anonymous", 0);
      Queue queue = Queue.create("queue");
      queue.setFreeReading();
      queue.setFreeWriting();
      AdminModule.disconnect();

      System.out.println("cfAsync=" + cfAsync);
      ((org.ow2.joram.jakarta.jms.ConnectionFactory) cf).getParameters().asyncSend = cfAsync;
      context = cf.createContext();
      JMSProducer producer = context.createProducer();
      producer.setAsync(this);
      JMSConsumer consumer = context.createConsumer(queue);
      context.start();

      int i = 0;
      
      {
        // Test to stop the context from CompletionListener (this will try to close
        // only the session as there is a second active context).
        TextMessage msg = context.createTextMessage("stop");
        producer.send(queue, msg);
        System.out.println("stop sent");
        Thread.sleep(2000L);
        assertTrue("Should expect message completion", nbmsg == ++i);
        Message recv = consumer.receive(1000L);
        System.out.println("receive " + recv);
        assertTrue("Should not receive message", (recv == null));
        context.start();
        System.out.println("Context started.");
        recv = consumer.receive(1000L);
        System.out.println("receive " + recv);
        assertTrue("Should receive message", (recv != null));        
      }
      
      JMSContext context2 = context.createContext(JMSContext.AUTO_ACKNOWLEDGE);
      {
        // Test to close the context from CompletionListener (this will try to close
        // only the session as there is a second active context).
        TextMessage msg = context.createTextMessage("close");
        producer.send(queue, msg);
        System.out.println("close#1 sent");
        Thread.sleep(2000L);
        assertTrue("Should expect message completion", nbmsg == ++i);
      }
      context2.close();
      
      {
        // Test to close the context from CompletionListener (this will try to close
        // the connection as there is no other active context).
        TextMessage msg = context.createTextMessage("close");
        producer.send(queue, msg);
        System.out.println("close#2 sent");
        Thread.sleep(2000L);
        assertTrue("Should expect message completion", nbmsg == ++i);
      }
      
      context.close();
      System.out.println("Context close: " + nbmsg);
    } catch (Throwable exc) {
      exc.printStackTrace();
      error(exc);
    } finally {
      stopAgentServer((short) 0);
      endTest();     
    }
  }
  
  volatile int nbmsg = 0;
  
  @Override
  public void onCompletion(Message message) {
    try {
      if ("stop".equals(((TextMessage) message).getText())) {
        try {
          context.stop();
          assertTrue("expect no exception.", true);
          System.out.println("Context stopped.");
        } catch (Exception e) {
          assertTrue("Caught unexpected exception: " + e.getMessage(), false);
        }
      } else if ("close".equals(((TextMessage) message).getText())) {
        try {
          context.close();
          assertTrue("expect an IllegalStateRuntimeException.", false);
        } catch (Exception e) {
          assertTrue("Caught unexpected exception: " + e.getMessage(), e instanceof IllegalStateRuntimeException);
          System.out.println("Context not closed: " + e);
        }
      }
      nbmsg += 1;
    } catch (JMSException e) {
      exception(e);
    }
  }

  @Override
  public void onException(Message message, Exception exc) {
    exception(exc);
  }
}
