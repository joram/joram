/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C) 2013 - 2024 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): ScalAgent Distributed Technologies
 */
package joram.jms2;

import jakarta.jms.Connection;
import jakarta.jms.ConnectionFactory;
import jakarta.jms.JMSConsumer;
import jakarta.jms.JMSContext;
import jakarta.jms.JMSException;
import jakarta.jms.Message;
import jakarta.jms.MessageConsumer;
import jakarta.jms.MessageListener;
import jakarta.jms.MessageProducer;
import jakarta.jms.Session;
import jakarta.jms.TextMessage;

import org.ow2.joram.jakarta.jms.Topic;
import org.ow2.joram.jakarta.jms.admin.AdminModule;
import org.ow2.joram.jakarta.jms.admin.User;
import org.ow2.joram.jakarta.jms.tcp.TcpConnectionFactory;

import framework.TestCase;

/**
 * Create a Topic, JMSContext, Connection and User.
 * 
 * On main thread:
 * - create 2 shared durable consumers ("sharedConsumerTest") on the topic
 * - create 2 non-shared consumer on the topic
 * - Set message listener for the 4 consumers
 * On Thread 1:
 * - create 1 shared durable consumer ("sharedConsumerTest") on the topic
 * 
 * - send 3 messages on the topic
 * - verify that the shared consumers (main and Thread) receive 1 message and the non-shared consumer receive 3 messages
 * 
 * - send 30 messages on the topic
 * - verify that the sum of received messages by the shared consumer is 30.
 * - verify that the non-shared consumer received 30 messages each one.
 */
public class TestSharedConsumer3 extends TestCase {
  public static void main(String[] args) {
    new TestSharedConsumer3().run();
  }
  
  final String subName = "sharedConsumerTest";

  public void run()  {
    try {
      startAgentServer((short) 0, new String[] {"-Dorg.ow2.joram.subcription.maxNumberOfMessagePerRequest=2"});
      Thread.sleep(1000);
      
      final TcpConnectionFactory cf = TcpConnectionFactory.create("localhost", 2560);
//      cf.getParameters().queueMessageReadMax = 0;
      cf.getParameters().topicActivationThreshold = 1;
      cf.getParameters().topicPassivationThreshold = 2;
      
      AdminModule.connect(cf, "root", "root");   
      User.create("anonymous", "anonymous", 0);
      final Topic topic = Topic.create("topic");
      topic.setFreeReading();
      topic.setFreeWriting();
      AdminModule.disconnect();

      // create non-shared consumer
      Connection cnx = cf.createConnection();
      Session sess1 = cnx.createSession(false, Session.AUTO_ACKNOWLEDGE);
      MessageConsumer receiver = sess1.createConsumer(topic);
      MsgListener mainML = new MsgListener("Main-Listener on topic");
      receiver.setMessageListener(mainML);
      cnx.start();
      
      // create shared consumer
      JMSContext context1 = cf.createContext();
      JMSConsumer receiver1 = context1.createSharedDurableConsumer(topic, subName);
      MsgListener sharedML1 = new MsgListener("Shared-1-Listener on topic");
      receiver1.setMessageListener(sharedML1);
      
      JMSContext context2 = cf.createContext();
      JMSConsumer receiver2 = context2.createSharedDurableConsumer(topic, subName);
      MsgListener sharedML2 = new MsgListener("Shared-2-Listener on topic");
      receiver2.setMessageListener(sharedML2);
      
      // send 10 messages.
      Session sess = cnx.createSession(false, Session.AUTO_ACKNOWLEDGE);
      MessageProducer producer = sess.createProducer(topic);
      int i = 0;
      for (; i < 20; i++) {
        System.out.println("message #" + i + " sent.");
        TextMessage msg = sess.createTextMessage("Test number " + i);
        producer.send(msg);
        // Waits allowing flow control..
        Thread.sleep(100);
      }
      Thread.sleep(100);

      System.out.println("Check#1 - each ML should have received 3 messages: " + sharedML1.getNbMsgReceived() + '/' + sharedML1.getNbMsgReceived() + '/' +  mainML.getNbMsgReceived());
      assertTrue(sharedML1.getName() + " should receives 3 messages : " + sharedML1.getNbMsgReceived(), sharedML1.getNbMsgReceived() == 3);
      assertTrue(sharedML2.getName() + " should receives 3 messages : " + sharedML1.getNbMsgReceived(), sharedML2.getNbMsgReceived() == 3);
      assertTrue(mainML.getName() + " should receives 3 messages : " + mainML.getNbMsgReceived(), mainML.getNbMsgReceived() == 3);

      JMSContext context3 = cf.createContext();
      JMSConsumer receiver3 = context3.createSharedDurableConsumer(topic, subName);
      MsgListener sharedML3 = new MsgListener("Shared-3-Listener on topic");
      System.out.println("Trace#1-2: creates " + sharedML3.getName());
//      sharedML3.unlock();
      receiver3.setMessageListener(sharedML3);

//      System.out.println("Trace#1-1: unlocks " + mainML.getName());
//      mainML.unlock();
      Thread.sleep(10000);
      System.out.println("Trace#2: " + mainML.getName() + " should have received all messages: " + mainML.getNbMsgReceived());
      
      System.out.println("Check#2 - all messages should have been received: " + sharedML1.getNbMsgReceived() + '/' + sharedML2.getNbMsgReceived() + '/' +  mainML.getNbMsgReceived());
      assertTrue(sharedML1.getName() + " : should receive at least 5 messages", sharedML1.getNbMsgReceived() >=5);
      assertTrue(sharedML2.getName() + " : should receive at least 5 messages", sharedML2.getNbMsgReceived() >=5);
      assertEquals(mainML.getName() + " :  should receive 20 message", 20, mainML.getNbMsgReceived());
      
      Thread.sleep(100);
      System.out.println("Trace#3: " + sharedML3.getName() + " should have received messages: " + sharedML3.getNbMsgReceived());
      assertTrue(sharedML3.getName() + " : should receive messages", sharedML3.getNbMsgReceived() >0);
      
//      sharedML1.unlock();
//      sharedML2.unlock();
      
      Thread.sleep(100);
      System.out.println("Trace#4: all messages should have been received.");
      assertEquals("Shared ML :  should receive all messages", 20,
                   sharedML1.getNbMsgReceived() + sharedML2.getNbMsgReceived() + sharedML3.getNbMsgReceived());
      
      for (; i < 30; i++) {
        System.out.println("message #" + i + " sent.");
        TextMessage msg = sess.createTextMessage("Test number " + i);
        producer.send(msg);
      }
      sess.close();
      
      Thread.sleep(5000);
      System.out.println("Check#2 - all messages should have been received: " + sharedML1.getNbMsgReceived() + '/' + sharedML1.getNbMsgReceived() + '/' + sharedML3.getNbMsgReceived() + '/' +  mainML.getNbMsgReceived());
      assertEquals(mainML.getName() + " :  should receive all messages", 30, mainML.getNbMsgReceived());
      assertEquals("Shared ML :  should receive all messages", 30,
                   sharedML1.getNbMsgReceived() + sharedML2.getNbMsgReceived() + sharedML3.getNbMsgReceived());
      
      System.out.println("Close.");
      context1.close();
      context2.close();
      context3.close();
      cnx.close();
      
    } catch (Throwable exc) {
      exc.printStackTrace();
      error(exc);
    } finally {
      stopAgentServer((short) 0);
      endTest();     
    }
  }
  
  public void consumer(ConnectionFactory cf, Topic topic, MsgListener ml) {
    // create shared consumer
    JMSContext context = cf.createContext();
    JMSConsumer receiver = context.createSharedDurableConsumer(topic, subName);
    receiver.setMessageListener(ml);
  }
  
  class MsgListener implements MessageListener {
    private String ident = null;
    private int nbMsgReceived = 0;

    public MsgListener(String ident) {
      this.ident = ident;
    }

    public String getName() {
      return ident;
    }
    
    public void onMessage(Message msg) {
      try {
        if (msg instanceof TextMessage) {
          System.out.println(ident + ": " + ((TextMessage) msg).getText());
          nbMsgReceived++;
          if (nbMsgReceived < 4) Thread.sleep(1000);
        }        
      } catch (JMSException e) {
        System.err.println("Exception in listener: " + e);
        error(e);
      } catch (InterruptedException exc) {
        exc.printStackTrace();
      }
    }
    
    public int getNbMsgReceived() {
      return nbMsgReceived;
    }
    
    public void reset() {
      nbMsgReceived = 0;
    }
  }
}
