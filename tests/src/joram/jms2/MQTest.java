/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C)  2016 - 2024 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s):
 * Contributor(s): 
 */
package joram.jms2;

import jakarta.jms.BytesMessage;
import jakarta.jms.ConnectionFactory;
import jakarta.jms.DeliveryMode;
import jakarta.jms.JMSConsumer;
import jakarta.jms.JMSContext;
import jakarta.jms.JMSProducer;
import jakarta.jms.Message;
import jakarta.jms.MessageListener;
import jakarta.jms.TextMessage;

import org.ow2.joram.jakarta.jms.Queue;
import org.ow2.joram.jakarta.jms.admin.AdminModule;
import org.ow2.joram.jakarta.jms.admin.User;
import org.ow2.joram.jakarta.jms.tcp.TcpConnectionFactory;

import framework.TestCase;

public class MQTest extends TestCase {
  public static void main(String[] args) {
    new MQTest().run();
  }
  
  static final int NBMSGS = 100000;
  static volatile long startprod1, startprod2;
  static volatile long endprod1, endprod2;
  static volatile long endcons;

  static JMSContext context = null;
  static JMSProducer producer = null;
  static JMSConsumer consumer = null;
  static JMSContext context2 = null;
  static JMSContext context3 = null;
  static JMSProducer producer2 = null;

  public void run()  {
    try {
      startAgentServer((short) 0);
      Thread.sleep(1000);

      ConnectionFactory cf = TcpConnectionFactory.create("localhost", 2560);
      AdminModule.connect(cf, "root", "root");   
      User.create("anonymous", "anonymous", 0);
      Queue dest = Queue.create("queue");
      dest.setFreeReading();
      dest.setFreeWriting();
      AdminModule.disconnect();

      // ================================================================================
      // Test1: Sends and receives a message

      try {
        System.out.println("==========\nRun Test1");
        
        // Create JMS objects
        context = cf.createContext();
        // JMSContext context = cf.createContext(APP_USER, APP_PASSWORD);
//        Destination dest = context.createQueue("queue:///" + WMQ_QUEUE_NAME);
        // System.out.println(dest);
        producer = context.createProducer();
        producer.setDeliveryMode(DeliveryMode.PERSISTENT);
        consumer = context.createConsumer(dest);
        
        // Set up the message
        long date = System.currentTimeMillis();
        TextMessage message = context.createTextMessage("Your lucky number today is " + date);
        message.setLongProperty("timestamp", date);

        long start = System.currentTimeMillis();
        producer.send(dest, message);
        System.out.println("Sent message:\n" + message);

        String receivedMessage = consumer.receiveBody(String.class, 15000);
        System.out.println("Test1 Ok.\nReceive message in " + (System.currentTimeMillis() -start) + "ms -> " + receivedMessage);
      } catch (Exception exc) {
        System.err.println("Test1 fails:");
        exc.printStackTrace();
        System.exit(-1);
      } finally {
        consumer.close();
        context.close();
      }

      // ================================================================================
      // Test2: 
      
      try {
        System.out.println("==========\nRun Test2");
        
        // Create JMS objects
        context = cf.createContext();
        context2 = cf.createContext();
        // JMSContext context = cf.createContext(APP_USER, APP_PASSWORD);
//        Destination dest = context.createQueue("queue:///" + WMQ_QUEUE_NAME);
        // System.out.println(dest);
        producer = context.createProducer();
        producer.setDeliveryMode(DeliveryMode.PERSISTENT);
        consumer = context2.createConsumer(dest);
        endcons = 0L;
        consumer.setMessageListener(new MessageListener() {
          int nbreceived = 0;
          public void onMessage(Message message) {
            nbreceived += 1;
            if (nbreceived == NBMSGS) {
              endcons = System.currentTimeMillis();
            }
          }
        });
        
        BytesMessage message = context.createBytesMessage();
        message.writeBytes(new byte[500]);
        long start = System.currentTimeMillis();
        for (int i=0; i<NBMSGS; i++) {
          producer.send(dest, message);
        }
        long end1 = System.currentTimeMillis();
        System.out.println("Sends " + NBMSGS + " messages in " + (end1 -start) + "ms.");
        do {
          Thread.sleep(1000L);
        } while (endcons == 0L);
        System.out.println("Receives " + NBMSGS + " messages in " + (endcons -start) + "ms.");
        System.out.println("Test2 Ok.");      
      } catch (Exception exc) {
        System.err.println("Test2 fails:");
        exc.printStackTrace();
        System.exit(-1);
      } finally {
        consumer.close();
        context.close();
        context2.close();
      }

      // ================================================================================
      // Test3: 
      
      try {
        System.out.println("==========\nRun Test3");
        
        // Create JMS objects
        context = cf.createContext(JMSContext.SESSION_TRANSACTED);
        context2 = cf.createContext();
        // JMSContext context = cf.createContext(APP_USER, APP_PASSWORD);
//        Destination dest = context.createQueue("queue:///" + WMQ_QUEUE_NAME);
        // System.out.println(dest);
        producer = context.createProducer();
        producer.setDeliveryMode(DeliveryMode.PERSISTENT);
        consumer = context2.createConsumer(dest);
        endcons = 0L;
        consumer.setMessageListener(new MessageListener() {
          int nbreceived = 0;
          public void onMessage(Message message) {
            nbreceived += 1;
            if (nbreceived == NBMSGS) {
              endcons = System.currentTimeMillis();
            }
          }
        });
        
        BytesMessage message = context.createBytesMessage();
        message.writeBytes(new byte[500]);
        long start = System.currentTimeMillis();
        for (int i=0; i<NBMSGS; i++) {
          producer.send(dest, message);
          if ((i%10) == 9) context.commit();
        }
        long end1 = System.currentTimeMillis();
        System.out.println("Sends " + NBMSGS + " messages in " + (end1 -start) + "ms.");
        do {
          Thread.sleep(1000L);
        } while (endcons == 0L);
        System.out.println("Receives " + NBMSGS + " messages in " + (endcons -start) + "ms.");
        System.out.println("Test3 Ok.");      
      } catch (Exception exc) {
        System.err.println("Test3 fails:");
        exc.printStackTrace();
        System.exit(-1);
      } finally {
        consumer.close();
        context.close();
        context2.close();
      }

      // ================================================================================
      // Test4: 

      try {
        System.out.println("==========\nRun Test4");
        
        // Create JMS objects
        context = cf.createContext();
        context3 = cf.createContext();
        context2 = cf.createContext();
        // JMSContext context = cf.createContext(APP_USER, APP_PASSWORD);
//        Destination dest = context.createQueue("queue:///" + WMQ_QUEUE_NAME);
        // System.out.println(dest);
        producer = context.createProducer();
        producer.setDeliveryMode(DeliveryMode.PERSISTENT);
        producer2 = context3.createProducer();
        producer2.setDeliveryMode(DeliveryMode.PERSISTENT);
        consumer = context2.createConsumer(dest);
        endcons = 0L;
        consumer.setMessageListener(new MessageListener() {
          int nbreceived = 0;
          public void onMessage(Message message) {
            nbreceived += 1;
            if (nbreceived == NBMSGS) {
              endcons = System.currentTimeMillis();
            }
          }
        });
        
        BytesMessage message = context.createBytesMessage();
        message.writeBytes(new byte[500]);
        long start = System.currentTimeMillis();
        Thread p1 = new Thread(new Runnable() {
          @Override
          public void run() {
            startprod1 = System.currentTimeMillis();
            for (int i=0; i<(NBMSGS/2); i++) {
              producer.send(dest, message);
            }
            endprod1 = System.currentTimeMillis();
            System.out.println("Sends " + NBMSGS + " messages in " + (endprod1 -startprod1) + "ms.");
          }
        });
        Thread p2 = new Thread(new Runnable() {
          @Override
          public void run() {
            startprod2 = System.currentTimeMillis();
            for (int i=0; i<(NBMSGS/2); i++) {
              producer.send(dest, message);
            }
            endprod2 = System.currentTimeMillis();
            System.out.println("Sends " + NBMSGS + " messages in " + (endprod2 -startprod2) + "ms.");
          }
        });
        p1.start();
        p2.start();
//        long end1 = System.currentTimeMillis();
//        System.out.println("Sends " + NBMSGS + " messages in " + (end1 -start) + "ms.");
        do {
          Thread.sleep(1000L);
        } while (endcons == 0L);
        System.out.println("Receives " + NBMSGS + " messages in " + (endcons -start) + "ms.");
        p1.join(1000); p2.join(1000);
        System.out.println("Test4 Ok.");      
      } catch (Exception exc) {
        System.err.println("Test4 fails:");
        exc.printStackTrace();
        System.exit(-1);
      } finally {
        consumer.close();
        context.close();
        context2.close();
      }
    } catch (Throwable exc) {
      exc.printStackTrace();
      error(exc);
    } finally {
      stopAgentServer((short) 0);
      endTest();     
    }
  }

}
