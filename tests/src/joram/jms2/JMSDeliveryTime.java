/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C)  2016 - 2024 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s):
 * Contributor(s): 
 */
package joram.jms2;

import jakarta.jms.ConnectionFactory;
import jakarta.jms.DeliveryMode;
import jakarta.jms.JMSConsumer;
import jakarta.jms.JMSContext;
import jakarta.jms.JMSException;
import jakarta.jms.JMSProducer;
import jakarta.jms.Message;
import jakarta.jms.TextMessage;

import org.ow2.joram.jakarta.jms.Queue;
import org.ow2.joram.jakarta.jms.admin.AdminModule;
import org.ow2.joram.jakarta.jms.admin.User;
import org.ow2.joram.jakarta.jms.tcp.TcpConnectionFactory;

import framework.TestCase;

/**
 * Test the JMSDeliveryTime property (From Jakarta/JMS TCK).
 * Send message and verify that JMSDeliveryTime is correct with the DeliveryDelay set to
 * 20 seconds. Test with DeliveryMode.PERSISTENT and DeliveryMode.NON_PERSISTENT.
 */
public class JMSDeliveryTime extends TestCase {

  public static void main(String[] args) {
    new JMSDeliveryTime().run();
  }
  
  public void run() {
    try {
      startAgentServer((short) 0);
      Thread.sleep(1000);
      
      admin();
      
      test1();
      test2();
    } catch (Throwable exc) {
      exc.printStackTrace();
      error(exc);
    } finally {
      stopAgentServer((short) 0);
      endTest();     
    }
  }
  
  ConnectionFactory cf = null;
  Queue dest = null;
  
  void admin() throws Exception {
    cf = TcpConnectionFactory.create("localhost", 2560);
    AdminModule.connect(cf, "root", "root");   
    User.create("anonymous", "anonymous", 0);
    dest = Queue.create("queue");
    dest.setFreeReading();
    dest.setFreeWriting();
    AdminModule.disconnect();
  }
  
  void test1() throws InterruptedException, JMSException {
    long deliverydelay = 0L;
    long timeBeforeSend = 0L;
    long timeAfterReceive = 0L;
    long jmsDeliveryTimeMsgSent = 0L;
    long jmsDeliveryTimeMsgReceived = 0L;

    JMSContext consCtx = cf.createContext(JMSContext.AUTO_ACKNOWLEDGE);
    JMSConsumer consumer = consCtx.createConsumer(dest);
    consCtx.start();

    JMSContext prodCtx = cf.createContext(JMSContext.AUTO_ACKNOWLEDGE);
    JMSProducer producer = prodCtx.createProducer();
    prodCtx.start();
    
    producer.setDeliveryDelay(10000);
    producer.setDeliveryMode(DeliveryMode.PERSISTENT);
    producer.setPriority(Message.DEFAULT_PRIORITY);
    producer.setTimeToLive(0L);
    TextMessage sent = prodCtx.createTextMessage("test redeliveryTime");
    timeBeforeSend = System.currentTimeMillis();
    producer.send(dest, sent);
    deliverydelay = producer.getDeliveryDelay();
    jmsDeliveryTimeMsgSent = sent.getJMSDeliveryTime();
    
    // Wait to receive the message.
    TextMessage received = (TextMessage) consumer.receive(11000);
    if (received != null) {
      timeAfterReceive = System.currentTimeMillis();
      jmsDeliveryTimeMsgReceived = received.getJMSDeliveryTime();
    }
    
    System.out.println("deliverydelay = " + deliverydelay);
    System.out.println("JMSDeliveryTime after send = " + jmsDeliveryTimeMsgSent);
    System.out.println("JMSDeliveryTime after receive = " + jmsDeliveryTimeMsgReceived);
    System.out.println("gmtTimeAfterSend after send = " + timeBeforeSend);
    System.out.println("gmtTimeAfterRecv after receive = " + timeAfterReceive);
    
    assertTrue("JMSDeliveryTime after send is not set", (jmsDeliveryTimeMsgSent != 0));
    assertTrue("JMSDeliveryTime after receive is not set", (jmsDeliveryTimeMsgReceived != 0));
    assertTrue("JMSDeliveryTimeAfterSend != JMSDeliveryTimeAfterRecv", (jmsDeliveryTimeMsgSent == jmsDeliveryTimeMsgReceived));
    assertTrue("The message is received before the delivery delay", (timeAfterReceive >= (timeBeforeSend + deliverydelay)));

    consCtx.close();
    prodCtx.close();
  }
  
  void test2() throws InterruptedException, JMSException {
    long deliverydelay = 0L;
    long timeBeforeSend = 0L;
    long timeAfterReceive = 0L;
    long jmsDeliveryTimeMsgSent = 0L;
    long jmsDeliveryTimeMsgReceived = 0L;

    JMSContext consCtx = cf.createContext(JMSContext.AUTO_ACKNOWLEDGE);
    JMSConsumer consumer = consCtx.createConsumer(dest);
    consCtx.start();

    JMSContext prodCtx = cf.createContext(JMSContext.AUTO_ACKNOWLEDGE);
    JMSProducer producer = prodCtx.createProducer();
    prodCtx.start();
    
    producer.setDeliveryDelay(10000);
    producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
    producer.setPriority(Message.DEFAULT_PRIORITY);
    producer.setTimeToLive(0L);
    TextMessage sent = prodCtx.createTextMessage("test redeliveryTime");
    timeBeforeSend = System.currentTimeMillis();
    producer.send(dest, sent);
    deliverydelay = producer.getDeliveryDelay();
    jmsDeliveryTimeMsgSent = sent.getJMSDeliveryTime();
    
    // Wait to receive the message.
    TextMessage received = (TextMessage) consumer.receive(11000);
    if (received != null) {
      timeAfterReceive = System.currentTimeMillis();
      jmsDeliveryTimeMsgReceived = received.getJMSDeliveryTime();
    }

    System.out.println("deliverydelay = " + deliverydelay);
    System.out.println("JMSDeliveryTime after send = " + jmsDeliveryTimeMsgSent);
    System.out.println("JMSDeliveryTime after receive = " + jmsDeliveryTimeMsgReceived);
    System.out.println("gmtTimeAfterSend after send = " + timeBeforeSend);
    System.out.println("gmtTimeAfterRecv after receive = " + timeAfterReceive);
    
    assertTrue("JMSDeliveryTime after send is not set", (jmsDeliveryTimeMsgSent != 0));
    assertTrue("JMSDeliveryTime after receive is not set", (jmsDeliveryTimeMsgReceived != 0));
    assertTrue("JMSDeliveryTimeAfterSend != JMSDeliveryTimeAfterRecv", (jmsDeliveryTimeMsgSent == jmsDeliveryTimeMsgReceived));
    assertTrue("The message is received before the delivery delay", (timeAfterReceive >= (timeBeforeSend + deliverydelay)));

    consCtx.close();
    prodCtx.close();
  }
}
