/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C) 2013 - 2024 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): 
 */
package joram.jms2;

import jakarta.jms.CompletionListener;
import jakarta.jms.ConnectionFactory;
import jakarta.jms.IllegalStateRuntimeException;
import jakarta.jms.JMSConsumer;
import jakarta.jms.JMSContext;
import jakarta.jms.JMSException;
import jakarta.jms.JMSProducer;
import jakarta.jms.Message;
import jakarta.jms.TextMessage;

import org.ow2.joram.jakarta.jms.Queue;
import org.ow2.joram.jakarta.jms.admin.AdminModule;
import org.ow2.joram.jakarta.jms.admin.User;
import org.ow2.joram.jakarta.jms.tcp.TcpConnectionFactory;

import framework.TestCase;

/**
 * Test calling commit and rollback from CompletionListener (expect IllegalStateRuntimeException).
 */
public class CompletionListenerTest4 extends TestCase implements CompletionListener{
  public static void main(String[] args) {
    new CompletionListenerTest4().run();
  }

  public static final boolean cfAsync = Boolean.getBoolean("CF_ASYNC");

  JMSContext context = null;
  
  public void run()  {
    try {
      startAgentServer((short) 0);
      Thread.sleep(1000);

      ConnectionFactory cf = TcpConnectionFactory.create("localhost", 2560);
      AdminModule.connect(cf, "root", "root");   
      User.create("anonymous", "anonymous", 0);
      Queue queue = Queue.create("queue");
      queue.setFreeReading();
      queue.setFreeWriting();
      AdminModule.disconnect();

      System.out.println("cfAsync=" + cfAsync);
      ((org.ow2.joram.jakarta.jms.ConnectionFactory) cf).getParameters().asyncSend = cfAsync;
      context = cf.createContext(JMSContext.SESSION_TRANSACTED);
      JMSProducer producer = context.createProducer();
      producer.setAsync(this);
      JMSConsumer consumer = context.createConsumer(queue);
      context.start();

      {
        // Test to commit the context from CompletionListener.
        TextMessage msg1 = context.createTextMessage("test1");
        producer.send(queue, msg1);
        TextMessage msg2 = context.createTextMessage("test2");
        producer.send(queue, msg2);
        context.commit();
        System.out.println("test1&2 sent");
        
        {
          System.out.println("waits test1");
          Message recv1 = consumer.receive(2000L);
          System.out.println("receives: " + recv1);
          assertTrue("Should receive message", (recv1 != null));
          if (recv1 != null)
            assertTrue("Should receive test1 != " + ((TextMessage) recv1).getText(), ((TextMessage) recv1).getText().equals("test1"));
        }
        
        {
          System.out.println("waits test2");
          Message recv2 = consumer.receive(2000L);
          System.out.println("receives: " + recv2);
          assertTrue("Should receive message", (recv2 != null));
          if (recv2 != null)
            assertTrue("Should receive test2 != " + ((TextMessage) recv2).getText(), ((TextMessage) recv2).getText().equals("test2"));
        }
        
        assertTrue("Should expect message completion", nbmsg == 2);
      }

      {
        // Test to commit the context from CompletionListener.
        TextMessage msg = context.createTextMessage("commit");
        producer.send(queue, msg);
        // TODO (AF): The commit below involves a deadlock with the commit
        // in CompletionListener !!
        context.commit();
        System.out.println("commit sent");
        Thread.sleep(2000L);
        assertTrue("Should expect message completion", nbmsg == 3);
      }

      {
        // Test to rollback the context from CompletionListener.
        TextMessage msg = context.createTextMessage("rollback");
        producer.send(queue, msg);
        context.commit();
        System.out.println("rollback sent");
        Thread.sleep(2000L);
        assertTrue("Should expect message completion", nbmsg == 4);
      }
      
      context.close();
      System.out.println("Context closed: " + nbmsg);
    } catch (Throwable exc) {
      exc.printStackTrace();
      error(exc);
    } finally {
      stopAgentServer((short) 0);
      endTest();     
    }
  }
  
  volatile int nbmsg = 0;
  
  @Override
  public void onCompletion(Message message) {
    try {
      System.out.println("onCompletion: " + ((TextMessage) message).getText());
      if ("commit".equals(((TextMessage) message).getText())) {
        try {
          context.commit();
          assertTrue("expect an IllegalStateRuntimeException.", false);
        } catch (Exception e) {
          System.out.println("Exception on message commit: " + e);
          assertTrue("Caught unexpected exception: " + e.getMessage(), e instanceof IllegalStateRuntimeException);
        }
      } else if ("rollback".equals(((TextMessage) message).getText())) {
        try {
          context.rollback();
          assertTrue("expect an IllegalStateRuntimeException.", false);
        } catch (Exception e) {
          System.out.println("Exception on message rollback: " + e);
          assertTrue("Caught unexpected exception: " + e.getMessage(), e instanceof IllegalStateRuntimeException);
        }
      }
      System.out.println("End of completion..");
      nbmsg += 1;
    } catch (JMSException e) {
      exception(e);
    }
  }

  @Override
  public void onException(Message message, Exception exc) {
    exception(exc);
  }
}
