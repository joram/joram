/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C)  2016 - 2024 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s):
 * Contributor(s): 
 */
package joram.jms2;

import jakarta.jms.ConnectionFactory;
import jakarta.jms.DeliveryMode;
import jakarta.jms.JMSConsumer;
import jakarta.jms.JMSContext;
import jakarta.jms.JMSException;
import jakarta.jms.JMSProducer;
import jakarta.jms.Message;
import jakarta.jms.TextMessage;

import org.ow2.joram.jakarta.jms.Topic;
import org.ow2.joram.jakarta.jms.admin.AdminModule;
import org.ow2.joram.jakarta.jms.admin.User;
import org.ow2.joram.jakarta.jms.tcp.TcpConnectionFactory;

import framework.TestCase;

/**
 * Test the delivery delay.
 */
public class DeliveryDelay_T extends TestCase implements jakarta.jms.MessageListener {

  public static void main(String[] args) {
    new DeliveryDelay_T().run();
  }
  
  JMSContext context;
  JMSConsumer consumer;
  
  public void run() {
    try {
      startAgentServer((short) 0);
      Thread.sleep(1000);
      
      admin();
      test();
      
      Thread.sleep(1000);
      stopAgentServer((short) 0);
      Thread.sleep(1000);
      startAgentServer((short) 0);
      Thread.sleep(1000);

      System.out.println("Server restarted.");
      test();
      
      assertTrue("Should receive 2 messages: " + nbrecv, (nbrecv == 2));
    } catch (Throwable exc) {
      exc.printStackTrace();
      error(exc);
    } finally {
      stopAgentServer((short) 0);
      endTest();     
    }
  }
  
  ConnectionFactory cf = null;
  Topic dest = null;
  
  void admin() throws Exception {
    cf = TcpConnectionFactory.create("localhost", 2560);
    AdminModule.connect(cf, "root", "root");   
    User.create("anonymous", "anonymous", 0);
    dest = Topic.create("topic");
    dest.setFreeReading();
    dest.setFreeWriting();
    AdminModule.disconnect();
  }

  long send = 0L;
  long recv = 0L;

  void test() throws InterruptedException {
    context = cf.createContext(JMSContext.AUTO_ACKNOWLEDGE);
    consumer = context.createConsumer(dest);
    consumer.setMessageListener(this);
    context.start();

    JMSContext prodCtx = cf.createContext();
    JMSProducer producer = prodCtx.createProducer();
    producer.setDeliveryMode(DeliveryMode.PERSISTENT);
    producer.setDeliveryDelay(5000L);
    TextMessage msg = prodCtx.createTextMessage("test redeliveryTime");
    send = System.currentTimeMillis();
    producer.send(dest, msg);

    // Wait to receive the message.
    Thread.sleep(6000);

    assertTrue("The rollback or recover message not received after the redelivery delay", (recv - send) > 5000);

    context.close();
    prodCtx.close();
  }
  
  int nbrecv = 0;
  
  public void onMessage(Message message) {
    try {
      nbrecv += 1;
      System.out.println("#" + nbrecv + " - " + message.getJMSMessageID() + ", JMSRedelivered = " + message.getJMSRedelivered() + 
                         ", JMSTimestamp = " + message.getJMSTimestamp());
      System.out.println(System.currentTimeMillis() + ": message received deliveryTime = " + message.getJMSDeliveryTime());
    } catch (JMSException e) {
      e.printStackTrace();
    }
    recv = System.currentTimeMillis();
  }

}
