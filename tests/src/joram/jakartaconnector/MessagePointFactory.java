
/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C) 2007 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): BADOLLE Fabien ( ScalAgent Distributed Technologies )
 * Contributor(s):
 */
package joram.jakartaconnector;

import java.lang.reflect.Method;

import jakarta.resource.spi.UnavailableException;
import jakarta.resource.spi.endpoint.MessageEndpoint;
import jakarta.resource.spi.endpoint.MessageEndpointFactory;
import javax.transaction.xa.XAResource;

public class MessagePointFactory implements MessageEndpointFactory {

  public MessageEndpoint createEndpoint(XAResource xaResource) throws UnavailableException {
    return new MessagePoint() ;
  }
  
  public MessageEndpoint createEndpoint(XAResource xaResource, long timeout) throws UnavailableException {
    return new MessagePoint() ;
  }
  
  public boolean isDeliveryTransacted(Method method) throws NoSuchMethodException {
    return false;
  }
  
  public String getActivationName() {
    return "JoramMQ";
  }

  /**
   * Return the <code>Class</code> object corresponding to the message
   * endpoint class. For example, for a Message Driven Bean this is the 
   * <code>Class</code> object corresponding to the application's MDB class.  
   * 
   * The resource adapter may use this to introspect the 
   * message endpoint class to discover annotations, interfaces implemented, 
   * etc. and modify the behavior of the resource adapter accordingly.
   * 
   * A return value of <code>null</code> indicates that the 
   * <code>MessageEndpoint</code> doesn't implement the business methods of 
   * underlying message endpoint class. 
   * 
   * @return A <code>Class</code> corresponding to the message endpoint class.
   * 
   * @since 1.7
   */
  public Class<?> getEndpointClass() {
    return MessageEndpoint.class;
  }
}
