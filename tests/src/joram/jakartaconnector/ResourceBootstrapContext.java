/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C) 2007 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): BADOLLE Fabien ( ScalAgent Distributed Technologies )
 * Contributor(s):
 */
package joram.jakartaconnector;

import java.util.Timer;

import jakarta.resource.spi.BootstrapContext;
import jakarta.resource.spi.UnavailableException;
import jakarta.resource.spi.XATerminator;
import jakarta.resource.spi.work.WorkContext;
import jakarta.resource.spi.work.WorkManager;
import jakarta.transaction.TransactionSynchronizationRegistry;

public class ResourceBootstrapContext implements BootstrapContext {
  private WorkManager wrkMgr = null;

  public ResourceBootstrapContext(WorkManager wm) {
    wrkMgr = wm;
  }

  /**
   * A resource adapter can check an application server's support 
   * for a particular WorkContext type through this method. 
   * This mechanism enables a resource adapter developer to
   * dynamically change the WorkContexts submitted with a Work instance 
   * based on the support provided by the application server.
   *
   * The application server must employ an exact type equality check (that is
   * <code>java.lang.Class.equals(java.lang.Class)</code> check) in
   * this method, to check if it supports the WorkContext type provided
   * by the resource adapter. This method must be idempotent, that is all 
   * calls to this method by a resource adapter for a particular 
   * <code>WorkContext</code> type must return the same boolean value 
   * throughout the lifecycle of that resource adapter instance.
   * 
   * @param workContextClass The WorkContext type that is tested for
   * support by the application server.
   *     
   * @return true if the <code>workContextClass</code> is supported
   * by the application server. false if the <code>workContextClass</code>
   * is unsupported or unknown to the application server.
   *
   * @since 1.6
   */
  @Override
  public boolean isContextSupported(Class<? extends WorkContext> workContextClass) {
    return false;
  }


  /**
   * Provides a handle to a <code>TransactionSynchronization</code> instance. The
   * <code>TransactionSynchronizationRegistry</code> instance could be used by a 
   * resource adapter to register synchronization objects, get transaction state and
   * status etc. This interface is implemented by the application server by a 
   * stateless service object. The same object can be used by any number of 
   * resource adapter objects with thread safety. 
   *
   * @return a <code>TransactionSynchronizationRegistry</code> instance.
   * @since 1.6
   */
  @Override
  public TransactionSynchronizationRegistry getTransactionSynchronizationRegistry() {
    return (TransactionSynchronizationRegistry) null;
  }

  /**
   * Creates a new <code>java.util.Timer</code> instance. The
   * <code>Timer</code> instance could be used to perform periodic 
   * <code>Work</code> executions or other tasks.
   *
   * @throws UnavailableException indicates that a 
   * <code>Timer</code> instance is not available. The 
   * request may be retried later.
   *
   * @return a new <code>Timer</code> instance.
   */
  @Override
  public Timer createTimer() throws UnavailableException {
    return new Timer(true);
  }


  /**
   * Provides a handle to a <code>WorkManager</code> instance. The
   * <code>WorkManager</code> instance could be used by a resource adapter to
   * do its work by submitting <code>Work</code> instances for execution. 
   *
   * @return a <code>WorkManager</code> instance.
   */
  @Override
  public WorkManager getWorkManager() {
    return wrkMgr;
  }

  /**
   * Provides a handle to a <code>XATerminator</code> instance. The
   * <code>XATerminator</code> instance could be used by a resource adapter 
   * to flow-in transaction completion and crash recovery calls from an EIS.
   *
   * @return a <code>XATerminator</code> instance.
   */
  @Override
  public XATerminator getXATerminator() {
    return null;
  }
}
