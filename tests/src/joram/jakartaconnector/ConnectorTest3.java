/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C) 2007 - 2012 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): BADOLLE Fabien ( ScalAgent Distributed Technologies )
 * Contributor(s):
 */
package joram.jakartaconnector;

import jakarta.jms.Connection;
import jakarta.jms.Session;
import jakarta.jms.MessageProducer;
import jakarta.jms.Queue;
import jakarta.jms.TextMessage;

import javax.naming.Context;
import javax.naming.InitialContext;

import org.ow2.joram.jakarta.connector.ActivationSpecImpl;
import org.ow2.joram.jakarta.connector.JoramAdapter;
import org.ow2.joram.jakarta.connector.ManagedConnectionFactoryImpl;
import org.ow2.joram.jakarta.connector.ManagedConnectionImpl;
import org.ow2.joram.jakarta.connector.OutboundConnection;
import org.ow2.joram.jakarta.connector.OutboundConsumer;
import org.ow2.joram.jakarta.connector.OutboundProducer;
import org.ow2.joram.jakarta.connector.OutboundSession;
import org.ow2.joram.jakarta.jms.ConnectionFactory;
import org.ow2.joram.jakarta.jms.admin.AdminModule;

import framework.TestCase;

/**
 * JCA Connector test with a colocated Joram server.
 */
public class ConnectorTest3 extends TestCase {
  static boolean colocated = false;
  
  public static void main(String[] args) throws Exception {
    new ConnectorTest3().run();
  }

  public void run() {
    try{
      colocated = Boolean.getBoolean("colocated");
      System.out.println("colocated=" + colocated);
      
      JoramAdapter ja= new JoramAdapter() ;
      ja.setName("ra");
      
      if (! colocated) {
        startAgentServer((short) 0);
        ja.setHostName("localhost");
        ja.setServerPort(17010);
      } else {
      	ja.setStartJoramServer(true);
      	ja.setStorage("s0");
      }
      
      ja.setServerId((short) 0);
      ja.setPlatformConfigDir(".");
      ja.setAdminFileXML("joramAdmin.xml");
      ja.setAdminFileExportXML("joramAdminExport.xml");
      
      ja.setCollocated(Boolean.valueOf(colocated));
      ja.start(new ResourceBootstrapContext(new JWorkManager(1, 5, 5000)));
      
      Thread.sleep(5000);
      
      Context ictx = new InitialContext();
      ConnectionFactory cf = (ConnectionFactory) ictx.lookup("JCF");
      Queue queue = (Queue) ictx.lookup("sampleQueue");
      assertTrue("queue not found", queue != null);
      ictx.close();

      ManagedConnectionFactoryImpl mcf = new ManagedConnectionFactoryImpl();
      mcf.setResourceAdapter(ja);
      mcf.setUserName("anonymous");
      mcf.setPassword("anonymous");
      if (! colocated) {
      	mcf.setCollocated(false);
      	mcf.setHostName("localhost");
      	mcf.setServerPort(17010);
      } else {
      	mcf.setCollocated(true);
      }
      ManagedConnectionImpl mci = (ManagedConnectionImpl) mcf.createManagedConnection(null,null);
      OutboundConnection oc = (OutboundConnection) mci.getConnection(null,null);
      final OutboundSession os =(OutboundSession) oc.createSession(false,0);
      OutboundConsumer cons = (OutboundConsumer) os.createConsumer(queue);

      oc.start();

      MessagePointFactory  mep = new MessagePointFactory();
      ActivationSpecImpl spec = new ActivationSpecImpl();
      spec.setResourceAdapter(ja);
      spec.setDestinationType("jakarta.jms.Queue");
      spec.setDestination("sampleQueue");
              
      ja.endpointActivation(mep , spec);    // listener on queue

      assertTrue("counter=" + ConnectorTest1.counter1 + " should be 0", ConnectorTest1.counter1 == 0);
      
      Connection cnx = cf.createConnection("anonymous", "anonymous");
      Session session = cnx.createSession(true, Session.SESSION_TRANSACTED);
      // create a producer
      MessageProducer producer = session.createProducer(queue);
      cnx.start();
      
      int nbmsg = 100;
      // create a message sent to the queue by the producer 
      for (int i=0; i<nbmsg; i++) {
        TextMessage msg = session.createTextMessage("with queue #" +i);
        msg.setIntProperty("idx",i);
        producer.send(msg);
      }
      session.commit();
      System.out.println("messages sent");

      
      cnx.close();

      Thread.sleep(5000); // wait onMessage
      assertTrue("counter=" + ConnectorTest1.counter1 + " should be " + nbmsg, ConnectorTest1.counter1 == nbmsg);
      
      ja.stop();
    } catch(Throwable exc) {
      exc.printStackTrace();
      error(exc);
    } finally {
      if (!colocated)
        stopAgentServer((short) 0);
      endTest();
    }
  }
  
  // TODO (AF): Unused the MessagePoint class directly references the ConnectorTest1 class!!
//  private static int counter = 0;
//  
//  public static synchronized void countMessages(String text, int idx) {
//    assertTrue("content is null", (text != null));
//
//    if (text == null) return;
//    
//    if (text.startsWith("with queue")) {
//      counter += 1;
//    } else {
//      assertTrue("Bad content: " + text, false);
//    }
//  }
}


