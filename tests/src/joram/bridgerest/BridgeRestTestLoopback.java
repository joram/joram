/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C)  2017 - 2022 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s):
 * Contributor(s): 
 */
package joram.bridgerest;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;

import org.objectweb.joram.client.jms.Queue;
import org.objectweb.joram.client.jms.admin.AdminModule;
import org.objectweb.joram.client.jms.admin.RestAcquisitionQueue;
import org.objectweb.joram.client.jms.admin.RestDistributionQueue;
import org.objectweb.joram.client.jms.admin.User;
import org.objectweb.joram.client.jms.tcp.TcpConnectionFactory;

import framework.TestCase;

/**
 * Test: Test the rest bridge behavior during stop / restart of bridge server (#0).
 *  - Architecture: producer --JMS--> [#0 DistributionQueue] --REST--> [#0 foreignQueue]
 *                  consumer <--JMS-- [#0 AcquisitionQueue]  <--REST-- [#0 foreignQueue]
 *  - Sends 1000 messages on a DistributionQueue
 *  - Stops the Joram bridge server, then restarts it.
 *  - Receives the messages through an AcquisitionQueue.
 * Note: This test can either use the administration API or XML script.
 * Note: This test is similar to BridgeRestTest6.
 */
public class BridgeRestTestLoopback extends TestCase implements MessageListener {
  public static void main(String[] args) {
    new BridgeRestTestLoopback().run();
  }

  public void startAgentServer0() throws Exception {
    System.setProperty("felix.config.properties", "file:config_loopback.properties");
    startAgentServer((short)0);
  }

  public void run() {
    try {
      System.out.println("servers start");
      startAgentServer0();
      Thread.sleep(1000);
      
      admin();
      System.out.println("admin config ok");
      Thread.sleep(1000);
      
      test();

      long start = System.currentTimeMillis();
      System.out.println("Stop server.");
      stopAgentServer((short)0);
      System.out.println("Server stopped: " + (System.currentTimeMillis() - start));
    } catch (Throwable exc) {
      exc.printStackTrace();
      error(exc);
    } finally {
      System.out.println("Server stop ");
      killAgentServer((short)0);
      endTest(); 
    }
  }

  
  private void admin() throws Exception {
    javax.jms.ConnectionFactory cf = TcpConnectionFactory.create("localhost", 16010);

    AdminModule.connect(cf, "root", "root");
    javax.naming.Context jndiCtx = new javax.naming.InitialContext();

    User.create("anonymous", "anonymous", 0);

    // create The foreign destination and connectionFactory on server 1
    Queue foreignQueue = Queue.create(0, "foreignQueue");
    foreignQueue.setFreeReading();
    foreignQueue.setFreeWriting();
    System.out.println("foreign queue = " + foreignQueue);

    // bind foreign destination and connectionFactory
    jndiCtx.rebind("foreignQueue", foreignQueue);
    jndiCtx.rebind("foreignCF", cf);

    // /!\ Be careful, do not use not async mode in loopback tests.
    // This leads to deadlocks in the engine. The bridge destination makes requests to the Rest/JMS connector which
    // wishes to interact with the MOM while it is blocked in the reaction of the bridge destination.
    
    // Create a REST acquisition queue on server.
    Queue distQueue = new RestDistributionQueue()
        .setHost("localhost")
        .setPort(8989)
        .setIdleTimeout(10)
        .create(0, "distQueue", "foreignQueue");
    distQueue.setFreeWriting();
    System.out.println("joram distribution queue = " + distQueue);

    // Create a REST acquisition queue on server.
    Queue acqQueue = new RestAcquisitionQueue()
        .setHost("localhost")
        .setPort(8989)
        .setMediaTypeJson(true)
        .setTimeout(5000)
        .setIdleTimeout(10)
        .create(0, "acqQueue", "foreignQueue");
    acqQueue.setFreeReading();
    System.out.println("joram acquisition queue = " + acqQueue);

    jndiCtx.bind("acqQueue", acqQueue);
    jndiCtx.bind("distQueue", distQueue);
    jndiCtx.rebind("bridgeCF", cf);
    jndiCtx.close();

    AdminModule.disconnect();
  }
  
  public static final int NB_MSG = 100;
  public static final Object lock = new Object();
  
  boolean ended = false;
  
  void test() throws Exception {
    Context jndiCtx = new InitialContext();
    ConnectionFactory bridgeCF = (ConnectionFactory) jndiCtx.lookup("bridgeCF");
    Destination acqQueue = (Destination) jndiCtx.lookup("acqQueue");
    Destination distQueue = (Destination) jndiCtx.lookup("distQueue");
    jndiCtx.close();

    Connection cnx = bridgeCF.createConnection();

    Session session1 = cnx.createSession(false, Session.AUTO_ACKNOWLEDGE);
    MessageProducer producer = session1.createProducer(distQueue);

    Session session2 = cnx.createSession(false, Session.AUTO_ACKNOWLEDGE);
    MessageConsumer consumer = session2.createConsumer(acqQueue);
    consumer.setMessageListener(this);

    cnx.start(); 

    TextMessage msg = session1.createTextMessage();
    for (int i=0; i < NB_MSG; i++) {
//      System.out.println("Send msg #" + i);
      msg.setText("Message number #" + i);
      producer.send(msg);
    }
    System.out.println(NB_MSG + " messages sent.");

    int nbtry = 0;
    synchronized (lock) {
      while ((nbtry <60) && !ended)
        lock.wait(1000L);
    }
    System.out.println("Receives " + nbmsg + " messages.");
    assertEquals("Receives " + nbmsg + " messages, should be " + NB_MSG, NB_MSG, nbmsg);

    cnx.close();
  }
  
  int nbmsg = 0;
  
  String previous = null;
  String current = null;
  
  public void onMessage(Message msg) {
    try {
      String current = "Message number #" + nbmsg;
      String txt = ((TextMessage) msg).getText();
      
//      System.out.println(txt);
      if (! current.equals(txt)) {
        // Verify if it is a duplicate due to the server's failure.
        if ((previous == null) || (! previous.equals(txt))) {
          System.out.println("Expected <" + current + "> but was <" + txt + "> ");
          assertEquals("Message " + msg.getJMSMessageID(), current, txt);
        } else {
          System.out.println("Duplicate message: " + msg.getJMSMessageID() + " -> " + txt);
          return;
        }
      }
      
      previous = current;
      if (nbmsg % 100 == 0)
        System.out.println("Received " + nbmsg + " messages.");
      
      nbmsg += 1;
      if (nbmsg == NB_MSG) {
        synchronized (lock) {
          ended = true;
          System.out.println("notify");
          lock.notify(); 
        }
      }
    } catch (JMSException e) {
      assertTrue("Exception: " + e, false);
      e.printStackTrace();
    }
  }
}

