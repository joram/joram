/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C)  2024 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): ScalAgent D.T.
 * Contributor(s): 
 */
package joram.bridgerest;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import javax.naming.*;

import org.objectweb.joram.client.jms.Queue;
import org.objectweb.joram.client.jms.admin.AdminModule;
import org.objectweb.joram.client.jms.admin.RestAcquisitionQueue;
import org.objectweb.joram.client.jms.admin.RestDistributionQueue;
import org.objectweb.joram.client.jms.admin.User;
import org.objectweb.joram.client.jms.tcp.TcpConnectionFactory;

import framework.TestCase;

/**
 * Test: Test the behavior of BridgeDistributionQueue and BridgeAcquisitionQueue during a long run.
 */
public class BridgeRestTest13 extends TestCase implements MessageListener {
  public static void main(String[] args) {
    new BridgeRestTest13().run();
  }

  public void startAgentServer0() throws Exception {
    System.setProperty("felix.config.properties", "file:config0.properties");
    startAgentServer((short)0);
  }

  public void startAgentServer1() throws Exception {
    System.setProperty("felix.config.properties", "file:config1.properties");
    startAgentServer((short)1);
  }

  public void run() {
    try {
      System.out.println("servers start");
      startAgentServer0();
      startAgentServer1();
      Thread.sleep(1000);

      admin();
      test();
    } catch (Throwable exc) {
      exc.printStackTrace();
      error(exc);
    } finally {
      System.out.println("Server stop ");
      killAgentServer((short)0);
      killAgentServer((short)1);
      endTest(); 
    }
  }

  private final static String foreignQueueName = "foreignQueue";
  private final static String acqQueueName = "acqQueue";
  private final static String distQueueName = "distQueue";
  
  private void admin() throws Exception {
    javax.jms.ConnectionFactory bridgeCF = TcpConnectionFactory.create("localhost", 16010);

    AdminModule.connect(bridgeCF, "root", "root");
    javax.naming.Context jndiCtx = new javax.naming.InitialContext();

    User.create("anonymous", "anonymous", 0);
    User.create("anonymous", "anonymous", 1);

    // create The foreign destination and connectionFactory on server 1
    Queue foreignQueue = Queue.create(1, foreignQueueName);
    foreignQueue.setFreeReading();
    foreignQueue.setFreeWriting();
    System.out.println("foreign queue = " + foreignQueue);

    javax.jms.ConnectionFactory foreignCF = TcpConnectionFactory.create("localhost", 16011);

    // bind foreign destination and connectionFactory
    Context tmpCtx = jndiCtx.createSubcontext("tmp");
    tmpCtx.rebind(foreignQueueName, foreignQueue);
    jndiCtx.rebind("foreignCF", foreignCF);

    // Create a REST distribution queue on server.
    Queue distQueue = new RestDistributionQueue()
        .setHost("localhost")
        .setPort(8989)
        .setIdleTimeout(10)
        .create(0, distQueueName, "tmp/" + foreignQueueName);
    distQueue.setFreeWriting();
    System.out.println("joram distribution queue = " + distQueue);

    // Create a REST acquisition queue on server.
    Queue acqQueue = new RestAcquisitionQueue()
        .setMediaTypeJson(true)
        .setTimeout(5000)
        .setIdleTimeout(10)
        .create(0, acqQueueName, "tmp/" + foreignQueueName);
    acqQueue.setFreeReading();
    System.out.println("joram acquisition queue = " + acqQueue);

    jndiCtx.bind(acqQueueName, acqQueue);
    jndiCtx.bind(distQueueName, distQueue);
    jndiCtx.rebind("bridgeCF", bridgeCF);
    jndiCtx.close();

    AdminModule.disconnect();

    System.out.println("admin config ok");
    Thread.sleep(1000);
  }

  int nbmsg = 500000;
  Object lock = new Object();
  
  public void test() throws Exception {
    Context jndiCtx = new InitialContext();
    ConnectionFactory bridgeCF = (ConnectionFactory) jndiCtx.lookup("bridgeCF");
    Destination acqQueue = (Destination) jndiCtx.lookup(acqQueueName);
    Destination distQueue = (Destination) jndiCtx.lookup(distQueueName);
    jndiCtx.close();

    Connection bridgeCnx1 = bridgeCF.createConnection();
    Session bridgeSess1 = bridgeCnx1.createSession(false, Session.AUTO_ACKNOWLEDGE);
    MessageProducer bridgeProd = bridgeSess1.createProducer(distQueue);
    bridgeCnx1.start(); 

    Connection bridgeCnx2 = bridgeCF.createConnection();
    Session bridgeSess2 = bridgeCnx2.createSession(false, Session.AUTO_ACKNOWLEDGE);
    MessageConsumer bridgeCons = bridgeSess2.createConsumer(acqQueue);
    bridgeCons.setMessageListener(this);
    bridgeCnx2.start(); 

    TextMessage msg = bridgeSess1.createTextMessage();
    for (int i = 0; i < nbmsg; i++) {
      if ((i % 1000) == 0)
        System.out.println("Send msg #" + i);
      msg.setText("Message number #" + i);
      bridgeProd.send(msg);
      
      synchronized (lock) {
        if ((i - counter) > 10000)
          lock.wait((1L*(i - counter)));
      }
    }
    
    synchronized (lock) {
      if (counter != nbmsg)
        lock.wait((1000L*nbmsg) + 10000L);
    }
    assertTrue("Assertion#1", (counter == nbmsg));
    
    if (counter != nbmsg)
      throw new Exception("Bad message count");
  }
  
  int counter = 0;
  
  public void onMessage(Message msg) {
    try {
      String txt1 = "Message number #" + counter;
      String txt2 = ((TextMessage) msg).getText();
      if (! txt1.equals(txt2))
        System.out.println("Message " + msg.getJMSMessageID() + ": Expected <" + txt1 + "> but was <" + txt2 + "> ");
      assertEquals("Message " + msg.getJMSMessageID(), txt1, txt2);
      
      if ((counter % 1000) == 0)
        System.out.println("Receives " + msg.getJMSMessageID() + " -> " + txt2);
      
      counter += 1;
      if (counter == nbmsg) {
        synchronized (lock) {
          System.out.println("notify");
          lock.notify(); 
        }
      }
    } catch (JMSException exc) {
      error(exc);
      exc.printStackTrace();
    }
  }
}
