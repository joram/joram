/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C) 2006 - 2024 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s):  ScalAgent D.T.
 * Contributor(s): Badolle Fabien (ScalAgent D.T.)
 */
package joram.recovery;

import jakarta.jms.BytesMessage;
import jakarta.jms.Connection;
import jakarta.jms.ConnectionFactory;
import jakarta.jms.JMSException;
import jakarta.jms.MessageConsumer;
import jakarta.jms.MessageProducer;
import jakarta.jms.Queue;
import jakarta.jms.Session;
import joram.connection.BaseTest;

import java.util.BitSet;

import javax.naming.Context;
import javax.naming.InitialContext;

import org.objectweb.joram.shared.messages.Message;

import framework.TestCase;

/**
 * Test:
 *   - A first thread sending messages to a queue with SWAPALLOWED.
 *   - A second thread receiving the messages sent to the queue.
 *   - The broker is regularly killed and restarted.
 *   - We verify that all messages are received.
 */
public class Test11 extends TestCase {
  private ConnectionFactory cf;
  private Queue queue;

  int nbmsgs = Integer.getInteger("NBMESSAGES", 10000);
  int nbrounds = Integer.getInteger("NBROUNDS", 5);
  boolean swapAllowed = Boolean.getBoolean("SWAPALLOWED");
  
  BitSet received = new BitSet(nbmsgs);
  int nbdup = 0;
  int nberrors = 0;
  
  public static void main(String[] args) {
    new Test11().run();
  }

  transient int nbsent = 0;
  transient int nbreceived = 0;
  
  class Producer implements Runnable {
    String name;

    Connection cnx;
    Session session;
    MessageProducer producer;

    public Producer(String name) {
      this.name = name;
    }

    long start = System.currentTimeMillis();
    
    void connect()  {
      try {
        cnx = cf.createConnection();
        session = cnx.createSession(false, Session.AUTO_ACKNOWLEDGE);
        cnx.start();
        producer = session.createProducer(queue);
          logmsg(name + " reconnects: " + (System.currentTimeMillis()-start));
      } catch (JMSException exc) {}
    }

    @Override
    public void run() {
      BytesMessage msg = null;
      while (nbsent < nbmsgs) {
        try {
          while (cnx == null)
            connect();
          msg = session.createBytesMessage();
          if (swapAllowed)
            msg.setBooleanProperty(Message.SWAPALLOWED, true);
          msg.setIntProperty("index", nbsent);
          msg.writeUTF("message#" + nbsent);
          producer.send(msg);
          nbsent += 1;
          if ((nbsent %1000) == 0) logmsg("message sent #" + nbsent);
        } catch (JMSException exc) {
          cnx = null;
          logmsg(name + ", nbsent=" + nbsent + " -> " + exc.getMessage());
          start = System.currentTimeMillis();
        }
      }
    }
  }

  class Consumer implements Runnable {
    String name;

    Connection cnx;
    Session session;
    MessageConsumer consumer;

    public Consumer(String name) {
      this.name = name;
    }

    long start = System.currentTimeMillis();
    
    void connect() {
      try {
        cnx = cf.createConnection();
        session = cnx.createSession(false, Session.CLIENT_ACKNOWLEDGE);
        cnx.start();
        consumer = session.createConsumer(queue);
        logmsg(name + " reconnects: " + (System.currentTimeMillis()-start));
      } catch (JMSException exc) {}
    }

    @Override
    public void run() {
      BytesMessage msg;
      while (nbreceived < nbmsgs) {
        try {
          while (cnx == null)
            connect();
          msg = (BytesMessage) consumer.receive();
          String payload = msg.readUTF();
          int index = msg.getIntProperty("index");
          if (index != nbreceived) {
            logmsg("Receives " + index + " != "+ nbreceived + " (" + msg.getJMSRedelivered());
            if (index < nbreceived)
              nbdup += 1;
            else
              nberrors += 1;
            nbreceived = index;
          }
          received.set(index);
          msg.acknowledge();
          nbreceived += 1;
          if ((nbreceived %1000) == 0) logmsg("message received #" + nbreceived);
        } catch (JMSException exc) {
          cnx = null;
          logmsg(name + ", nbreceived=" + nbreceived + " -> " + exc.getMessage());
          start = System.currentTimeMillis();
        }
      }
    }
  }
  
  public void run() {
    try {
      logmsg("nbrounds=" + nbrounds + ", nbmsgs=" + nbmsgs);
      startAgentServer((short) 0);
      Thread.sleep(2000);
      admin();
      logmsg("admin config ok");

      Context ictx = new InitialContext();
      queue = (Queue) ictx.lookup("queue");
      cf = (ConnectionFactory) ictx.lookup("cf");
      ictx.close();

      Thread producer = new Thread(new Producer("producer1"));
      producer.start();

      Thread consumer = new Thread(new Consumer("consumer1"));
      consumer.start();

      int last = 0;
      for (int i=0; i<nbrounds; i++) {
        while (nbsent < ((i+1)*(nbmsgs/(nbrounds+1)))) {
          if (last != nbsent) {
            logmsg("Round#" + i + ", nbsent=" + nbsent + ", nbreceived=" + nbreceived);
            last = nbsent;
          }
          Thread.sleep(1000L);
        }
        logmsg("Next, Round#" + i + ", nbsent=" + nbsent + ", nbreceived=" + nbreceived);
        // kill and restart server
        logmsg("Server stop ");
        killAgentServer((short) 0);
        logmsg("server start");
        startAgentServer((short) 0);
      }
      while (nbreceived < nbmsgs) {
        if (last != nbsent) {
          logmsg("End, nbsent=" + nbsent + ", nbreceived=" + nbreceived);
          last = nbsent;
        }
        Thread.sleep(5000L);
      }
      logmsg("nberrors=" + nberrors + ", nbdup=" + nbdup);
      int nextMissing = 0;
      do {
        nextMissing = received.nextClearBit(nextMissing);
        if (nextMissing != nbmsgs)
          logmsg("missing=" + nextMissing);
      } while (nextMissing < nbmsgs);
      BaseTest.assertTrue(received.nextClearBit(0) == nbmsgs);
      BaseTest.assertTrue(nberrors == 0);
    } catch (Throwable exc) {
      exc.printStackTrace();
      error(exc);
    } finally {
      logmsg("Server stop ");
      stopAgentServer((short) 0);
      endTest();
    }
  }

  static long teststart = System.currentTimeMillis();
  
  private static void logmsg(String msg) {
    System.out.println("" + (System.currentTimeMillis()-teststart) + " -" + msg);
  }
  
  /**
   * Admin : Create queue and a user anonymous; use jndi
   */
  public void admin() throws Exception {
    org.ow2.joram.jakarta.jms.admin.AdminModule.connect("localhost", 2560, "root", "root", 60);
    
    org.ow2.joram.jakarta.jms.Queue queue = (org.ow2.joram.jakarta.jms.Queue) org.ow2.joram.jakarta.jms.Queue.create("queue");
    queue.setFreeReading();
    queue.setFreeWriting();
    
    org.ow2.joram.jakarta.jms.admin.User user = org.ow2.joram.jakarta.jms.admin.User.create("anonymous", "anonymous");

    jakarta.jms.ConnectionFactory cf = org.ow2.joram.jakarta.jms.tcp.TcpConnectionFactory.create("localhost", 2560);

    javax.naming.Context jndiCtx = new javax.naming.InitialContext();
    jndiCtx.bind("cf", cf);
    jndiCtx.bind("queue", queue);
    jndiCtx.close();

    org.ow2.joram.jakarta.jms.admin.AdminModule.disconnect();
  }
}
