/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C) 2022 - 2024 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): ScalAgent Distributed Technologies
 * Contributor(s): 
 */
package joram.noreg;

import jakarta.jms.Connection;
import jakarta.jms.ConnectionFactory;
import jakarta.jms.DeliveryMode;
import jakarta.jms.ExceptionListener;
import jakarta.jms.JMSException;
import jakarta.jms.MessageConsumer;
import jakarta.jms.MessageProducer;
import jakarta.jms.Session;
import jakarta.jms.TextMessage;

import org.ow2.joram.jakarta.jms.Queue;
import org.ow2.joram.jakarta.jms.admin.AdminModule;
import org.ow2.joram.jakarta.jms.admin.User;
import org.ow2.joram.jakarta.jms.local.LocalConnectionFactory;
import org.ow2.joram.jakarta.jms.tcp.TcpConnectionFactory;

import fr.dyade.aaa.agent.AgentServer;
import framework.TestCase;

/**
 * Test charset issues.
 */
public class EncodingTest2 extends TestCase implements ExceptionListener {

  public static void main(String[] args) {
    new EncodingTest2().run();
  }
  
  Queue queue;

  public void run() {
    try {
      AgentServer.init((short) 0, "s0", null);
      AgentServer.start();

      Thread.sleep(1000L);

      // Create an administration connection on server #0
      ConnectionFactory cf1 = LocalConnectionFactory.create();
      ((LocalConnectionFactory) cf1).getParameters().cnxPendingTimer = 5000;
      ((LocalConnectionFactory) cf1).getParameters().connectingTimer = 30;

      AdminModule.connect(cf1, "root", "root");
      queue = Queue.create(0, "queue");
      queue.setFreeReading();
      queue.setFreeWriting();

      // Create the anonymous user needed for test
      User.create("anonymous", "anonymous");
      AdminModule.disconnect();

      test(cf1);

      // Create an administration connection on server #0
      ConnectionFactory cf2 = TcpConnectionFactory.create("localhost", 16010);
      ((TcpConnectionFactory) cf2).getParameters().cnxPendingTimer = 5000;
      ((TcpConnectionFactory) cf2).getParameters().connectingTimer = 30;

      test(cf2);
    } catch (Throwable exc) {
      exc.printStackTrace();
      error(exc);
    } finally {
      killAgentServer((short) 0);
      endTest();     
    }
  }
  
  private void test(ConnectionFactory cf) throws Exception {
    Connection cnx = cf.createConnection("anonymous", "anonymous");
    cnx.setExceptionListener(this);

    Session session = cnx.createSession(false, Session.AUTO_ACKNOWLEDGE);
    MessageConsumer cons = session.createConsumer(queue);
    MessageProducer prod = session.createProducer(queue);
    prod.setDeliveryMode(DeliveryMode.PERSISTENT);
    cnx.start();

    String utf8Content = new String("abcdef \u20AC \uF93D\uF936\uF949\uF942");
    System.out.println("utf8Content: " + utf8Content);
    System.out.println("utf8Content: " + utf8Content.length() + "/" + utf8Content.getBytes().length + "/" + utf8Content.getBytes("UTF-8").length);

    TextMessage sent = session.createTextMessage(utf8Content);
    sent.setStringProperty("content", utf8Content);
    sent.setJMSCorrelationID(utf8Content);
    prod.send(sent);

    TextMessage recv = (TextMessage) cons.receive(1000L);
    System.out.println("Body: " + recv.getText());
    assertTrue("Body is not ok", utf8Content.equals(recv.getText()));

    String prop = recv.getStringProperty("content");
    System.out.println("Property: " + prop + " -> " + prop.length() + "/" + prop.getBytes().length + "/" + prop.getBytes("UTF-8").length);
    assertTrue("Property is not ok", utf8Content.equals(prop));

    prop = recv.getJMSCorrelationID();
    System.out.println("JMSCorrelationID: " + prop + " -> " + prop.length() + "/" + prop.getBytes().length + "/" + prop.getBytes("UTF-8").length);
    assertTrue("JMSCorrelationID is not ok", utf8Content.equals(recv.getJMSCorrelationID()));
  }

  @Override
  public void onException(JMSException e) {
    System.out.println("JMS NOK -> " + e.getMessage());
//    this.exc = e;
//    e.printStackTrace();
  }
}
