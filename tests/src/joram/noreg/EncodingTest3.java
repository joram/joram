/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C) 2022 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): ScalAgent Distributed Technologies
 * Contributor(s): 
 */
package joram.noreg;


import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.DeliveryMode;

import org.objectweb.joram.client.jms.Queue;
import org.objectweb.joram.client.jms.admin.AdminModule;
import org.objectweb.joram.client.jms.admin.User;
import org.objectweb.joram.client.jms.tcp.TcpConnectionFactory;

import framework.TestCase;

/**
 * Test charset issues.
 */
public class EncodingTest3 extends TestCase {

  public static void main(String[] args) {
    new EncodingTest3().run();
  }
  
  public void run() {
    try {
      startAgentServer((short) 0, new String[] {});
      Thread.sleep(1000);

      // Create an administration connection on server #0
      ConnectionFactory cf = TcpConnectionFactory.create("localhost", 16010);
      ((TcpConnectionFactory) cf).getParameters().cnxPendingTimer = 5000;
      ((TcpConnectionFactory) cf).getParameters().connectingTimer = 30;
      
      AdminModule.connect(cf, "root", "root");
      Queue queue = Queue.create(0, "queue");
      queue.setFreeReading();
      queue.setFreeWriting();

      // Create the anonymous user needed for test
      User.create("anonymous", "anonymous");
      AdminModule.disconnect();
      
      Thread.sleep(1000);
      
      Connection cnx = cf.createConnection("anonymous", "anonymous");
      
      Session session = cnx.createSession(false, Session.AUTO_ACKNOWLEDGE);
      MessageProducer prod = session.createProducer(queue);
      prod.setDeliveryMode(DeliveryMode.PERSISTENT);
      cnx.start();
      
      String utf8Content = "abcdef � \uF93D\uF936\uF949\uF942";
      System.out.println("utf8Content: " + utf8Content);
      System.out.println("utf8Content: " + utf8Content.length() + "/" + utf8Content.getBytes().length + "/" + utf8Content.getBytes("UTF-8").length);
      
      TextMessage sent = session.createTextMessage(utf8Content);
      sent.setStringProperty("content", utf8Content);
      sent.setJMSCorrelationID(utf8Content);
      prod.send(sent);
      
      cnx.close();
      
      stopAgentServer((short) 0);
      System.out.println("Server stopped.");
      Thread.sleep(1000);
      
      startAgentServer((short) 0, new String[] {
            "-Dfile.encoding=UTF-8"
            });
      System.out.println("Server started.");
      Thread.sleep(1000);
      
      cnx = cf.createConnection("anonymous", "anonymous");
      
      session = cnx.createSession(false, Session.AUTO_ACKNOWLEDGE);
      MessageConsumer cons = session.createConsumer(queue);
      cnx.start();
      
      TextMessage recv = (TextMessage) cons.receive(1000L);
      System.out.println("Body: " + recv.getText());
      assertTrue("Body is not ok", utf8Content.equals(recv.getText()));
      
      String prop = recv.getStringProperty("content");
      System.out.println("Property: " + prop + " -> " + prop.length() + "/" + prop.getBytes().length + "/" + prop.getBytes("UTF-8").length);
      assertTrue("Property is not ok", utf8Content.equals(prop));

      prop = recv.getJMSCorrelationID();
      System.out.println("JMSCorrelationID: " + prop + " -> " + prop.length() + "/" + prop.getBytes().length + "/" + prop.getBytes("UTF-8").length);
      assertTrue("JMSCorrelationID is not ok", utf8Content.equals(recv.getJMSCorrelationID()));
      
      System.out.println("JMS OK");
    } catch (Throwable exc) {
      exc.printStackTrace();
      error(exc);
    } finally {
      killAgentServer((short) 0);
      endTest();     
    }
  }
}
