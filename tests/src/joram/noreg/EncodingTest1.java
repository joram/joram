/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C) 2022 - 2024 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): ScalAgent Distributed Technologies
 * Contributor(s): 
 */
package joram.noreg;

import jakarta.jms.Connection;
import jakarta.jms.ConnectionFactory;
import jakarta.jms.DeliveryMode;
import jakarta.jms.ExceptionListener;
import jakarta.jms.JMSException;
import jakarta.jms.MessageConsumer;
import jakarta.jms.MessageProducer;
import jakarta.jms.Session;
import jakarta.jms.TextMessage;

import java.nio.charset.Charset;

import org.ow2.joram.jakarta.jms.Queue;
import org.ow2.joram.jakarta.jms.admin.AdminModule;
import org.ow2.joram.jakarta.jms.admin.User;
import org.ow2.joram.jakarta.jms.tcp.TcpConnectionFactory;

import framework.TestCase;

/**
 * Test charset issues.
 */
public class EncodingTest1 extends TestCase implements ExceptionListener {

  public static void main(String[] args) {
    new EncodingTest1().run();
  }
  
  public void run() {
    try {
      startAgentServer((short) 0, new String[] {
//                                                "-Dfile.encoding=UTF-8"
                                                });
      Thread.sleep(1000);
      
      Charset charset = Charset.defaultCharset();
      System.out.println(charset.name() + " = " + charset.newEncoder().maxBytesPerChar());
      charset = Charset.forName("UTF-8");
      System.out.println(charset.name() + " = " + charset.newEncoder().maxBytesPerChar());
      charset = Charset.forName("UTF-16");
      System.out.println(charset.name() + " = " + charset.newEncoder().maxBytesPerChar());
      charset = Charset.forName("ASCII");
      System.out.println(charset.name() + " = " + charset.newEncoder().maxBytesPerChar());
      charset = Charset.forName("ISO-8859-15");
      System.out.println(charset.name() + " = " + charset.newEncoder().maxBytesPerChar());

      // Create an administration connection on server #0
      ConnectionFactory cf = TcpConnectionFactory.create("localhost", 16010);
      ((TcpConnectionFactory) cf).getParameters().cnxPendingTimer = 5000;
      ((TcpConnectionFactory) cf).getParameters().connectingTimer = 30;
      
      AdminModule.connect(cf, "root", "root");
      Queue queue = Queue.create(0, "queue");
      queue.setFreeReading();
      queue.setFreeWriting();

      // Create the anonymous user needed for test
      User.create("anonymous", "anonymous");
      AdminModule.disconnect();
      
      Thread.sleep(1000);
      
      Connection cnx = cf.createConnection("anonymous", "anonymous");
      cnx.setClientID("ITSME");
      cnx.setExceptionListener(this);
      
      Session session = cnx.createSession(false, Session.AUTO_ACKNOWLEDGE);
      MessageConsumer cons = session.createConsumer(queue);
      MessageProducer prod = session.createProducer(queue);
      prod.setDeliveryMode(DeliveryMode.PERSISTENT);
      cnx.start();
      
      String utf8Content = "abcdef \u20AC \uF93D\uF936\uF949\uF942";
      System.out.println("utf8Content: " + utf8Content);
      System.out.println("utf8Content: " + utf8Content.length() + "/" + utf8Content.getBytes().length + "/" + utf8Content.getBytes("UTF-8").length);
      
      TextMessage sent = session.createTextMessage(utf8Content);
      sent.setStringProperty("content", utf8Content);
      sent.setJMSCorrelationID(utf8Content);
      prod.send(sent);
      
      TextMessage recv = (TextMessage) cons.receive(1000L);
      System.out.println("Body: " + recv.getText());
      assertTrue("Body is not ok", utf8Content.equals(recv.getText()));
      
      String prop = recv.getStringProperty("content");
      System.out.println("Property: " + prop + " -> " + prop.length() + "/" + prop.getBytes().length + "/" + prop.getBytes("UTF-8").length);
      assertTrue("Property is not ok", utf8Content.equals(prop));

      prop = recv.getJMSCorrelationID();
      System.out.println("JMSCorrelationID: " + prop + " -> " + prop.length() + "/" + prop.getBytes().length + "/" + prop.getBytes("UTF-8").length);
      assertTrue("JMSCorrelationID is not ok", utf8Content.equals(recv.getJMSCorrelationID()));
      
      System.out.println("JMS OK");
    } catch (Throwable exc) {
      exc.printStackTrace();
      error(exc);
    } finally {
      killAgentServer((short) 0);
      endTest();     
    }
  }

  @Override
  public void onException(JMSException e) {
    System.out.println("JMS NOK -> " + e.getMessage());
//    this.exc = e;
//    e.printStackTrace();
  }
}
