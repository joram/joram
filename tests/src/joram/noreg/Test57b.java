/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C) 2008 - 2024 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): ScalAgent Distributed Technologies
 * Contributor(s):
 */
package joram.noreg;

import java.util.Properties;

import javax.jms.BytesMessage;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;

import org.objectweb.joram.client.jms.Destination;
import org.objectweb.joram.client.jms.Queue;
import org.objectweb.joram.client.jms.admin.AdminModule;
import org.objectweb.joram.client.jms.admin.User;
import org.objectweb.joram.shared.DestinationConstants;

import framework.TestCase;


/**
 * Test a server restart with lot of waiting (big) messages:
 *   - Sends a big number of messages.
 *   - Stops and restarts the server.
 *   - Sends anew a big number of messages to verify performance (JORAM-358).
 *   - Stops and restarts the server.
 *   - Consumes all messages.
 * Verify that all messages are delivered in the right order.
 */
public class Test57b extends BaseTest {
  static int MsgSize = 1*1024*1024;
  static int NbMsg = 100;

  static Destination dest = null;
  static ConnectionFactory cf = null;

  static String host = "localhost";
  static int port = 16010;

  public static void main(String[] args) throws Exception {
    new Test57b().run(args);
  }

  public void run(String[] args) {
    String[] jmwargs = null;
    
    try {
      try {
        boolean swapAllowed = Boolean.getBoolean("swapAllowed");
        MsgSize = Integer.getInteger("MsgSize", MsgSize/1024).intValue() *1024;
        NbMsg = Integer.getInteger("NbMsg", NbMsg).intValue();
        String destc = System.getProperty("Destination","org.objectweb.joram.client.jms.Queue");
        boolean fixedInMemory = Boolean.getBoolean("fixedInMemory");
        
        if (swapAllowed)
          jmwargs = new String[]{"-Dorg.objectweb.joram.mom.messages.SWAPALLOWED=true"};
        System.out.println("server start");
        TestCase.startAgentServer((short) 0, jmwargs);
        Thread.sleep(2000);

        writeIntoFile("===================== start test 57b =====================");
        writeIntoFile("----------------------------------------------------");
        writeIntoFile("Destination: " + destc + " fixedInMemory: " + fixedInMemory);
        writeIntoFile("MsgSize: " + MsgSize + " swapAllowed: " + swapAllowed);
        writeIntoFile("NbMsg: " + NbMsg);
        writeIntoFile("----------------------------------------------------");

        cf = TcpBaseTest.createConnectionFactory();
        AdminModule.connect(cf);
        if (fixedInMemory) {
          Properties props = new Properties();
          props.setProperty(DestinationConstants.FIXED_IN_MEMORY, "true");
          dest = Queue.create(0, props);
        } else {
          dest = createDestination(destc);
        }
        User.create("anonymous", "anonymous", 0);
        dest.setFreeReading();
        dest.setFreeWriting();
        org.objectweb.joram.client.jms.admin.AdminModule.disconnect();
      } catch(Throwable exc){
        exc.printStackTrace();
        error(exc);
        return;
      }

      
      long dt1, dt2;
      
      // Phase1: Send a big number of messages.
      String[] sentMsgId = new String[2*NbMsg];
      try {
        Connection cnx = cf.createConnection();
        Session sess = cnx.createSession(false, Session.AUTO_ACKNOWLEDGE);
        MessageProducer prod = sess.createProducer(dest);
        cnx.start();


        byte[] content = new byte[MsgSize];
        for (int i = 0; i< MsgSize; i++)
          content[i] = (byte) (i & 0xFF);

        long start = System.currentTimeMillis();
        long start2 = start;
        for (int nb=0; nb<NbMsg; nb++) {
          BytesMessage msg = sess.createBytesMessage();
          msg.writeBytes(content);
          prod.send(msg);
          sentMsgId[nb] = msg.getJMSMessageID();
          if ((nb %10000) == 9999) {
            long end2 = System.currentTimeMillis();
            System.out.println("message sent #" +nb + " -> "  + (end2 - start2));
            start2 = end2;
          }
        }
        long end = System.currentTimeMillis();
        dt1 = end -start;
        System.out.println("All messages sent: " + (dt1/1000L));

        prod.close();
        sess.close();
        cnx.close();
      } catch (javax.jms.JMSException exc) {
        exc.printStackTrace();
        error(exc);
        return;
      }

      // Phase2: Kill and restart server
      try {
//        System.out.println("Ready to backup ");
//        Thread.sleep(60000);

        System.out.println("Server stop ");
        TestCase.stopAgentServer((short) 0);
        Thread.sleep(1000L);
        System.out.println("server start");
        TestCase.startAgentServer((short) 0);
        Thread.sleep(1000L);
      } catch (Exception exc) {
        exc.printStackTrace();
        error(exc);
        return;
      }
      
      // Phase3: Send a big number of messages.
      try {
        Connection cnx = cf.createConnection();
        Session sess = cnx.createSession(false, Session.AUTO_ACKNOWLEDGE);
        MessageProducer prod = sess.createProducer(dest);
        cnx.start();


        byte[] content = new byte[MsgSize];
        for (int i = 0; i< MsgSize; i++)
          content[i] = (byte) (i & 0xFF);

        long start = System.currentTimeMillis();
        long start2 = start;
        for (int nb=NbMsg; nb<(2*NbMsg); nb++) {
          BytesMessage msg = sess.createBytesMessage();
          msg.writeBytes(content);
          prod.send(msg);
          sentMsgId[nb] = msg.getJMSMessageID();
          if ((nb %10000) == 9999) {
            long end2 = System.currentTimeMillis();
            System.out.println("message sent #" +nb + " -> "  + (end2 - start2));
            start2 = end2;
          }
        }
        long end = System.currentTimeMillis();
        dt2 = end -start;
        System.out.println("All messages sent: " + (dt2/1000L));

        prod.close();
        sess.close();
        cnx.close();
      } catch (javax.jms.JMSException exc) {
        exc.printStackTrace();
        error(exc);
        return;
      }

      BaseTest.assertTrue("dt1=" + dt1 + ", dt2=" + dt2, ((dt2*10)/dt1) < 12);
      // Phase4: Kill and restart server
      try {
//        System.out.println("Ready to backup ");
//        Thread.sleep(60000);

        System.out.println("Server stop ");
        TestCase.stopAgentServer((short) 0);
        Thread.sleep(1000L);
        System.out.println("server start");
        TestCase.startAgentServer((short) 0);
        Thread.sleep(1000L);
      } catch (Exception exc) {
        exc.printStackTrace();
        error(exc);
        return;
      }
      
      // Phase5: Consume all messages.
      try {
        long start = System.currentTimeMillis();
        Connection cnx = cf.createConnection();
        Session sess = cnx.createSession(false, Session.AUTO_ACKNOWLEDGE);
        MessageConsumer cons = sess.createConsumer(dest);
        cnx.start();
        long end = System.currentTimeMillis();
        System.out.println("Connection started: " + ((end -start)/1000L));

        start = System.currentTimeMillis();
        long start2 = start;
        for (int nb=0; nb<(2*NbMsg); nb++) {
          BytesMessage msg = (BytesMessage) cons.receive();
          if (nb == 0) {
            end = System.currentTimeMillis();
            System.out.println("1st message received: " + ((end -start)/1000L));
          }
          boolean good = msg.getJMSMessageID().equals(sentMsgId[nb]);
          BaseTest.assertTrue("Receive bad msg #" + nb, good);
          if ((nb %10000) == 9999) {
            long end2 = System.currentTimeMillis();
            System.out.println("message received#" + nb + (good?" ok":" nok") + " -> "  + (end2 - start2));
            start2 = end2;
          }
        }
        end = System.currentTimeMillis();
        System.out.println("All messages received: " + ((end -start)/1000L));

        cons.close();
        sess.close();
        cnx.close();
      } catch(Throwable exc){
        exc.printStackTrace();
        error(exc);
        return;
      }
      
      BaseTest.assertTrue("dt1=" + dt1 + ", dt2=" + dt2, ((dt2*10)/dt1) < 12);
      // Phase5: Kill and restart server
      try {
//        System.out.println("Ready to backup ");
//        Thread.sleep(60000);

        System.out.println("Server stop ");
        TestCase.stopAgentServer((short) 0);
        Thread.sleep(1000L);
        System.out.println("server start");
        TestCase.startAgentServer((short) 0);
        Thread.sleep(1000L);
      } catch (Exception exc) {
        exc.printStackTrace();
        error(exc);
        return;
      }
      
      // Phase6: Verify if there is messages.
      try {
        Connection cnx = cf.createConnection();
        Session sess = cnx.createSession(false, Session.AUTO_ACKNOWLEDGE);
        MessageConsumer cons = sess.createConsumer(dest);
        cnx.start();

        BytesMessage msg = null;
        do {
          msg = (BytesMessage) cons.receive(1000);
          if (msg != null)
            System.out.println("Receives: " + msg.getJMSMessageID());
        } while (msg != null);
        
//        System.out.println("Ready to backup ");
//        Thread.sleep(60000);

        cons.close();
        sess.close();
        cnx.close();
      } catch(Throwable exc){
        exc.printStackTrace();
        error(exc);
        return;
      }      
    } finally{
      TestCase.stopAgentServer((short)0);
      endTest();
    }

    System.exit(0);
  }
}
