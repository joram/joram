/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C) 2016 - 2023 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): ScalAgent D.T.
 * Contributor(s):
 */
package joram.rest;

import java.net.URI;
import java.net.URLConnection;
import java.util.Set;

import jakarta.ws.rs.core.Link;
import jakarta.ws.rs.core.UriBuilder;

public class Helper {

  public static URI getBaseJmsURI() {  
    return UriBuilder.fromUri("http://localhost:8989/joram/").build();  
  }
  
  public static URI getBaseJmsURI(int port) {  
    return UriBuilder.fromUri("http://localhost:"+port+"/joram/").build();  
  }
  
  public static URI getBaseAdminURI() {  
    return UriBuilder.fromUri("http://localhost:8989/joram/admin").build();  
  }
  
  public static URI getBaseAdminURI(int port) {  
    return UriBuilder.fromUri("http://localhost:"+port+"/joram/admin").build();  
  }
  
  public static void print(Set<Link> links) {
    System.out.println("  link :");
    for (Link link : links)
      System.out.println("\t" + link.getRel() + " : " + link.getUri());
  }
  
  public static boolean isConnected(URI uri) {
    try {
      URLConnection connection = uri.toURL().openConnection();
      connection.getInputStream();
      return true;
    } catch (Throwable exc) {
      exc.printStackTrace();
    }
    return false;
  }
  
  public static void waitConnection(URI uri, int timeOut) throws Exception {
    long start = System.currentTimeMillis();
    while (true) {
      try {
        URLConnection connection = uri.toURL().openConnection();
        connection.getInputStream();
        return;
      } catch (Exception e) {
        if ((timeOut * 1000) < (System.currentTimeMillis() - start))
          throw new Exception("Connection impossible in " + timeOut + " second");
        Thread.sleep(100);
      }
    }
  }
}
