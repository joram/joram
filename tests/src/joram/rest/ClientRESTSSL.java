/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C) 2016 - 2024 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): ScalAgent D.T.
 * Contributor(s):
 */
package joram.rest;

import java.io.File;
import java.io.FileInputStream;
import java.io.PrintStream;
import java.net.URI;
import java.security.KeyStore;
import java.util.Properties;

import javax.net.ssl.SSLContext;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.Invocation.Builder;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import org.glassfish.jersey.SslConfigurator;

import framework.TestCase;

/**
 * Test Rest/JMS connector with HTTPS.
 * 
 * /!\ Be careful in order to satisfy SNI this test needs to bind joram.objectweb.org DNS name to the localhost IP address.
 * joram.objectweb.org is registered as CN in Joram certificate.
 */
public class ClientRESTSSL extends TestCase {
  private static final boolean debug = false;
  
  public static void main(String[] args) {
    new ClientRESTSSL().run();
  }

  public void run() {
    try {
      // TODO: Try to solves the SNI issue (does not work).
      Properties props = System.getProperties();
//      props.setProperty("jdk.internal.httpclient.disableHostnameVerification", "true");
//      props.setProperty("sun.net.http.allowRestrictedHeaders", "true");
      if (debug)
        props.setProperty("javax.net.debug", "ssl:handshake:verbose");

      PrintStream out0 = null, err0 = null;
      if (debug) {
        out0 = new PrintStream(File.createTempFile("ClientRESTSSL-s0", ".stdout", new File(".")));
        err0 = new PrintStream(File.createTempFile("ClientRESTSSL-s0", ".stderr", new File(".")));
      }
      
      String handshake = "-Djavax.net.debug=ssl:none";
      if (debug)
        handshake = "-Djavax.net.debug=ssl:handshake:verbose";
      startAgentServer((short)0, new String[] {handshake,
//                                               "-Djdk.internal.httpclient.disableHostnameVerification=true",
//                                               "-Djetty.ssl.sniRequired=false",
//                                               "-Djetty.sslContext.sniRequired=false",
//                                               "-Djetty.ssl.sniHostCheck=false",
                                               },
                       out0, err0);
      
      // wait REST bundle
      Helper.waitConnection(Helper.getBaseJmsURI(), 20);

      javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(
          new javax.net.ssl.HostnameVerifier(){
            public boolean verify(String hostname, javax.net.ssl.SSLSession sslSession) {
              System.out.println("== verify " + hostname);
              return true;
//              if (hostname.equals("localhost") || hostname.equals("127.0.0.1") || hostname.equals("joram.objectweb.org")) {
//                return true;
//              }
//              return false;
            }
          });
      
      SslConfigurator sslConfig = SslConfigurator.newInstance()
          .trustStoreFile("./jssecacerts")
          .keyStoreFile("./joram_ks")
          .keyPassword("jorampass");
      SSLContext sslContext = sslConfig.createSSLContext();
      Client client = ClientBuilder.newBuilder().sslContext(sslContext).build();

      WebTarget target = client.target("https://joram.objectweb.org:8443/joram/");
      
      // lookup the destination
//      Builder builder = target.path("jndi").path("queue").request().header("Host","joram.objectweb.org");
      Builder builder = target.path("jndi").path("queue").request();
      Response response = builder.accept(MediaType.TEXT_PLAIN).head();
      System.out.println("Status=" + response.getStatus());
      assertEquals("jndi-queue", 201, response.getStatus());
      if (response.getStatus() != 201) {
        System.out.println(response.getHeaders());
        System.out.println(response.getStringHeaders());
        System.out.println(response.getEntity());
        System.out.println(response.getStatusInfo());
        System.out.println(response.toString());
        return;
      }

      URI uriCreateProd = response.getLink("create-producer").getUri();
      URI uriCreateCons = response.getLink("create-consumer").getUri();

      // Create the producer
      response = client.target(uriCreateProd)
          .queryParam("user", "anonymous")
          .queryParam("password", "anonymous")
          .request().accept(MediaType.TEXT_PLAIN).post(null);
// TODO (AF):
//          .post(Entity.entity(null, MediaType.APPLICATION_FORM_URLENCODED));
      System.out.println("Status=" + response.getStatus());
      assertEquals("create-producer", 201, response.getStatus());

      URI uriCloseProd = response.getLink("close-context").getUri();
      URI uriSendNextMsg = response.getLink("send-next-message").getUri();

      // Send next message
      String message = "Test message.";
      response = client.target(uriSendNextMsg)
          .request()
          .accept(MediaType.TEXT_PLAIN)
          .post(Entity.entity(message, MediaType.TEXT_PLAIN));
      System.out.println("Status=" + response.getStatus());
      assertEquals("send-next-message", 200, response.getStatus());

      // Create the consumer
      response = client.target(uriCreateCons)
          .request()
          .accept(MediaType.TEXT_PLAIN).post(null);
// TODO (AF):
//          .post(Entity.entity(null, MediaType.APPLICATION_FORM_URLENCODED));
      System.out.println("Status=" + response.getStatus());
      assertEquals("create-consumer", 201, response.getStatus());

      URI uriCloseCons = response.getLink("close-context").getUri();

      // receive the message
      URI uriReceive = response.getLink("receive-next-message").getUri();
      builder = client.target(uriReceive)
          .request()
          .accept(MediaType.TEXT_PLAIN);
      response = builder.get();
      System.out.println("Status=" + response.getStatus());
      String msg = response.readEntity(String.class);
      System.out.println("Message=" + msg);
      assertEquals("receive-next-message", 200, response.getStatus());
      assertNotNull("receive-message", msg);
      assertEquals("receive-message", message, msg);

      // close the producer
      response = client.target(uriCloseProd).request().accept(MediaType.TEXT_PLAIN).delete();
      System.out.println("Status=" + response.getStatus());
      assertEquals("close-producer", 200, response.getStatus());

      // close the consumer
      response = client.target(uriCloseCons).request().accept(MediaType.TEXT_PLAIN).delete();
      System.out.println("Status=" + response.getStatus());
      assertEquals("close-consumer", 200, response.getStatus());
    } catch (Throwable exc) {
      exc.printStackTrace();
      error(exc);
    } finally {
      stopAgentServer((short)0);
      endTest(); 
    }
  }

  public static KeyStore loadStore(String trustStoreFile, String password) throws Exception {
    KeyStore store = KeyStore.getInstance("JKS");
    store.load(new FileInputStream(trustStoreFile), password.toCharArray());
    return store;
  }

}
