/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C) 2016 - 2023 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): ScalAgent D.T.
 * Contributor(s):
 */
package joram.rest;

import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.client.Invocation.Builder;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;

import framework.TestCase;

/**
 * Test: REST admin command with authentication
 * create queues, users and connections factories
 */
public class AdminRESTBadJNDI extends TestCase {

  public static void main(String[] args) {
    new AdminRESTBadJNDI().run();
  }

  public void run() {
    try {
      startAgentServer((short)0, new String[] {"-Drest.jndi.factory.port=16500"});
      
      // wait REST bundle
      Helper.waitConnection(Helper.getBaseJmsURI(), 20);

      ClientConfig config = new ClientConfig();
      Client client = ClientBuilder.newClient(config);
      WebTarget target = client.target(Helper.getBaseJmsURI());

      Builder builder = target.path("jndi").path("queue").request();
      Response response = builder.accept(MediaType.TEXT_PLAIN).head();
      System.out.println(response);
      assertEquals("Request should return 404: " + response.getStatus(), 404, response.getStatus());
      
      builder = target.path("jndi").path("queue").path("create-producer").request();
      response = builder.accept(MediaType.TEXT_PLAIN).post(null);
      System.out.println(response);
      assertEquals("Request should return 404: " + response.getStatus(), 404, response.getStatus());
    } catch (Throwable exc) {
      exc.printStackTrace();
      error(exc);
    } finally {
      stopAgentServer((short)0);
      endTest(); 
    }
  }
}
