/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C) 2016 -2025 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): ScalAgent D.T.
 * Contributor(s):
 */
package joram.rest;

import java.net.URI;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicLong;

import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.Invocation.Builder;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import java.util.Base64;

import org.glassfish.jersey.client.ClientConfig;

import framework.TestCase;

/**
 * Simulates an eFluid scenario.
 *  - Numerous Rest clients producing messages, each message is sent in a new producer.
 *  - No JMS client consuming messages.
 *  
 *  Administration is done through joramAdmin.xml script.
 */
public class ClientRESTTextPerf3 extends TestCase {
  ClientConfig config = new ClientConfig();
  Client client = ClientBuilder.newClient(config);
  WebTarget target = client.target(Helper.getBaseJmsURI());
  private final AtomicLong counter = new AtomicLong(1);
  private final AtomicLong failure = new AtomicLong(1);
  boolean DEBUG = false;
  int NB_THREAD = 4;
  int NB_MSG = 100;
  int MODULO = 10;
  
  long duration = 0;
  long nbMsg = 0;
  int nbRun = 0;

  public static void main(final String[] args) {
    ClientRESTTextPerf3 clientPerf = new ClientRESTTextPerf3();
    if (args != null && args.length > 1) {
      clientPerf.NB_THREAD = Integer.parseInt(args[0]);
      clientPerf.NB_MSG = Integer.parseInt(args[1]);
      if (args.length > 2)
        clientPerf.MODULO = Integer.parseInt(args[2]);
      else
        clientPerf.MODULO = clientPerf.NB_MSG;
      if (args.length == 4)
        clientPerf.DEBUG = Boolean.parseBoolean(args[3]);
    }
    clientPerf.run();
  }

  public void run() {
    try {
      startAgentServer((short) 0, new String[] {
                                                "-Drest.jndi.socketTimeout=" + System.getProperty("rest.jndi.socketTimeout", "5000"),
                                                "-Dfr.dyade.aaa.jndi2.socketLinger=" + System.getProperty("fr.dyade.aaa.jndi2.socketLinger", "-1"),
                                                "-Dfr.dyade.aaa.jndi2.socketReuseAddress=" + System.getProperty("fr.dyade.aaa.jndi2.socketReuseAddress", "false"),
                                                "-Drest.jms.connectionFactory=" + System.getProperty("rest.jms.connectionFactory", "cf"),
                                                "-Drest.jndi.cacheUpdatePeriod=" + System.getProperty("rest.jndi.cacheUpdatePeriod", "0")
      });
      
      // wait REST bundle
      Helper.waitConnection(Helper.getBaseJmsURI(), 20);

      System.out.println("start TEST: \n\tnb thread " + getNbThread() + ", send/receive " + getNbMessage() + " messages/thread");
      writeIntoFile("nb thread " + getNbThread() + ", send/receive " + getNbMessage() + " messages/thread");
      
      ArrayList<Thread> threads = new ArrayList<>();      
      for (int i = 0; i < getNbThread(); i++) {
        Thread thread = new Thread(new Runnable() {
          @Override
          public void run() {
            long i = counter.getAndIncrement();
            try {
              createQueue("queue"+i);
              //Thread.sleep(50);
              test(getNbMessage(), getModulo(), "queue"+i, "prod"+i, "clientID-"+i);
              deleteQueue("queue"+i);
            } catch (Exception e) {
              System.out.println("==== nb failure = " + failure.getAndIncrement() + ", queue" + i + ", prod" + i + " / cons" + i);
              System.out.println("Error: " + e.getMessage());
            }
          }
        });
        thread.start();
        threads.add(thread);
      }
      
      for (Thread t : threads) {
        t.join();
      }
      
      System.out.println("\tresult: " + nbMsg + " messages in " + getStringDuration(duration/nbRun));
      writeIntoFile("result: " + nbMsg + " messages in " + getStringDuration(duration/nbRun));
      System.out.println("====");
      
    } catch (Throwable exc) {
      exc.printStackTrace();
      error(exc);
    } finally {
      stopAgentServer((short)0);
      endTest(); 
    }
  }

  private int getNbThread() {
    return NB_THREAD;
  }
  
  private int getNbMessage() {
    return NB_MSG;
  }
  
  private int getModulo() {
    return MODULO;
  }
  
  private String getStringDuration(long duration) {
    StringBuffer buff = new StringBuffer();
    long ddays = duration / 86400000;
    if (ddays > 0) {
      duration -= ddays * 86400000;
      buff.append(ddays+"D ");
    }
    long dhours = duration / 3600000;
    if (dhours > 0) {
      duration -= dhours * 3600000;
      buff.append(dhours+"H ");
    }
    long dminutes = duration / 60000;
    if (dminutes > 0) {
      duration -= dminutes * 60000;
      buff.append(dminutes+"M ");
    }
    long dsecs = duration / 1000;
    if (dsecs > 0) {
      duration -= dsecs * 1000;
      buff.append(dsecs+"S ");
    }
    if (duration > 0) {
      buff.append(duration+"ms");
    }
    return buff.toString();
  }
  
  private void setPerfs(String prodName, int nbMsg, long duration) {
    if (DEBUG) {
      if (prodName != null)
        System.out.println(prodName + ": " + nbMsg + " messages in " + duration + "ms");
      else
        System.out.println(nbMsg + " messages in " + duration + "ms");
    }
    this.nbRun++;
    this.nbMsg += nbMsg;
    this.duration += duration;
  }
  
  public void createQueue(String queueName) throws Exception {
    URI uri = target.path("admin").path("queue").path(queueName).getUri();
    String encodedUserPassword = Base64.getEncoder().encodeToString("admin:admin".getBytes());
    // Create a queue
    Response response = client.target(uri)
        .queryParam("bind", true)
        .request()
        .header("Authorization", encodedUserPassword)
        .accept(MediaType.TEXT_PLAIN)
        .get();
    if (response.getStatus() != Response.Status.CREATED.getStatusCode())
      throw new Exception("admin createQueue " + response.getStatus());
  }
  
  public void deleteQueue(String queueName) throws Exception {
    URI uri = target.path("admin").path("queue").path(queueName).getUri();
    String encodedUserPassword = Base64.getEncoder().encodeToString("admin:admin".getBytes());
    // Create a queue
    Response response = client.target(uri)
        .request()
        .header("Authorization", encodedUserPassword)
        .accept(MediaType.TEXT_PLAIN)
        .delete();
    if (response.getStatus() != Response.Status.OK.getStatusCode())
      throw new Exception("admin deleteQueue " + response.getStatus());
  }
  
  public void test(int nbMsg, int modulo, String queueName, String prodName, String clientId) throws Exception {
    Builder builder = target.path("jndi").path(queueName).request();
    Response response = builder.accept(MediaType.TEXT_PLAIN).head();
    if (response.getStatus() != Response.Status.CREATED.getStatusCode())
      throw new Exception("lookup \"" + queueName + "\" = " + response.getStatus());
   
    // URI to create prod/cons
    URI uriCreateProd = response.getLink("create-producer").getUri();
    // TODO (AF): to remove (use FormParam alternate URI)
    uriCreateProd = new URI(uriCreateProd.toString() + "-fp");

    long start = System.currentTimeMillis();
    for (int i = 0; i < nbMsg; ) {
      // create producer
      response = createProducer(uriCreateProd, prodName, clientId);
      // URI to close producer and send next message
      URI uriCloseProd = response.getLink("close-context").getUri();
      URI uriSendNext = response.getLink("send-next-message").getUri();

      if (DEBUG) {
        System.out.println("uriSend = " + uriSendNext);
      }

      // Sends messages.
      // Should be configurable!!
      uriSendNext = send(uriSendNext, i++, modulo);
      uriSendNext = send(uriSendNext, i++, modulo);
      uriSendNext = send(uriSendNext, i++, modulo);
      uriSendNext = send(uriSendNext, i++, modulo);

      response = client.target(uriCloseProd).request().accept(MediaType.TEXT_PLAIN).delete();
      //    System.out.println("== close-producer = " + response.getStatus());
    }
    setPerfs(prodName, nbMsg, System.currentTimeMillis() - start);
  }

//  private Response createConsumer(URI uriCreateCons, String name, String clientId) throws Exception {
//    WebTarget target = client.target(uriCreateCons);
//    if (clientId != null)
//      target = target.queryParam("client-id", clientId);
//    if (name != null)
//      target = target.queryParam("name", name);
//    Response response = target.request()
//        .accept(MediaType.TEXT_PLAIN)
//        .post(Entity.entity("user=anonymous&password=anonymous", MediaType.APPLICATION_FORM_URLENCODED));
//    if (response.getStatus() != Response.Status.CREATED.getStatusCode())
//      throw new Exception("createConsumer = " + response.getStatus() + ", target = " + target);
//    return response;
//  }
  
  private Response createProducer(URI uriCreateProd, String name, String clientId) throws Exception {
    WebTarget target = client.target(uriCreateProd);
    if (clientId != null)
      target = target.queryParam("client-id", clientId);
    if (name != null)
      target = target.queryParam("name", name);

    int nbtry = 0;
    while (true) {
      Response response = target.request()
          .accept(MediaType.TEXT_PLAIN)
          .post(Entity.entity("user=anonymous&password=anonymous", MediaType.APPLICATION_FORM_URLENCODED));
      if (response.getStatus() == Response.Status.CREATED.getStatusCode()) 
        return response;

      Thread.sleep(100*nbtry);
      nbtry += 1;
      if (nbtry > 10)
        throw new Exception("createProducer = " + response.getStatus() + ", " + target);
    }
  }
  
  private URI send(URI uriSendNext, int count, int modulo) throws Exception {
    Response response = client.target(uriSendNext)
        .request()
        .accept(MediaType.TEXT_PLAIN)
        .post(Entity.entity("mon message de test next " + count, MediaType.TEXT_PLAIN));
    if (response.getStatus() != Response.Status.OK.getStatusCode())
      throw new Exception("send-next-message = " + response.getStatus());
    else
      if (DEBUG && count%modulo == 0)
        System.out.println(uriSendNext + ", send "+ count);
    return response.getLink("send-next-message").getUri();
  }
  
//  private URI consume(URI uriConsume, int count, int modulo) throws Exception {
//    Response response = client.target(uriConsume)
//        .request()
//        .accept(MediaType.TEXT_PLAIN)
//        .get();
//    String msg = response.readEntity(String.class);
//    if (response.getStatus() == Response.Status.OK.getStatusCode() && msg != null) {
//      if (DEBUG &&count%modulo == 0)
//        System.out.println(uriConsume + ", receive msg = " + msg);
//    } else {
//      System.out.println("ERROR consume msg = " + msg);
//      System.out.println("== receive-next-message = " + response);
//      throw new Exception("receive-next-message = " + response + ", msg = " + msg);
//    }
//    return response.getLink("receive-next-message").getUri();
//  }
}
