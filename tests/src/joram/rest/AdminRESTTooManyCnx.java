/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C) 2025 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): ScalAgent D.T.
 * Contributor(s):
 */
package joram.rest;

import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.client.Invocation.Builder;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import java.net.URI;

import org.glassfish.jersey.client.ClientConfig;

import framework.TestCase;

/**
 * Test
 */
public class AdminRESTTooManyCnx extends TestCase {

  public static void main(String[] args) {
    new AdminRESTTooManyCnx().run();
  }

  public void run() {
    try {
      startAgentServer((short)0, new String[] {"-Drest.jms.maxNumberOfContext=1"});
      
      // wait REST bundle
      Helper.waitConnection(Helper.getBaseJmsURI(), 20);

      ClientConfig config = new ClientConfig();
      Client client = ClientBuilder.newClient(config);
      WebTarget target = client.target(Helper.getBaseJmsURI());

      Builder builder = target.path("jndi").path("queue").request();
      Response response = builder.accept(MediaType.TEXT_PLAIN).head();
      System.out.println(response);
      assertEquals("Request should return 201: " + response.getStatus(), 201, response.getStatus());
      
      URI createProducer = response.getLink("create-producer").getUri();
      response = client.target(createProducer).request().accept(MediaType.TEXT_PLAIN).post(null);
      System.out.println(response);
      assertEquals("Request should return 201: " + response.getStatus(), 201, response.getStatus());
      
      response = client.target(createProducer).request().accept(MediaType.TEXT_PLAIN).post(null);
      System.out.println(response);
      assertEquals("Request should return 429: " + response.getStatus(), 429, response.getStatus());
      
      response = client.target(createProducer).request().accept(MediaType.TEXT_PLAIN).post(null);
      System.out.println(response);
      assertEquals("Request should return 429: " + response.getStatus(), 429, response.getStatus());
    } catch (Throwable exc) {
      exc.printStackTrace();
      error(exc);
    } finally {
      stopAgentServer((short)0);
      endTest(); 
    }
  }
}
