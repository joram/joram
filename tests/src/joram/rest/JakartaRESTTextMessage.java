/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C) 2023 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): ScalAgent D.T.
 * Contributor(s):
 */
package joram.rest;

import java.net.URI;

import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.Invocation.Builder;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;

import org.ow2.joram.jakarta.jms.Queue;
import org.ow2.joram.jakarta.jms.Topic;
import org.ow2.joram.jakarta.jms.admin.AdminModule;
import org.ow2.joram.jakarta.jms.admin.User;
import org.ow2.joram.jakarta.jms.tcp.TcpConnectionFactory;

import framework.TestCase;

import jakarta.jms.ConnectionFactory;

/**
 * Test: 
 * - lookup queue
 * - create producer
 * - send Text message
 * - send Text message next
 * - create consumer
 * - receive Text message
 * - receive Text message next
 * - close 
 */
public class JakartaRESTTextMessage extends TestCase {

  public static void main(String[] args) {
    new JakartaRESTTextMessage().run();
  }
  
  private void createJakartaJMSObjects() throws Exception {
    ConnectionFactory cf = TcpConnectionFactory.create("localhost", 16010);
    ((TcpConnectionFactory) cf).getParameters().connectingTimer = 0;

    AdminModule.connect(cf, "root", "root");

    User.create("anonymous", "anonymous", 0);

    // create destinations and connectionFactory
    Queue queue = Queue.create("queue");
    queue.setFreeReading();
    queue.setFreeWriting();
    System.out.println("queue = " + queue);

    Topic topic = Topic.create("topic");
    topic.setFreeReading();
    topic.setFreeWriting();
    System.out.println("topic = " + topic);

    Queue myQueue1 = Queue.create("myQueue1");
    myQueue1.setFreeReading();
    myQueue1.setFreeWriting();
    System.out.println("myQueue1 = " + myQueue1);

    // bind destinations and connectionFactory
    javax.naming.Context jndiCtx = new javax.naming.InitialContext();
    jndiCtx.rebind("cf", cf);
    jndiCtx.rebind("queue", queue);
    jndiCtx.rebind("topic", topic);
    jndiCtx.rebind("myQueue1", myQueue1);
    jndiCtx.close();

    AdminModule.disconnect();
    System.out.println("Admin closed.");
  }

  public void run() {
    try {
      startAgentServer((short)0);
      Thread.sleep(10000);
      
      createJakartaJMSObjects();
      
      // wait REST bundle
      Helper.waitConnection(Helper.getBaseJmsURI(), 20);
      
      ClientConfig config = new ClientConfig();
      Client client = ClientBuilder.newClient(config);
      WebTarget target = client.target(Helper.getBaseJmsURI());

      Builder builder = target.path("jndi").path("queue").request();
      Response response = builder.accept(MediaType.TEXT_PLAIN).head();
      assertEquals("jndi-queue", 201, response.getStatus());

      System.out.println(response);
      
      URI uriCreateCons = response.getLink("create-consumer").getUri();

      response = client.target(response.getLink("create-producer").getUri())
          .request()
          .accept(MediaType.TEXT_PLAIN).post(null);
// TODO (AF):
//          .post(Entity.entity(null, MediaType.APPLICATION_FORM_URLENCODED));
      assertEquals("create-producer", 201, response.getStatus());

      URI uriCloseProd = response.getLink("close-context").getUri();

      String message = "my test message";
      String messageNext = "my test message next";
      
      response = client.target(response.getLink("send-message").getUri())
          .request()
          .accept(MediaType.TEXT_PLAIN)
          .post(Entity.entity(message, MediaType.TEXT_PLAIN));
      assertEquals("send-message", 200, response.getStatus());

      response = client.target(response.getLink("send-next-message").getUri())
          .request()
          .accept(MediaType.TEXT_PLAIN)
          .post(Entity.entity(messageNext, MediaType.TEXT_PLAIN));
      assertEquals("send-next-message", 200, response.getStatus());

      response = client.target(uriCreateCons)
          .request()
          .accept(MediaType.TEXT_PLAIN).post(null);
// TODO (AF):
//          .post(Entity.entity(null, MediaType.APPLICATION_FORM_URLENCODED));
      assertEquals("create-consumer", 201, response.getStatus());

      URI uriCloseCons = response.getLink("close-context").getUri();

      URI uriConsume = response.getLink("receive-message").getUri();
      builder = client.target(uriConsume)
          .request()
          .accept(MediaType.TEXT_PLAIN);
      response = builder.get();
      String msg = response.readEntity(String.class);
      assertEquals(200, response.getStatus());
      assertNotNull("receive-message", msg);
      assertEquals("receive-message", message, msg);

      uriConsume = response.getLink("receive-next-message").getUri();
      builder = client.target(uriConsume)
          .request()
          .accept(MediaType.TEXT_PLAIN);
      response = builder.get();
      msg = response.readEntity(String.class);
      assertEquals(200, response.getStatus());
      assertNotNull("receive-next-message", msg);
      assertEquals("receive-next-message", messageNext, msg);

      response = client.target(uriCloseProd)
          .request()
          .accept(MediaType.TEXT_PLAIN)
          .delete();
      assertEquals("close-producer", 200, response.getStatus());

      response = client.target(uriCloseCons)
          .request()
          .accept(MediaType.TEXT_PLAIN)
          .delete();
      assertEquals("close-consumer", 200, response.getStatus());

    } catch (Throwable exc) {
      exc.printStackTrace();
      error(exc);
    } finally {
      stopAgentServer((short)0);
      endTest(); 
    }
  }
}
