# Sample using JMS REST API

## Configuration

config/config_rest.properties

## Initialization

Launches the Joram/JMS server with JMS REST API
 - ant clean compile
 - ant rest_server

Creations of JMS user and destinations needed for the tests are automatically done
through joramAdmin.xml file.

## Using browser

Access to JMS, JNDI and Admin API:
 - http://localhost:8989/joram/

Access to JMX API:
 - http://localhost:8989/jmx/

## Test

Creates a Producer sending a message, and a consumer receiving the message.
 - ant rest.test

## Sample

Creates a producer sending 10 messages.
 - ant rest.producer

Creates a consumer waiting 10 messages. 
 - ant rest.consumer

## Performance

Creates 5 consumers, each waiting 20.000 messages.
 - ant rest_perfs_cons_q

Creates 2 producers, each sending 50.000 messages.
 - ant rest_perfs_prod_q
