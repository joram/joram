/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C) 2001 - 2012 ScalAgent Distributed Technologies
 * Copyright (C) 1996 - 2000 Dyade
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): ScalAgent Distributed Technologies
 */
package classic;

import javax.jms.*;
import javax.naming.*;

public class PollingConsumer {

  public static void main(String[] args) throws Exception {
    long delay = Long.getLong("delay", 10000);
    boolean nowait = Boolean.getBoolean("nowait");
    
    Context context = new InitialContext();
    ConnectionFactory cf = (ConnectionFactory) context.lookup("cf");
    javax.jms.Queue queue =(Queue)context.lookup(args[0]);
    context.close();
    
    Connection cnx = cf.createConnection();
    Session session = (Session) cnx.createSession(false, Session.AUTO_ACKNOWLEDGE);
    MessageConsumer consumer = (MessageConsumer) session.createConsumer(queue);
    cnx.start();

    Message msg = null;
    while (msg == null) {
      System.out.println("Try to receive..");
      if (nowait) {
        msg = consumer.receiveNoWait();
        Thread.sleep(delay);
      } else {
        msg = consumer.receive(delay);
      }
    }
    
    System.out.println("Receives: " + msg);
    
    session.close();
    cnx.close();
    System.exit(0);
  }

}
