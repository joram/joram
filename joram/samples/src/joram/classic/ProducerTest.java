/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C) 2022 - 2024 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): ScalAgent Distributed Technologies
 * Contributor(s):
 */
package classic;

import java.io.Serializable;

import javax.jms.*;
import javax.naming.*;

/**
 * Produces messages on the queue and on the topic.
 */
public class ProducerTest {
  static Context ictx = null; 

  public static void main(String[] args) throws Exception {
    System.out.println("Produces messages on " + args[0]);

    ictx = new InitialContext();
    Destination dest = (Destination) ictx.lookup(args[0]);
    ConnectionFactory cf = (ConnectionFactory) ictx.lookup("cf");
    ictx.close();

    String prefix = System.getProperty("JMSMessageID.prefix");
    if (prefix != null)
      System.setProperty("org.objectweb.joram.client.jms.messageid.prefix", prefix);
    Connection cnx = cf.createConnection();
    Session sess = cnx.createSession(false, Session.AUTO_ACKNOWLEDGE);
    MessageProducer producer = sess.createProducer(dest);

    Message msg = sess.createMessage();
    msg.setStringProperty("comment", "empty message");
    producer.send(msg);

    msg = sess.createMessage();
    msg.setBooleanProperty("JMS_JORAM_SWAPALLOWED", true);
    msg.setStringProperty("comment", "empty message with properties");
    msg.setLongProperty("long", 0x1234567890ABCL);
    msg.setShortProperty("short", (short) 32767);
    msg.setBooleanProperty("boolean", true);
    msg.setObjectProperty("object1", Long.valueOf(0xF7F7F7F7F7F7L));
    msg.setObjectProperty("object2", "object2");
    producer.send(msg);
    
    msg = sess.createTextMessage("Message Text");
    msg.setStringProperty("comment", "TextMessage");
    producer.send(msg);
    
    msg = sess.createBytesMessage();
    msg.setStringProperty("comment", "BytesMessage with UTF content");
    ((BytesMessage) msg).writeUTF("BytesMessage");
    producer.send(msg);

    msg = sess.createBytesMessage();
    msg.setStringProperty("comment", "BytesMessage with UTF content and other fields");
    ((BytesMessage) msg).writeUTF("BytesMessage");
    ((BytesMessage) msg).writeBoolean(true);
    ((BytesMessage) msg).writeLong(0x7F0F0F0F0FL);
    producer.send(msg);
    
    msg = sess.createBytesMessage();
    msg.setStringProperty("comment", "BytesMessage with UTF content using writeBytes");
    ((BytesMessage) msg).writeBytes("BytesMessage with charset".getBytes("UTF-8"));
    producer.send(msg);
    
    msg = sess.createBytesMessage();
    msg.setStringProperty("comment", "BytesMessage with UTF-16 content");
    ((BytesMessage) msg).writeBytes("BytesMessage with charset".getBytes("UTF-16"));
    producer.send(msg);
    
    msg = sess.createBytesMessage();
    msg.setBooleanProperty("JMS_JORAM_SWAPALLOWED", true);
    msg.setStringProperty("comment", "BytesMessage with non UTF fields");
    ((BytesMessage) msg).writeBoolean(true);
    ((BytesMessage) msg).writeLong(0x7F0F0F0F0FL);
    ((BytesMessage) msg).writeBytes("BytesMessage with charset".getBytes("UTF-16"));
    producer.send(msg);

    msg = sess.createMapMessage();
    msg.setStringProperty("comment", "MapMessage");
    ((MapMessage) msg).setString("String", "MapMessage");
    ((MapMessage) msg).setBoolean("Boolean", true);
    ((MapMessage) msg).setInt("Integer", 10);
    producer.send(msg);
    
    msg = sess.createStreamMessage();
    msg.setStringProperty("comment", "StreamMessage");
    ((StreamMessage) msg).writeString("StreamMessage");
    ((StreamMessage) msg).writeBoolean(true);
    ((StreamMessage) msg).writeInt(10);
    producer.send(msg);
    
    msg = sess.createObjectMessage();
    msg.setStringProperty("comment", "ObjectMessage with String");
    ((ObjectMessage) msg).setObject("ObjectMessage");
    producer.send(msg);
    
    msg = sess.createObjectMessage();
    msg.setStringProperty("comment", "ObjectMessage with standard Object");
    ((ObjectMessage) msg).setObject(Long.valueOf(0x7FFF00FF00L));
    producer.send(msg);
    
    msg = sess.createObjectMessage();
    msg.setBooleanProperty("JMS_JORAM_SWAPALLOWED", true);
    msg.setStringProperty("comment", "ObjectMessage with client specific object");
    SerializableObject obj = new SerializableObject(10, 1234567890L);
    ((ObjectMessage) msg).setObject(obj);
    producer.send(msg);
    System.out.println("Messages sent.");

    producer.setDeliveryDelay(30000L);
    
    msg = sess.createMessage();
    msg.setStringProperty("comment", "empty delayed message (30s)");
    producer.send(msg);
    
    msg = sess.createTextMessage("delayed TextMessage (30s)");
    msg.setStringProperty("comment", "delayed TextMessage (30s)");
    producer.send(msg);

    cnx.close();
  }
}

class SerializableObject implements Serializable {
  public int value1;
  public long value2;
  
  public SerializableObject(int value1, long value2) {
    this.value1 = value1;
    this.value2 = value2;
  }

 @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("SerializableObject [value1=").append(value1)
           .append(", value2=").append(value2).append("]");
    return builder.toString();
  }
}
