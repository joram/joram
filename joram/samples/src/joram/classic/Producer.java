/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C) 2001 - 2024 ScalAgent Distributed Technologies
 * Copyright (C) 1996 - 2000 Dyade
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): Frederic Maistre (INRIA)
 * Contributor(s):
 */
package classic;

import javax.jms.*;
import javax.naming.*;

/**
 * Produces messages on the queue and on the topic.
 */
public class Producer {
  static Context ictx = null; 

  public static void main(String[] args) throws Exception {
    System.out.println("Produces messages on " + args[0]);

    boolean map = Boolean.getBoolean("map");
    System.out.println("map=" + map);
    
    long delay = Long.getLong("delay", 0L);
    if (delay > 0)
      System.out.println("delay=" + delay);

    ictx = new InitialContext();
    Destination dest = (Destination) ictx.lookup(args[0]);
    ConnectionFactory cf = (ConnectionFactory) ictx.lookup("cf");
    ictx.close();

    String prefix = System.getProperty("JMSMessageID.prefix");
    if (prefix != null)
      System.setProperty("org.objectweb.joram.client.jms.messageid.prefix", prefix);
    Connection cnx = cf.createConnection();
    Session sess = cnx.createSession(false, Session.AUTO_ACKNOWLEDGE);
    MessageProducer producer = sess.createProducer(dest);
    if (delay > 0)
      producer.setDeliveryDelay(delay);

    int i;
    Message msg;
    for (i = 0; i < 10; i++) {
      if (map) {
        msg = sess.createMapMessage();
        ((MapMessage) msg).setString("payload", "Test number " + i);
        ((MapMessage) msg).setBoolean("true", true);
        ((MapMessage) msg).setLong("long", 0x1F0F0F0F0L);
        ((MapMessage) msg).setBytes("bytes", new byte[] {12, 8, 36});
      } else {
        msg = sess.createTextMessage("Test number " + i);
      }
      producer.send(msg);
    }

    System.out.println(i + " messages sent.");

    cnx.close();
  }
}
