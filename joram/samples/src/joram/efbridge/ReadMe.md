# Sample using JMS bridge

/!\ Be careful, the configuration has changed to allow the use of users and ACL. 
efbridge_admin target now configures both the bridge server and the hub server.
XML administration can no longer be used.

Windows #1
 - ant clean compile
 - ant ngt_server

Windows #2
 - ant efbridge_server

Windows #3
 - ant efbridge_admin
 - ant efbridge_consumer

Windows #4
 - ant efbridge_producer
