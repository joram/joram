package test;

import java.io.ObjectStreamClass;

public class listSVid {

  public static void main(String[] args) {
    for (String cn : args) {
      try {
        long serialVersionID = ObjectStreamClass.lookup(Class.forName(cn)).getSerialVersionUID();
        System.out.println("Class: " + cn + " -> " + serialVersionID);
      } catch (Throwable t) {
        t.printStackTrace(System.out);
      }
    }
  }

}
