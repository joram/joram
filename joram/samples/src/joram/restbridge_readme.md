# Rest and Rest bridge samples

## Rest - Sample

Serveur utilisant ngt\_a3servers.xml (JNDI 16400, JMS 16010) et config\_rest.properties.

W#1
ant clean compile
ant reset rest_server

W#2
ant rest.producer
ant rest.consumer

## Rest - Performances

W#1
ant clean compile
ant reset rest_server

W#2
ant rest_perfs_cons_q

W#3
ant rest_perfs_prod_q

## Bridge Rest - Sample

Serveur bridge restbridge\_a3servers.xml (JNDI 16401, JMS 16011) et config\_restbridge.properties.
Serveur backend utilisant ngt\_a3servers.xml (JNDI 16400, JMS 16010) et config\_rest.properties.

W#1
ant clean compile
ant reset rest_server

W#2
ant restbridge_server

W#3
ant restbridge_admin
ant restbridge_consumer

W#4
ant restbridge_producer

## Bridge Rest - Performances

W#1
ant clean compile
ant reset rest_server

W#2
ant restbridge_server

W#3
restbridge_perfs_admin
ant restbridge_perfs_cons_q1 & sleep 1 ; ant restbridge_perfs_cons_q2 &

W#4
ant restbridge_perfs_prod_q1 & ant restbridge_perfs_prod_q2 &