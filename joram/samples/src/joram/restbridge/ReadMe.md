# Sample using bridge REST

## Configuration

This sample uses 2 servers, the front end with the bridge, and the back end with JMS REST API (see corresponding sample).

The configuration of the front end server is:
 - config/restbridge_a3servers.xml (only changes listening port for JNDI and JMS connector).
 - config/config_restbridge.properties (adds library for Rest/JMS bridge destinations).

The back end server is a simple broker running the JMS REST API.

## Initialization

Launches the Joram/JMS server with JMS REST API
 - ant clean compile
 - ant rest_server
 - ant restbridge_server

Creates JMS user and bridge destinations needed for the tests. It creates a distribution queue pushing messages
to 'queue' on back end server, and an acquisition queue acquiring messages from 'queue' on back end server. Acquisition
and distribution queue use JMS REST API. So each message sent to the distribution queue can be received through the
acquisition queue :
 - ant restbridge\_admin (or restbridge\_adminxml)

Be careful, the bridge destinations uses the 'queue', this queue is created on the back end by the XML administration
script '../../src/joram/rest/joramAdmin.xml'.

## Sample

Creates a consumer waiting messages on acquisition queue.
 - ant rest.consumer

Creates a producer sending 1 messages to the distribution queue.
 - ant restbridge_producer

## Performance

 - ant restbridge\_perfs\_admin
 - ant restbridge\_perfs\_cons_q1
 - ant restbridge\_perfs\_cons_q2
 - ant restbridge\_perfs\_prod_q1
 - ant restbridge\_perfs\_prod_q2
