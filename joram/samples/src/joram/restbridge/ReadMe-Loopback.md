# Sample using bridge REST in a loopback behavior

## Configuration

This sample uses a single server, his broker hosts both the bridge destinations (Rest/JMS distribution and acquisition),
the JMS Rest connector, and the foreign destination.

This broker must have both the libraries containing the Rest/JMS bridge destinations and the Rest/JMS connector:
 - config/config_restbridge_loopback.properties

## Initialization

Launches the Joram/JMS server with JMS REST API
 - ant clean compile
 - ant restbridge_server_loopback
 - ant restbridge_admin_loopbak

Creates JMS user and bridge destinations needed for the tests. It creates a distribution queue pushing messages
to 'queue' on local server, and an acquisition queue acquiring messages from 'queue' on local server. Acquisition
and distribution queue use JMS REST API. So each message sent to the distribution queue can be received through the
acquisition queue :
 - ant restbridge\_admin (or restbridge\_adminxml)

Be careful, the bridge destinations uses the 'queue', this queue is created on the back end by the XML administration
script '../../src/joram/rest/joramAdmin.xml'.

## Sample

Creates a consumer waiting messages on acquisition queue.
 - ant restbridge_consumer_loopbak

Creates a producer sending 1 messages to the distribution queue.
 - ant restbridge_producer_loopbak
