These files are used by samples with JDBCTransaction.
They are copied in ship/bundles by assembly (it seems that these files are duplicated in assembly/lib).
They should be handled by maven.