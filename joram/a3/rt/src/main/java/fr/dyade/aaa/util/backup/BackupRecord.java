/*
 * Copyright (C) 2021 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): ScalAgent Distributed Technologies
 * Contributor(s): 
 */
package fr.dyade.aaa.util.backup;

/**
 * Class defining the content of a [Backup|Restore]File.
 * Each BackupRecord contains the description of a persistent object.
 */
public class BackupRecord {
  static final byte[] emptyUTFString = {0, 0};
  
  /**
   * Directory name of the persistent object.
   */
  String dirName;
  
  /**
   * Returns the directory name of the persistent object.
   * @return the dirName
   */
  public String getDirName() {
    return dirName;
  }
  
  /**
   * Name of the persistent object.
   */
  String name;
  
  /**
   * Returns the name of the persistent object.
   * @return the name
   */
  public String getName() {
    return name;
  }
  
  /**
   * Returns the pathname of the persistent object.
   * @return the name
   */
  public String getPathname() {
    if (dirName != null)
      return dirName + '/' + name;
    return name;
  }
  
  /**
   * Value of the persistent object.
   */
  byte[] value;

  /**
   * Returns the value of the persistent object.
   * @return the value
   */
  public byte[] getValue() {
    return value;
  }
  
  /**
   * Sets the value of the persistent object.
   * @param value the value to set
   */
  public void setValue(byte[] value) {
    this.value = value;
  }

  /**
   * Creates an empty record.
   */
  BackupRecord() {}

  /**
   * Creates a record with the specified values.
   * 
   * @param dirName the directory name of the persistent object.
   * @param name    the name of the persistent object.
   * @param value   the value of the persistent object.
   */
  public BackupRecord(String dirName, String name, byte[] value) {
    this.dirName = dirName;
    this.name = name;
    this.value = value;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("BackupRecord [name=");
    if (dirName != null)
      builder.append(dirName).append('/');
    builder.append(name).append(", value.len=").append(value.length).append("]");
    return builder.toString();
  }
}
