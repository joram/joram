/*
 * Copyright (C) 2005 - 2023 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): ScalAgent Distributed Technologies
 * Contributor(s): 
 */
package fr.dyade.aaa.util;

public interface TransactionMBean {
  /**
   * Tests if the Transaction component is persistent.
   *
   * @return true if the component is persistent; false otherwise.
   */
  boolean isPersistent();
  
  /**
   * Returns true if this Transaction implementation implements an optimized loadAll method.
   * 
   * @return true if this Transaction implementation implements an optimized loadAll method.
   */
  boolean useLoadAll();

  /**
   * Returns the transaction state.
   * @return the transaction state.
   */
  int getPhase();

  /**
   * Returns a string representation of the transaction state.
   * @return the string representation of the transaction state.
   */
  String getPhaseInfo();

  String getObject(String dirname, String name);
  String dumpObjectList(String prefix);
  String[] getObjectList(String prefix);

  /**
   * Returns the number of commit operation since startup.
   *
   * @return The number of commit operation.
   */
  int getCommitCount();

  /**
   * Returns the starting time.
   *
   * @return The starting time.
   */
  long getStartTime();
  
  /**
   * Backups the content of Transaction module.
   * Produces a generic Backup/Restore file containing all living objects.
   * 
   * @param path  Directory path to store the backup.
   * @return the path of created backup file.
   * @throws Exception An error occurs during backup.
   */
  String backup(String path) throws Exception;
}
