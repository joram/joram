/*
 * Copyright (C) 2022 ScalAgent Distributed Technologies 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 */
package fr.dyade.aaa.agent.conf;

import org.xml.sax.SAXParseException;

/**
 * Thrown to indicate that the Configuration is invalid. 
 */
public class ParseException extends Exception {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Name of the configuration file.
   * May be set after the exception has been created.
   */
  private String fileName;
  
  /**
   *  Constructs a new <code>InvalidConfigurationException</code> with no
   * detail message.
   */
  public ParseException() {
    super();
  }

  /**
   *  Constructs a new <code>InvalidConfigurationException</code> with the
   * specified  detail message. 
   *
   * @param   s   the detail message.
   */
  public ParseException(String s) {
    super(s);
  }

  /**
   *  Constructs a new <code>InvalidConfigurationException</code> with the
   * specified  detail message and cause. 
   *
   * @param   s   the detail message.
   * @param   cause   the cause of the exception
   */
  public ParseException(String s, Throwable cause) {
    super(s, cause);
  }

  /**
   *  Constructs a new <code>InvalidConfigurationException</code> with the
   * specified cause. 
   *
   * @param   cause   the cause of the exception
   */
  public ParseException(Throwable cause) {
    super(null, cause);
  }
  
  /**
   * Sets the fileName field of the exception.
   * @param fileName  name of the file holding the configuration definition
   */
  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  /**
   * Returns the detail message string of this throwable.
   */
  @Override
  public String getMessage() {
    String message = super.getMessage();
    Throwable cause = getCause();
    SAXParseException saxExc = null;
    if (cause != null && cause instanceof SAXParseException) {
      saxExc = (SAXParseException) cause;
    }
    StringBuffer buf = new StringBuffer();
    if (message == null) {
      if (saxExc != null) {
        message = "Error parsing configuration";
      } else {
        message = "Invalid configuration";
      }
    }
    buf.append(message);
    if (fileName != null) {
      buf.append(" file ").append(fileName);
    }
    if (saxExc != null) {
      buf.append(", lineNumber=").append(saxExc.getLineNumber())
      .append(", columnNumber=").append(saxExc.getColumnNumber())
      .append(" : ").append(saxExc.getMessage());
    }
    return buf.toString();
  }
}