/*
 * Copyright (C) 2024 ScalAgent Distributed Technologies
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): ScalAgent Distributed Technologies
 * Contributor(s): 
 */
package fr.dyade.aaa.agent;

import java.util.Properties;

/**
 * This notification allows to require an administration action to a specified agent.
 * It is used for JMX requests that requires an interaction with an agent.
 * 
 * This notification is not persistent and returns a result to the sender through the
 * SyncNotification context.
 */
public class AdminSyncNotification extends SyncNotification {
     // Serial version UID corresponding to Joram 5.22
    private static final long serialVersionUID = 522L;
    
    public final static int NOOP = 0;
    public final static int BACKUP = 1; // Currently, never used.
    
    public final static int EXPORT = 2;
    public final static String EXPORT_DIRPATH = "DIRPATH";
    public static final String EXPORT_SELECTOR = "SELECTOR";
    public static final String EXPORT_BINARY = "BINARY";
    
    public final static int CLEAN_PENDING_MESSAGE = 3;
    public final static int CLEAN_WAITING_REQUEST = 4;
    
    private int command;
    private Properties parameters;
    private boolean quiet;

    public AdminSyncNotification(int command, boolean quiet) {
      super();
      this.command = command;
      this.quiet = quiet;
    }

    public int getCommand() {
      return command;
    }

    public void addParameter(String key, String value) {
      if (parameters == null)
        parameters= new Properties();
      parameters.setProperty(key, value);
    }

    public String getParameter(String key) {
      if (parameters == null)
        return null;
      return parameters.getProperty(key);
    }
    
    public boolean isQuiet() {
      return quiet;
    }

    public StringBuffer toString(StringBuffer output) {
      output.append('(');
      super.toString(output);
      output.append(",command=").append(command);
      output.append(')');

      return output;
    }
  }