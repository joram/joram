/*
 * Copyright (C) 2001 - 2024 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): ScalAgent Distributed Technologies
 * Contributor(s): 
 */
package fr.dyade.aaa.agent;

public interface SCServerMBean {
  public short getServerId();
  public String getName();

  public void start();
  public void stop();
  public void shutdown();

  public int getStatus();
  public String getStatusInfo();

  public String[] getServers();
  
  public void dumpServerState(String path);
  public void dumpAttributes(String name, String path, boolean threaddump);

  public String getConfigurationProperty(String name);
  
  // ********************************************************************************
  // Allows to get heap informations from JMX
  
  /**
   * Returns the maximum amount of memory that can be used for memory management.
   * Its value may be undefined. The maximum amount of memory may change over time if defined. The amount of used and committed memory will
   * always be less than or equal to max if max is defined. A memory allocation may fail if it attempts to increase the used memory such that
   * used > committed even if used <= max would still be true (for example, when the system is low on virtual memory).

   * @return
   */
  long getHeapMaxMemory();
  
  /**
   * Returns the amount of memory that is guaranteed to be available for use by the JVM.
   * The amount of committed memory may change over time (increase or decrease). The Java virtual machine may release memory to the system
   * and committed could be less than init. committed will always be greater than or equal to used.
   * 
   * @return  the amount of memory (in bytes) that is guaranteed to be available for use by the JVM.
   */
  long getHeapTotalMemory();
  
  /**
   * Return the amount of memory currently used in the JVM.
   * 
   * @return the amount of memory currently used in the JVM.
   */
  long getHeapUsedMemory();
  
  /**
   * Returns the amount of free memory in the JVM.
   * @return the amount of free memory in the JVM.
   */
  long getHeapFreeMemory();
  
  // ********************************************************************************
  // Allows to modify logging configuration from JMX

  /**
   * Sets the DEBUG level for the specified logger.
   * 
   * @param classname The logger name.
   */
  public void debug(String classname);
  
  /**
   * Sets the INFO level for the specified logger.
   * 
   * @param classname The logger name.
   */
  public void info(String classname);
  
  /**
   * Sets the WARN level for the specified logger.
   * 
   * @param classname The logger name.
   */
  public void warn(String classname);
  
  /**
   * Sets the ERROR level for the specified logger.
   * 
   * @param classname The logger name.
   */
  public void error(String classname);
  
  /**
   * Sets the FATAL level for the specified logger.
   * 
   * @param classname The logger name.
   */
  public void fatal(String classname);
  
  /**
   * Set the given level for the specified logger.
   * 
   * @param classname The logger name.
   * @param level     The level name
   */
  void setLoggerLevel(String classname, String level);

  /**
   * Sets the INHERIT level for the specified logger.
   * 
   * @param classname The logger name.
   */
  public void clearLoggerLevel(String classname);
  
//  /**
//   * Freezes the Transaction module.
//   * 
//   * @param timeout
//   * @throws InterruptedException
//   */
//  // TODO (AF): Needed for test/debug, do not commit.
//  public void freeze(long timeout) throws InterruptedException;
  
  /**
   * Backups the content of Transaction module.
   * Produces a generic Backup/Restore file containing all living objects.
   * 
   * @param path  Directory path to store the backup.
   * @return the name of created backup file.
   * @throws Exception An error occurs during backup.
   */
  public String backup(String path) throws Exception;
}
