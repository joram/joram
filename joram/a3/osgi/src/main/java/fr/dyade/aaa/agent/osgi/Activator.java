/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C) 2009 - 2021 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): ScalAgent Distributed Technologies
 * Contributor(s): 
 */
package fr.dyade.aaa.agent.osgi;

import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;

import fr.dyade.aaa.agent.AgentServer;
import fr.dyade.aaa.agent.AgentServerActivator;
import fr.dyade.aaa.common.Debug;

public class Activator implements BundleActivator, AgentServerActivator {
  public static final Logger logmon = Debug.getLogger(Activator.class.getName());

  public static final String AGENT_SERVER_ID_PROPERTY = "fr.dyade.aaa.agent.AgentServer.id";
  public static final String AGENT_SERVER_STORAGE_PROPERTY = "fr.dyade.aaa.agent.AgentServer.storage";

  /**
   * Property defining the policy towards the OSGi container in case of an error or a server stop.
   * If true the container exits. 
   */
  public static final String OSGI_EXIT_PROPERTY = "fr.dyade.aaa.osgi.exit";
  
  public static BundleContext context;

  public void start(BundleContext context) throws Exception {
    Activator.context = context;

    short sid = getShortProperty(AGENT_SERVER_ID_PROPERTY, (short) 0);
    String path = getProperty(AGENT_SERVER_STORAGE_PROPERTY, "s" + sid);
    try {
      AgentServer.activator = this;
      AgentServer.init(sid, path, null);
    } catch (Exception exc) {
      // Note (AF): This sequence below corresponds to the remote launching of A3 runtime for mediation.
//      System.out.println(AgentServer.getName() + " initialization failed: " + AgentServer.ERRORSTRING);
//      System.out.println(exc.toString());
//      System.out.println(AgentServer.ENDSTRING);
      
      // Note (AF): This error is already logged by AgentServer
//      if (logmon.isLoggable(BasicLevel.DEBUG))
//        logmon.log(BasicLevel.ERROR, AgentServer.getName() + " initialization failed", exc);
//      else
//        logmon.log(BasicLevel.ERROR, AgentServer.getName() + " initialization failed: " + exc.getMessage());
      if (Boolean.parseBoolean(context.getProperty(OSGI_EXIT_PROPERTY))) {
        context.getBundle(0).stop();
        return;
      } else {
        throw exc;
      }
    }

    try {
      String errStr = AgentServer.start();
      // Note (AF): This sequence below corresponds to the remote launching of A3 runtime for mediation.
      if (errStr == null) {
        System.out.println(AgentServer.getName() + " started: " + AgentServer.OKSTRING);
      } else {
        System.out.println(AgentServer.getName() + " started: " + AgentServer.ERRORSTRING);
        System.out.print(errStr);
//        System.out.println(AgentServer.ENDSTRING);
      }
    } catch (Exception exc) {
      // Note (AF): This sequence below corresponds to the remote launching of A3 runtime for mediation.
//      System.out.println(AgentServer.getName() + " start failed: " + AgentServer.ERRORSTRING);
//      System.out.println(exc.toString());
//      System.out.println(AgentServer.ENDSTRING);
      
      // Note (AF): This error is already logged by AgentServer
//      if (logmon.isLoggable(BasicLevel.DEBUG))
//        logmon.log(BasicLevel.ERROR, AgentServer.getName() + " failed", exc);
//      else
//        logmon.log(BasicLevel.ERROR, AgentServer.getName() + " failed: " + exc.getMessage());
      if (Boolean.parseBoolean(context.getProperty(OSGI_EXIT_PROPERTY))) {
        context.getBundle(0).stop();
        return;
      } else {
        throw exc;
      }
    }
  }
  
  public void stop(BundleContext context) throws Exception {
    AgentServer.stop();
    AgentServer.reset();
    AgentServer.activator = null;
    Activator.context = null;
    if (Boolean.parseBoolean(context.getProperty(OSGI_EXIT_PROPERTY)))
      context.getBundle(0).stop();
  }

  private short getShortProperty(String propName, short defaultValue) {
    String propValue = context.getProperty(propName);
    if (propValue != null) {
      return Short.parseShort(propValue);
    }
    return defaultValue;
  }

  private String getProperty(String propName, String defaultValue) {
    String propValue = context.getProperty(propName);
    if (propValue != null) {
      return propValue;
    }
    return defaultValue;
  }
  
  // ================================================================================
  // Implementation of AgentServerActivator
  
  public void shutdown() throws Exception {
    if (context != null) context.getBundle(0).stop();
  }
}
