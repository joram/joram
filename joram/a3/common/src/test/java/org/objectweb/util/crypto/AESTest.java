/*
 * Copyright (C) 2025 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): ScalAgent Distributed Technologies
 * Contributor(s): 
 */
package org.objectweb.util.crypto;

import java.security.SecureRandom;
import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

import fr.dyade.aaa.common.crypto.AESTool;
import fr.dyade.aaa.common.crypto.Crypto;

public class AESTest {
  static final int NBROUND = 1000;

  static final String tests[] = {
                                 "Sample text to encrypt",
                                 "password",
                                 "very very very very very long password",
                                 "pass"
  };

  @Test
  public void test1() throws Exception {
    Crypto hash1 = AESTool.createCrypto("AES/CBC/PKCS5Padding", "paswword", "salt", "0123456789012345", 10000, 256);
    Crypto hash2 = AESTool.createCrypto("AES/CBC/PKCS5Padding", "paswword", "salt", "0123456789012345", 10000, 256);
    
    for (String input : tests) {
      byte[] encoded = hash1.encrypt(input.getBytes("UTF-8"));
      byte[] decoded = hash2.decrypt(encoded);

      System.out.println("Encode \"" + input + "\" => " + encoded.length + " != " + decoded.length);
      String output = new String(decoded, "UTF-8");
      Assert.assertTrue("Error: " + output + " != " + input, input.equals(output));
    }
  }

  @Test
  public void test2() throws Exception {
    Crypto hash1 = AESTool.createCrypto("AES/CBC/PKCS5Padding", "paswword", "salt", "0123456789012345", 10000, 256);
    Crypto hash2 = AESTool.createCrypto("AES/CBC/PKCS5Padding", "paswword", "salt", "0123456789012345", 10000, 256);

    SecureRandom secureRandom = SecureRandom.getInstanceStrong();
    byte[][] data = new byte[NBROUND][128];
    for (int i=0;i<NBROUND; i++) {
      secureRandom.nextBytes(data[i]);
    }
    long start = System.nanoTime();
    for (int i=0; i<NBROUND; i++) {
      byte[] encoded = hash1.encrypt(data[i]);
      byte[] decoded = hash2.decrypt(encoded);
      Assert.assertTrue("Error: #" + i, Arrays.equals(data[i], decoded));
    }
    long end = System.nanoTime();
    System.out.println("" + ((end-start) / NBROUND) + "ns");
  }

  @Test
  public void test3() throws Exception {
    Crypto hash1 = AESTool.createCrypto("AES/CBC/PKCS5Padding", "paswword", "salt", "0123456789012345", 10000, 256);
    Crypto hash2 = AESTool.createCrypto("AES/CBC/PKCS5Padding", "paswword", "salt", "0123456789012345", 10000, 256);

    SecureRandom secureRandom = SecureRandom.getInstanceStrong();
    byte[][] data = new byte[NBROUND][128];
    byte[][] nonce = new byte[NBROUND][16];
    for (int i=0;i<NBROUND; i++) {
      secureRandom.nextBytes(data[i]);
      secureRandom.nextBytes(nonce[i]);
    }
    long start = System.nanoTime();
    for (int i=0; i<NBROUND; i++) {
      byte[] encoded = hash1.encrypt(data[i], nonce[i]);
      byte[] decoded = hash2.decrypt(encoded, nonce[i]);
      Assert.assertTrue("Error: #" + i, Arrays.equals(data[i], decoded));
    }
    long end = System.nanoTime();
    System.out.println("" + ((end-start) / NBROUND) + "ns");
  }
}
