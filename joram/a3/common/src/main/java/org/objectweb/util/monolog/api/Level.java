package org.objectweb.util.monolog.api;

public interface Level {
  int intValue();
}
