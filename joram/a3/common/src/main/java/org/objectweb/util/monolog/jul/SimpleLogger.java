/*
 * Copyright (C) 2021 - 2022 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Original developer: ScalAgent Distributed Technologies
 */
package org.objectweb.util.monolog.jul;

import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Level;
import org.objectweb.util.monolog.api.Logger;

/**
 * This classe wraps old Monolog's Logger on JavaLog Logger.
 * It avoids the use of JavaLog Logger objects in Joram code.
 */
class SimpleLogger implements Logger {
  java.util.logging.Logger logger;

  SimpleLogger(String topic) {
    this.logger = java.util.logging.Logger.getLogger(topic);
  }
  
  /**
   *  Sets the IntLevel attribute of the LoggerImpl object
   *
   * @param  l  The new IntLevel value
   */
  @Override
  public void setLevel(Level level) {
    logger.setLevel((SimpleLevel) level);
  }

  @Override
  public void setLevel(String level) throws Exception {
    if ((level == null) || (level.equalsIgnoreCase("INHERIT"))) {
      setLevel((Level) null);
      return;
    }
    try {
      setLevel((SimpleLevel) BasicLevel.parseBasicLevel(level));
    } catch (Throwable exc) {
      throw new Exception("Cannot set " + logger.getName() + " to level " + level + ": " + exc.getMessage());
    }
  }

  /**
   *  Gets the Loggable attribute of the LoggerImpl object
   *
   * @param level  Description of Parameter
   * @return The Loggable value
   */
  @Override
  public boolean isLoggable(Level level) {
    return logger.isLoggable((SimpleLevel) level);
  }

  /**
   * Log a message, with no arguments.
   * If the logger is currently enabled for the given message level then the
   * given message is treated
   */
  @Override
  public void log(Level level, String msg) {
    if (!logger.isLoggable((SimpleLevel) level)) return;
    
    LogRecord record = new LogRecord((SimpleLevel) level, msg);
    record.setLoggerName(logger.getName());
    StackTraceElement[] stack = Thread.currentThread().getStackTrace();
    record.setSourceClassName(stack[2].getClassName());
    record.setSourceMethodName(stack[2].getMethodName());
    record.setSourceLineNumber(stack[2].getLineNumber());
    record.setThreadName(Thread.currentThread().getName());
    logger.log(record);
  }

  /**
   * Log a message, with a throwable arguments which can represent an 
   * error or a context..
   */
  @Override
  public void log(Level level, String msg, Throwable t) {
    if (!logger.isLoggable((SimpleLevel) level)) return;
    
    LogRecord record = new LogRecord((SimpleLevel) level, msg);
    record.setLoggerName(logger.getName());
    StackTraceElement[] stack = Thread.currentThread().getStackTrace();
    record.setSourceClassName(stack[2].getClassName());
    record.setSourceMethodName(stack[2].getMethodName());
    record.setSourceLineNumber(stack[2].getLineNumber());
    record.setThreadName(Thread.currentThread().getName());
    record.setThrown(t);
    logger.log(record);
  }

  /**
   * Log a message, with one object parameter.
   * <p>
   * If the logger is currently enabled for the given message
   * level then a corresponding LogRecord is created and forwarded
   * to all the registered output Handler objects.
   *
   * @param   level   One of the message level identifiers, e.g., SEVERE
   * @param   msg     The string message (or a key in the message catalog)
   * @param   param1  parameter to the message
   */
  @Override
  public void log(Level level, String msg, Object param1) {
    if (!logger.isLoggable((SimpleLevel) level)) return;
    
    LogRecord record = new LogRecord((SimpleLevel) level, msg);
    record.setLoggerName(logger.getName());
    StackTraceElement[] stack = Thread.currentThread().getStackTrace();
    record.setSourceClassName(stack[2].getClassName());
    record.setSourceMethodName(stack[2].getMethodName());
    record.setSourceLineNumber(stack[2].getLineNumber());
    record.setThreadName(Thread.currentThread().getName());
    record.setParameters(new Object[] { param1 });
    logger.log(record);
  }

  /**
   * Log a message, with an array of object arguments.
   * <p>
   * If the logger is currently enabled for the given message
   * level then a corresponding LogRecord is created and forwarded
   * to all the registered output Handler objects.
   *
   * @param   level   One of the message level identifiers, e.g., SEVERE
   * @param   msg     The string message (or a key in the message catalog)
   * @param   params  array of parameters to the message
   */
  @Override
  public void log(Level level, String msg, Object params[]) {
    if (!logger.isLoggable((SimpleLevel) level)) return;
    
    LogRecord record = new LogRecord((SimpleLevel) level, msg);
    record.setLoggerName(logger.getName());
    StackTraceElement[] stack = Thread.currentThread().getStackTrace();
    record.setSourceClassName(stack[2].getClassName());
    record.setSourceMethodName(stack[2].getMethodName());
    record.setSourceLineNumber(stack[2].getLineNumber());
    record.setThreadName(Thread.currentThread().getName());
    record.setParameters(params);
    logger.log(record);
  }

  @Override
  public String getName() {
    return logger.getName();
  }
}
