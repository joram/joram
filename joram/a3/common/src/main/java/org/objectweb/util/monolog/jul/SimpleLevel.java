/*
 * Copyright (C) 2021 - 2024 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Original developer: ScalAgent Distributed Technologies
 */
package org.objectweb.util.monolog.jul;

import org.objectweb.util.monolog.api.Level;

/**
 * This class is only needed to create the ERROR level.
 * The needed constructor in JUL Level class is protected.
 */
class SimpleLevel extends java.util.logging.Level implements Level {
  /** Define serialVersionUID for interoperability. */
  private static final long serialVersionUID = 1L;
  
  protected SimpleLevel(String name, int value) {
    super (name, value);
  }
}