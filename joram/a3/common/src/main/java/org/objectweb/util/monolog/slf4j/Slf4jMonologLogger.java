/*
 * Copyright (C) 2022 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): SACKSTEDER Fabien
 */
package org.objectweb.util.monolog.slf4j;

import org.objectweb.util.monolog.api.*;

/**
 * Adaptateur : redirection d'opérations de log Monolog vers Slf4j
 */
class Slf4jMonologLogger implements Logger {

  private final org.slf4j.Logger logger;
  private final Slf4jLoggerLevelSetter setter;

  Slf4jMonologLogger(org.slf4j.Logger logger, Slf4jLoggerLevelSetter setter) {
    this.logger = logger;
    this.setter = setter;
  }

  @Override
  public void setLevel(Level level) {
    String levelName = Slf4jMonologLevels.of(level).name();
    setter.reconfigure(logger, levelName);
  }

  @Override
  public void setLevel(String s) {
    String levelName = Slf4jMonologLevels.of(s).name();
    setter.reconfigure(logger, levelName);

  }

  @Override
  public boolean isLoggable(Level level) {
    return Slf4jMonologLevels.of(level).isLoggable(logger);
  }

  @Override
  public void log(Level level, String s) {
    Slf4jMonologLevels.of(level).log(logger, s);
  }

  @Override
  public void log(Level level, String s, Throwable throwable) {
    Slf4jMonologLevels.of(level).log(logger, s, throwable);
  }

  @Override
  public void log(Level level, String s, Object o) {
    Slf4jMonologLevels.of(level).log(logger, s, o);
  }

  @Override
  public void log(Level level, String s, Object[] objects) {
    Slf4jMonologLevels.of(level).log(logger, s, objects);
  }

  @Override
  public String getName() {
    return logger.getName();
  }
}
