/*
 * Copyright (C) 2022 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Original developer: ScalAgent Distributed Technologies
 */
package org.objectweb.util.monolog.api;

/**
 * This interface decouples the logging wrapper from the actual implementation.
 * It allows the effective configuration of the underlying logging system through
 * the initialize method and the search for loggers.
 * It is normally only used by the Debug class (fr.dyade.aaa.common package).
 * 
 * /!\ Be careful, the implementation class needs a public constructor without
 * parameters.
 * 
 * /!\ Call to BasicLevel.initialize(). test to call after initialisation of logging.
 */
public interface LoggingFactory {
  /**
   * Initializes the logging subsystem.
   * 
   * @param debugDir
   * @param debugFileName
   */
  public void initialize(String debugDir, String debugFileName);
  
  /**
   * Find or create a logger for a named subsystem.
   * 
   * @param topic  A name for the logger. This should be a dot-separated name and
   * should normally be based on the package name or class name of the subsystem.
   * @return  a suitable Logger
   */
  public Logger getLogger(String topic);

  /**
   * Returns the Level object for FATAL level.
   * @return the Level object for FATAL level.
   */
  public Level getFatalLevel();

  /**
   * Returns the Level object for ERROR level.
   * @return the Level object for ERROR level.
   */
  public Level getErrorLevel();

  /**
   * Returns the Level object for WARN level.
   * @return the Level object for WARN level.
   */
  public Level getWarnLevel();

  /**
   * Returns the Level object for INFO level.
   * @return the Level object for INFO level.
   */
  public Level getInfoLevel();

  /**
   * Returns the Level object for DEBUG level.
   * @return the Level object for DEBUG level.
   */
  public Level getDebugLevel();

  /**
   * Returns the Level object corresponding to specified name.
   * 
   * @param name  The level name, for example DEBUG or FATAL.
   * @return the Level object corresponding to specified name.
   */
  public Level parse(String name);
}
