/*
 * Copyright (C) 2021 - 2022 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Original developer: ScalAgent Distributed Technologies
 */
package org.objectweb.util.monolog.api;

/**
 * This class wraps old Monolog's BasicLevel on JavaLog Level.
 * It avoids the explicit use of implementation Level objects in Joram code.
 */
public final class BasicLevel {
  public static int BADLEVEL = 0;
//  public static int INHERITLEVEL = -2;
  
  public static Level FATAL = null;
  public static Level ERROR   = null;
  public static Level WARN   = null;
  public static Level INFO   = null;
  public static Level DEBUG   = null;
  
  private static LoggingFactory factory = null;
  
  public static void initialize(LoggingFactory f) {
    factory = f;
    
    FATAL = factory.getFatalLevel();
    ERROR = factory.getErrorLevel();
    WARN = factory.getWarnLevel();
    INFO = factory.getInfoLevel();
    DEBUG = factory.getDebugLevel();
  }
  
  public static int getIntLevel(String name) {
    if (name.equalsIgnoreCase("BasicLevel.DEBUG") || name.equalsIgnoreCase("DEBUG"))
      return DEBUG.intValue();
    else if (name.equalsIgnoreCase("BasicLevel.ERROR") || name.equalsIgnoreCase("ERROR"))
      return ERROR.intValue();
    else if (name.equalsIgnoreCase("BasicLevel.FATAL") || name.equalsIgnoreCase("FATAL"))
      return FATAL.intValue();
    else if (name.equalsIgnoreCase("BasicLevel.INFO") || name.equalsIgnoreCase("INFO"))
      return INFO.intValue();
    else if (name.equalsIgnoreCase("BasicLevel.WARN") || name.equalsIgnoreCase("WARN"))
      return WARN.intValue();
    
    Level level = factory.parse(name);
    if (level != null)
      return level.intValue();
    
    return BADLEVEL;
  }

  public static Level parse(String name) {
    return factory.parse(name);
  }
  
  public static Level fromIntLevel(int level) {
    if (level == FATAL.intValue()) {
      return FATAL;
    } else if (level == ERROR.intValue()) {
      return ERROR;
    } else if (level == WARN.intValue()) {
      return WARN;
    } else if (level == INFO.intValue()) {
      return INFO;
    } else if (level == DEBUG.intValue()) {
      return DEBUG;
    }
    return null;
  }
  public static Level parseBasicLevel(String name) {
    if (name.equalsIgnoreCase("BasicLevel.DEBUG") || name.equalsIgnoreCase("DEBUG"))
      return DEBUG;
    else if (name.equalsIgnoreCase("BasicLevel.ERROR") || name.equalsIgnoreCase("ERROR"))
      return ERROR;
    else if (name.equalsIgnoreCase("BasicLevel.FATAL") || name.equalsIgnoreCase("FATAL"))
      return FATAL;
    else if (name.equalsIgnoreCase("BasicLevel.INFO") || name.equalsIgnoreCase("INFO"))
      return INFO;
    else if (name.equalsIgnoreCase("BasicLevel.WARN") || name.equalsIgnoreCase("WARN"))
      return WARN;
    
    return factory.parse(name);
  }

  public static int getIntLevel(Level level) {
    return level.intValue();
  }
}
