/*
 * Copyright (C) 2022 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): SACKSTEDER Fabien
 */
package org.objectweb.util.monolog.slf4j;

import static org.objectweb.util.monolog.slf4j.Slf4jMonologLevels.*;

import org.objectweb.util.monolog.api.*;
import org.slf4j.LoggerFactory;

/**
 * Gestionnaire et factory principale pour Monolog.
 *
 * Défiit les différents niveaux "standards" de logs
 */
public class Slf4jMonologFactory implements LoggingFactory {

  private static Slf4jLoggerLevelSetter runtimeReconfiguration = Slf4jLoggerLevelSetter.unavailableFeature();

  @Override
  public void initialize(String debugDir, String debugFileName) {
    // non utilisée: la configuration réelle est déléguée au backend
  }

  @Override
  public Logger getLogger(String topic) {
    org.slf4j.Logger slf4jLogger = LoggerFactory.getLogger(topic);
    return new Slf4jMonologLogger(slf4jLogger, runtimeReconfiguration);
  }

  @Override
  public Level getFatalLevel() {
    return Slf4jMonologLevels.ERROR;
  }

  @Override
  public Level getErrorLevel() {
    return ERROR;
  }

  @Override
  public Level getWarnLevel() {
    return WARN;
  }

  @Override
  public Level getInfoLevel() {
    return INFO;
  }

  @Override
  public Level getDebugLevel() {
    return DEBUG;
  }

  @Override
  public Level parse(String name) {
    return Slf4jMonologLevels.of(name);
  }

  public static void setRuntimeReconfiguration(Slf4jLoggerLevelSetter runtimeReconfiguration) {
    if (null != runtimeReconfiguration) {
      Slf4jMonologFactory.runtimeReconfiguration = runtimeReconfiguration;
    }
  }
}
