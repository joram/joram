/*
 * Copyright (C) 2022 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): SACKSTEDER Fabien
 */
package org.objectweb.util.monolog.slf4j;

import org.slf4j.Logger;

/**
 * Stratégie d'écriture des logs de niveau DEBUG
 */
class LevelDebug implements Slf4jMonologLevel {

  @Override
  public String name() {
    return "DEBUG";
  }

  @Override
  public int intValue() {
    return 500;
  }

  @Override
  public boolean isLoggable(Logger logger) {
    return logger.isDebugEnabled();
  }

  @Override
  public void log(Logger logger, String s) {
    logger.debug(s);
  }

  @Override
  public void log(Logger logger, String s, Throwable throwable) {
    logger.debug(s, throwable);
  }

  @Override
  public void log(Logger logger, String s, Object o) {
    logger.debug(s, o);
  }

  @Override
  public void log(Logger logger, String s, Object[] objects) {
    logger.debug(s, objects);
  }
}
