/*
 * Copyright (C) 2022 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): SACKSTEDER Fabien
 */
package org.objectweb.util.monolog.slf4j;

import org.slf4j.Logger;

/**
 * Service de reconfiguration des niveaux de log
 */
@FunctionalInterface
public interface Slf4jLoggerLevelSetter {

  /**
   * modification du niveau d'un logger
   *
   * @param logger le logger
   * @param level le niveau du logger. Les noms 'slf4j' sont supportés
   */
  void reconfigure(Logger logger, String level);

  /**
   * @return un service "bouchon", par défaut
   */
  static Slf4jLoggerLevelSetter unavailableFeature() {
    return (logger, level) -> logger.warn("Reconfiguration du logger ({}) non supportée", level);
  }

}
