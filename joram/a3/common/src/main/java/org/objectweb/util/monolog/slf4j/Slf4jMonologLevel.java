/*
 * Copyright (C) 2022 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): SACKSTEDER Fabien
 */
package org.objectweb.util.monolog.slf4j;

import org.objectweb.util.monolog.api.Level;
import org.slf4j.Logger;

/**
 * Stratégie de logging associée à un niveau particulier
 */
interface Slf4jMonologLevel extends Level {

  /** @return le "nom" du niveau de log. Doit être compatible avec le backend choisi */
  String name();

  @Override
  int intValue();

  boolean isLoggable(Logger logger);

  void log(Logger logger, String s);

  void log(Logger logger, String s, Throwable throwable);

  void log(Logger logger, String s, Object o);

  void log(Logger logger, String s, Object[] objects);

}
