/*
 * Copyright (C) 2022 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): SACKSTEDER Fabien
 */
package org.objectweb.util.monolog.slf4j;

import org.objectweb.util.monolog.api.Level;

/**
 * Ensemble des stratégies de logging en fonction des niveaux de log
 */
class Slf4jMonologLevels {

  static final Slf4jMonologLevel ERROR = new LevelError();
  static final Slf4jMonologLevel WARN = new LevelWarn();
  static final Slf4jMonologLevel INFO = new LevelInfo();
  static final Slf4jMonologLevel DEBUG = new LevelDebug();
  static final Slf4jMonologLevel TRACE = new LevelTrace();
  static final Slf4jMonologLevel OFF = new LevelOff();

  private static final Slf4jMonologLevel[] VALUES = new Slf4jMonologLevel[] {
      ERROR, WARN, INFO, DEBUG, TRACE, OFF
  };

  /**
   * Correspondance à partir d'un niveau de log. Les noms supportés seront ceux de slf4j, log4j2, jul, ...
   *
   * @param name le nom du niveau souhaité
   * @return le niveau associé au nom fourni
   */
  static Slf4jMonologLevel of(String name) {
    if (null == name) {
      return null;
    }
    switch (name.toUpperCase()) {
    case "OFF": return OFF;
    case "FATAL":
    case "ERROR": return ERROR;
    case "WARNING":
    case "WARN": return WARN;
    case "CONFIG":
    case "INFO": return INFO;
    case "FINE":
    case "DEBUG": return DEBUG;
    case "TRACE":
    case "FINER":
    case "FINEST":
    case "ALL": return TRACE;
    default: return null;
    }
//    return switch (name.toUpperCase()) {
//      case "OFF" -> OFF;
//      case "FATAL", "ERROR" -> ERROR;
//      case "WARNING", "WARN" -> WARN;
//      case "CONFIG", "INFO" -> INFO;
//      case "FINE", "DEBUG" -> DEBUG;
//      case "TRACE", "FINER", "FINEST", "ALL" -> TRACE;
//      default -> null;
//    };
  }

  /**
   * Normalisation du niveau de log.
   * Permet de choisir une stratégie à partir de la valeur d'un niveau (si le niveau passé en paramètre n'est pas de type {@link Slf4jMonologLevel}
   *
   * @param level niveau à noramliser
   */
  static Slf4jMonologLevel of(Level level) {
    if (level instanceof Slf4jMonologLevel) {
      return (Slf4jMonologLevel) level;
    }
    if (null == level) {
      return DEBUG; // par défaut
    }
    int lvl = level.intValue();
    for (Slf4jMonologLevel l : VALUES) {
      if (lvl >= l.intValue()) {
        return l;
      }
    }
    return OFF;
  }

  Slf4jMonologLevels() {
    // classe utilitaire
  }
}
