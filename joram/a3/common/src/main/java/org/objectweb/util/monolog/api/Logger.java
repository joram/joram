/*
 * Copyright (C) 2022 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Original developer: ScalAgent Distributed Technologies
 */
package org.objectweb.util.monolog.api;

/**
 * This interface decouples the logging wrapper from the actual implementation.
 * It allows to leave unchanged the code previously using Monolog.
 */
public interface Logger {
  /**
   * Sets the Level attribute of the Logger object.
   * If level is null inherits from parent logger. 
   *
   * @param  level  The new level value
   */
  public void setLevel(Level level);

  /**
   * Sets the Level attribute of the Logger object.
   * If level is null inherits from parent logger. 
   *
   * @param  level  The new level value.
   * @throws Exception The level is unknown.
   */
  public void setLevel(String level) throws Exception;
  

  /**
   *  Gets the Loggable attribute of the LoggerImpl object
   *
   * @param level  Description of Parameter
   * @return The Loggable value
   */
  public boolean isLoggable(Level level);

  /**
   * Log a message, with no arguments.
   * If the logger is currently enabled for the given message level then the
   * given message is treated
   */
  public void log(Level level, String msg);

  /**
   * Log a message, with a throwable arguments which can represent an 
   * error or a context..
   */
  public void log(Level level, String msg, Throwable t);

  /**
   * Log a message, with one object parameter.
   * <p>
   * If the logger is currently enabled for the given message
   * level then a corresponding LogRecord is created and forwarded
   * to all the registered output Handler objects.
   *
   * @param   level   One of the message level identifiers, e.g., SEVERE
   * @param   msg     The string message (or a key in the message catalog)
   * @param   param1  parameter to the message
   */
  public void log(Level level, String msg, Object param1);

  /**
   * Log a message, with an array of object arguments.
   * <p>
   * If the logger is currently enabled for the given message
   * level then a corresponding LogRecord is created and forwarded
   * to all the registered output Handler objects.
   *
   * @param   level   One of the message level identifiers, e.g., SEVERE
   * @param   msg     The string message (or a key in the message catalog)
   * @param   params  array of parameters to the message
   */
  public void log(Level level, String msg, Object params[]);

  public String getName();
}
