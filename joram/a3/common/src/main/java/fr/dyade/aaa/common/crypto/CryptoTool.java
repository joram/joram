/*
 * Copyright (C) 2025 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): ScalAgent Distributed Technologies
 * Contributor(s): 
 */
package fr.dyade.aaa.common.crypto;

import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public abstract class CryptoTool implements Crypto {
  protected SecretKey key = null;

  /**
   * Derive the key from given password and salt.
   * 
   * @param password
   * @param salt
   * @param iterationCount
   * @param keyLength
   * @throws NoSuchAlgorithmException
   * @throws InvalidKeySpecException
   */
  protected CryptoTool(String password, String salt, int iterationCount, int keyLength) throws NoSuchAlgorithmException, InvalidKeySpecException {
    SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
    KeySpec keySpec = new PBEKeySpec(password.toCharArray(), salt.getBytes(), iterationCount, keyLength);
    byte[] secret = factory.generateSecret(keySpec).getEncoded();
    key = new SecretKeySpec(secret, "AES");
  }

  abstract Cipher getCipher(int mode) throws GeneralSecurityException;
  abstract Cipher getCipher(int mode, byte[] nonce) throws GeneralSecurityException;
  
  @Override
  public byte[] encrypt(byte[] clearText) throws GeneralSecurityException {
    return getCipher(Cipher.ENCRYPT_MODE).doFinal(clearText);
  }
  
  @Override
  public byte[] encrypt(byte[] clearText, byte[] nonce) throws GeneralSecurityException {
    return getCipher(Cipher.ENCRYPT_MODE, nonce).doFinal(clearText);
  }

  @Override
  public byte[] decrypt(byte[] cipherText) throws GeneralSecurityException {
    // If encrypted data is altered, during decryption authentication tag verification will fail
    // resulting in AEADBadTagException
    return getCipher(Cipher.DECRYPT_MODE).doFinal(cipherText);
  }

  @Override
  public byte[] decrypt(byte[] cipherText, byte[] nonce) throws GeneralSecurityException {
    // If encrypted data is altered, during decryption authentication tag verification will fail
    // resulting in AEADBadTagException
    return getCipher(Cipher.DECRYPT_MODE, nonce).doFinal(cipherText);
  }
}