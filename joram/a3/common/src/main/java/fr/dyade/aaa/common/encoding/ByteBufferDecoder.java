/*
 * Copyright (C) 2013 - 2022 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 * 
 * Initial developer(s): ScalAgent Distributed Technologies
 * Contributor(s): 
 */
package fr.dyade.aaa.common.encoding;

import java.nio.Buffer;
import java.nio.ByteBuffer;

/**
 * Decoder using a byte buffer.
 */
public final class ByteBufferDecoder implements Decoder {
  private ByteBuffer buf;

  public ByteBufferDecoder(ByteBuffer buf) {
    super();
    this.buf = buf;
  }

  @Override
  public short decodeSignedShort() throws Exception {
    return buf.getShort();
  }

  @Override
  public short decodeUnsignedShort() throws Exception {
    return buf.getShort();
  }
  
  @Override
  public short decode16() throws Exception {
    return buf.getShort();
  }

  @Override
  public int decodeSignedInt() throws Exception {
    return buf.getInt();
  }

  @Override
  public int decodeUnsignedInt() throws Exception {
    return buf.getInt();
  }
  
  @Override
  public int decode32() throws Exception {
    return buf.getInt();
  }

  @Override
  public long decodeUnsignedLong() throws Exception {
    return buf.getLong();
  }

  @Override
  public long decodeSignedLong() throws Exception {
    return buf.getLong();
  }
  
  @Override
  public long decode64() throws Exception {
    return buf.getLong();
  }
  
  private boolean isNull() throws Exception {
    return decodeBoolean();
  }

  @Override
  public String decodeNullableString() throws Exception {
    if (isNull()) return null;
    return decodeString();
  }

  @Override
  public String decodeString() throws Exception {
    int length = buf.getInt();
    return decodeString(length);
  }

  @Override
  public String decodeString(int length) throws Exception {
    String s;
    if (buf.hasArray()) {
      s = new String(buf.array(), ((Buffer) buf).position(), length, EncodableHelper.charset);
      ((Buffer) buf).position(((Buffer) buf).position() + length);
    } else {
      byte[] bytes = new byte[length];
      buf.get(bytes);
      s = new String(bytes, EncodableHelper.charset);  
    }
    return s;
  }

  @Override
  public byte decodeByte() throws Exception {
    return buf.get();
  }

  @Override
  public byte[] decodeNullableByteArray() throws Exception {
    if (isNull()) return null;
    return decodeByteArray();
  }

  @Override
  public byte[] decodeByteArray() throws Exception {
    int length = buf.getInt();
    return decodeByteArray(length);
  }

  @Override
  public byte[] decodeByteArray(int length) throws Exception {
    byte[] bytes = new byte[length];
    buf.get(bytes);
    return bytes;
  }

  @Override
  public boolean decodeBoolean() throws Exception {
    byte b = buf.get();
    return (b != 0);
  }

  @Override
  public float decodeFloat() throws Exception {
    return buf.getFloat();
  }

  @Override
  public double decodeDouble() throws Exception {
    return buf.getDouble();
  }
}
