/*
 * Copyright (C) 2025 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): ScalAgent Distributed Technologies
 * Contributor(s): 
 */
package fr.dyade.aaa.common.crypto;

import java.security.GeneralSecurityException;

public class VoidTool implements Crypto {
  public VoidTool() {}

  @Override
  public byte[] encrypt(byte[] clearText) throws GeneralSecurityException {
    return clearText;
  }

  @Override
  public byte[] decrypt(byte[] cipherText) throws GeneralSecurityException {
    return cipherText;
  }

  @Override
  public byte[] encrypt(byte[] clearText,
                        byte[] nonce) throws GeneralSecurityException {
    return clearText;
  }

  @Override
  public byte[] decrypt(byte[] cipherText,
                        byte[] nonce) throws GeneralSecurityException {
    return cipherText;
  }
}
