/*
 * Copyright (C) 2013 - 2022 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 * 
 * Initial developer(s): ScalAgent Distributed Technologies
 * Contributor(s): 
 */
package fr.dyade.aaa.common.encoding;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectStreamClass;

public final class MigrationControlInputStream extends ObjectInputStream {
  public MigrationControlInputStream(InputStream in) throws IOException {
    super(in);
  }

  protected ObjectStreamClass readClassDescriptor() throws IOException, ClassNotFoundException {
    ObjectStreamClass resultClassDescriptor = super.readClassDescriptor(); // initially streams descriptor
    Class localClass = Class.forName(resultClassDescriptor.getName()); // the class in the local JVM that this descriptor represents.
    if (localClass == null) {
      //System.out.println("No local class for " + resultClassDescriptor.getName());
      return resultClassDescriptor;
    }
    ObjectStreamClass localClassDescriptor = ObjectStreamClass.lookup(localClass);
    if (localClassDescriptor != null) { // only if class implements Serializable
      final long localSUID = localClassDescriptor.getSerialVersionUID();
      final long streamSUID = resultClassDescriptor.getSerialVersionUID();
      if (streamSUID != localSUID) { // check for serialVersionUID mismatch.
        //System.out.println("serialVersionUID mismatch for class " + resultClassDescriptor.getName() + " from uid " + streamSUID + " to uid " + localSUID);
        // check for authorized overriding
        // class org.objectweb.joram.mom.dest.Destination,
        //  stream -6697938753761953568L (version 5.16.1), local -4457639325610333885L (versions 5.16.0, 5.16.3, 5.17.x)
        if (resultClassDescriptor.getName().equals("org.objectweb.joram.mom.dest.Destination") &&
            streamSUID == -6697938753761953568L && localSUID == -4457639325610333885L) {
          //final StringBuffer s = new StringBuffer("Overriding serialized class version mismatch: ");
          //s.append("local serialVersionUID = ").append(localSUID);
          //s.append(" stream serialVersionUID = ").append(streamSUID);
          //Exception e = new InvalidClassException(s.toString());
          //System.out.println("Potentially Fatal Deserialization Operation. " + e);
          resultClassDescriptor = localClassDescriptor; // Use local class descriptor for deserialization
        }
      }
    }
    return resultClassDescriptor;
  }
}
