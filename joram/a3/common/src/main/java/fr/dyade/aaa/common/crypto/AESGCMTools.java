/*
 * Copyright (C) 2025 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): ScalAgent Distributed Technologies
 * Contributor(s): 
 */
package fr.dyade.aaa.common.crypto;

import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.Cipher;
import javax.crypto.spec.GCMParameterSpec;

public class AESGCMTools extends CryptoTool {
  //An authentication tag size in bits
  private static final int GCM_TAG_LENGTH = 128;

  private String algorithm = null;
  private GCMParameterSpec iv = null;
  
  /**
   * 
   * @param password
   * @param salt  A salt is a unique, randomly generated string that is added to each password as part of the hashing process.
   * @param nonce A nonce or an initialization vector is a random value chosen at encryption time and meant to be used only once.
   * @param iterationCount
   * @param keyLength
   * @throws NoSuchAlgorithmException
   * @throws InvalidKeySpecException
   */
  public AESGCMTools(String password, String salt, String nonce, int iterationCount, int keyLength) throws NoSuchAlgorithmException, InvalidKeySpecException  {
    this("AES/GCM/NoPadding", password, salt, nonce, iterationCount, keyLength);
  }

  public AESGCMTools(String algorithm, String password, String salt, String nonce, int iterationCount, int keyLength) throws NoSuchAlgorithmException, InvalidKeySpecException  {
      super(password, salt, iterationCount, keyLength);

    this.algorithm = algorithm;
    this.iv = new GCMParameterSpec(GCM_TAG_LENGTH, nonce.getBytes());
  }
  
  @Override
  Cipher getCipher(int mode) throws GeneralSecurityException {
    // AES-GCM encryption
    Cipher cipher = Cipher.getInstance(algorithm);
    cipher.init(mode, key, iv);
    return cipher;
  }

  @Override
  Cipher getCipher(int mode, byte[] nonce) throws GeneralSecurityException {
    // AES-GCM encryption
    Cipher cipher = Cipher.getInstance(algorithm);
    cipher.init(mode, key, new GCMParameterSpec(GCM_TAG_LENGTH, nonce));
    return cipher;
  }

  public static Crypto createCrypto(String algorithm, String password,
                                        String salt, String iv, int iterationCount,
                                        int keyLength) throws NoSuchAlgorithmException, InvalidKeySpecException {
    return new AESGCMTools(algorithm, password, salt, iv, iterationCount, keyLength);
  }
}