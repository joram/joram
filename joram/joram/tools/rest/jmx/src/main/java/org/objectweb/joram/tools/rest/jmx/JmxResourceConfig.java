package org.objectweb.joram.tools.rest.jmx;

import org.glassfish.jersey.server.ResourceConfig;

public class JmxResourceConfig extends ResourceConfig {
    public JmxResourceConfig(){
        register(JmxRestService.class);
    }
}
