package org.objectweb.joram.tools.rest.admin;

import org.osgi.framework.Bundle;
import org.osgi.service.http.context.ServletContextHelper;

public class AdminServletContext extends ServletContextHelper {
  public AdminServletContext(final Bundle bundle) {
    super(bundle);
  }
}
