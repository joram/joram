package org.objectweb.joram.tools.rest.jms;

import org.osgi.framework.Bundle;
import org.osgi.service.http.context.ServletContextHelper;

/**
 * Simple JMS servlet context
 */
public class JmsServletContext extends ServletContextHelper {
  public JmsServletContext(final Bundle bundle) {
    super(bundle);
  }
}
