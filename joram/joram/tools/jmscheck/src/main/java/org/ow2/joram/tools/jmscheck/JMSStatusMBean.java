/*
 * Copyright (C) 2020 - 2021 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): ScalAgent Distributed Technologies
 * Contributor(s): 
 */
package org.ow2.joram.tools.jmscheck;

public interface JMSStatusMBean {

  /**
   * Starts the JMS healthcheck component.
   * 
   * @throws Exception
   */
  void start() throws Exception;
  
  /**
   * Stops the JMS healthcheck component.
   * 
   * @throws Exception
   */
  void stop() throws Exception;

  /**
   * Returns the global healthcheck indication, 0 if all connectors are running, 1 otherwise.
   * 
   * @return the global healthcheck indication.
   */
  int getStatus();

  /**
   * Returns a user friendly indication of the global healthcheck: RUNNING or UNREACHABLE.
   * 
   * @return a user friendly indication of the global healthcheck
   */
  String getStatusInfo();

  /**
   * Returns the activation period in seconds.
   * 
   * @return the activation period in seconds.
   */
  int getPeriod();

  /**
   * Sets the activation period in seconds.
   * 
   * @param period the activation period in seconds.
   */
  void setPeriod(int period);

  /**
   * Returns the timeout used during check.
   * 
   * @return the timeout used during check.
   */
  int getTimeout();

  /**
   * Sets the timeout used during check.
   * 
   * @param timeout the timeout used during check.
   */
  void setTimeout(int timeout);

  /**
   * Returns the minimum number of failures before generating a dump.
   * 
   * @return the minimum number of failures before generating a dump.
   */
  int getThreshold();

  /**
   * Sets the minimum number of failures before generating a dump.
   * 
   * @param threshold the minimum number of failures before generating a dump.
   */
  void setThreshold(int threshold);

  /**
   * Returns the minimal delay between 2 dumps.
   * 
   * @return the minimal delay between 2 dumps.
   */
  int getDelay();

  /**
   * Sets the minimal delay between 2 dumps.
   *  
   * @param delay the minimal delay between 2 dumps.
   */
  void setDelay(int delay);

  /**
   * Generates a server dump.
   * 
   * @param path  Pathname of file to create.
   */
  void dumpServerState(String path);
}
