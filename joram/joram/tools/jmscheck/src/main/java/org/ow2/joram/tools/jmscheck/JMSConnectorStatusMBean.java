/*
 * Copyright (C) 2020 - 2021 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): ScalAgent Distributed Technologies
 * Contributor(s): 
 */
package org.ow2.joram.tools.jmscheck;

public interface JMSConnectorStatusMBean {
  String getName();
  
  /**
   * Returns the period of time between 2 checks.
   * @return the period of time between 2 checks.
   */
  int getPeriod();

  /**
   * Sets the period of time between 2 checks.
   * @param period the period of time between 2 checks.
   */
  void setPeriod(int period);

  /**
   * Returns the maximum time waiting for connection.
   * @return the maximum time waiting for connection.
   */
  int getTimeOut();

  /**
   * Sets the maximum time waiting for connection.
   * @param timeOut the maximum time waiting for connection.
   */
  void setTimeOut(int timeOut);

  /**
   * Returns the status of the connector: 0 => RUNNING, >0 => UNREACHABLE.
   * @return the number of unreachable connection
   */
  int getStatus();

  String getStatusInfo();

  /**
   * Returns the total number of try since starting.
   * @return the total number of try since starting.
   */
  int getNbTry();

  /**
   * Returns the total number of failures since starting.
   * @return the total number of failures since starting.
   */
  int getNbFailures();

  /**
   * Returns the error message of the last try.
   * Returns null if the test is successful.
   * @return
   */
  String getErrorMsg();

  /**
   * Returns the date of last successful connection.
   * @return the date of last successful connection.
   */
  String getLastConnectTime();

  /**
   * Returns the latency of the connection during the last try.
   * Returns -1 if the test failed.
   * @return the latency of the connection during the last try.
   */
  long getLatencyConnect();

  /**
   * Returns the latency of the send/receive during the last try.
   * Returns -1 if the test failed.
   * @return the latency of the send/receive during the last try.
   */
  long getLatencyPubSub();
}
