/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C) 2012 - 2022 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 */
package org.ow2.joram.jakarta.connector;

import jakarta.jms.JMSException;
import jakarta.jms.MessageConsumer;
import jakarta.jms.Queue;
import jakarta.jms.JMSSecurityException;
import jakarta.jms.Connection;

import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;
import org.ow2.joram.jakarta.jms.TemporaryQueue;

import fr.dyade.aaa.common.Debug;

/**
 * An <code>OutboundReceiver</code> instance wraps a JMS PTP consumer
 * for a component involved in outbound messaging. 
 */
public class OutboundReceiver extends OutboundConsumer implements jakarta.jms.QueueReceiver {
  private static final Logger logger = Debug.getLogger(OutboundReceiver.class.getName());
  
  /** Queue instance to consume messages from. */
  private Queue queue;

  /**
   * Constructs an <code>OutboundReceiver</code> instance.
   */
  OutboundReceiver(Queue queue, MessageConsumer consumer, OutboundSession session) throws JMSException {
    super(consumer, session);
    this.queue = queue;
    
    if (logger.isLoggable(BasicLevel.DEBUG))
      logger.log(BasicLevel.DEBUG,
                 "OutboundReceiver(" + queue +  ", " + consumer + ", " + session + ")");
    
    if (queue instanceof TemporaryQueue) {
      Connection tempQCnx = ((TemporaryQueue) queue).getCnx();

      if (tempQCnx == null || !session.cnx.cnxEquals(tempQCnx))
        throw new JMSSecurityException("Forbidden consumer on this temporary destination.");
    }
  }

  // ************************************************************
  // Implementation of QueueReceiver
  // ************************************************************

  /** Returns the consumer's queue. */
  @Override
  public Queue getQueue() throws JMSException {
    if (logger.isLoggable(BasicLevel.DEBUG))
      logger.log(BasicLevel.DEBUG, this + " getQueue() = " + queue);

    checkValidity();
    return queue;
  }
}
