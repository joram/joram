/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C) 2012 - 2024 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 */
package org.ow2.joram.jakarta.connector;

import jakarta.jms.ConnectionConsumer;
import jakarta.jms.IllegalStateException;
import jakarta.jms.JMSException;
import jakarta.jms.Queue;
import jakarta.jms.QueueConnection;
import jakarta.jms.QueueSession;
import jakarta.jms.ServerSessionPool;
import jakarta.jms.Session;
import jakarta.jms.XAQueueConnection;

import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

import fr.dyade.aaa.common.Debug;

/**
 * An <code>OutboundQueueConnection</code> instance is a handler for a
 * physical PTP connection to an underlying JORAM server, allowing a
 * component to transparently use this physical connection possibly within
 * a transaction (local or global).
 */
public class OutboundQueueConnection extends OutboundConnection implements QueueConnection {
  private static final Logger logger = Debug.getLogger(OutboundQueueConnection.class.getName());
  
  /**
   * Constructs an <code>OutboundQueueConnection</code> instance.
   *
   * @param managedCx  The managed connection building the handle.
   * @param xac        The underlying physical PTP connection to handle.
   */
  OutboundQueueConnection(ManagedConnectionImpl managedCx, XAQueueConnection xac) {
    super(managedCx, xac);
    if (logger.isLoggable(BasicLevel.DEBUG))
      logger.log(BasicLevel.DEBUG,
                 "OutboundQueueConnection(" + managedCx + ", " + xac + ")");
  }
  
  public boolean cnxEquals(Object obj) {
    return (obj instanceof QueueConnection)
           && xac.equals(obj);
  }
  
  // ************************************************************
  // Implementation of QueueConnection
  // ************************************************************
 
  /**
   * Returns the unique authorized JMS session per connection wrapped in
   * an <code>OutboundQueueSession</code> instance.
   *
   * @exception jakarta.jms.IllegalStateException  If the handle is invalid.
   * @exception jakarta.jms.JMSException           Generic exception.
   */
  @Override
  public QueueSession createQueueSession(boolean transacted, int acknowledgeMode) throws JMSException {
    if (logger.isLoggable(BasicLevel.DEBUG))
      logger.log(BasicLevel.DEBUG, this + " createQueueSession(" + transacted + ", " + acknowledgeMode +  ")");

    if (! valid)
      throw new jakarta.jms.IllegalStateException("Invalid connection handle.");

    Session sess = createManagedSession(acknowledgeMode);
    return new OutboundQueueSession(sess, this, transacted);
  }

  /**
   * Forbidden call on an application or component's outbound connection,
   * throws a <code>IllegalStateException</code> instance.
   */
  @Override
  public ConnectionConsumer createConnectionConsumer(Queue queue,
                                  String messageSelector,
                                  ServerSessionPool sessionPool,
                                  int maxMessages) throws JMSException {
    throw new IllegalStateException("Forbidden call on a component's "
                                    + "connection.");
  }
}
