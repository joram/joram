/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C) 2012 - 2022 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 */
package org.ow2.joram.jakarta.connector;

import jakarta.jms.JMSException;
import jakarta.jms.Queue;
import jakarta.jms.Session;

import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

import fr.dyade.aaa.common.Debug;

/**
 * An <code>OutboundQueueSession</code> instance wraps a JMS QueueSession
 * (XA or not) for a component involved in PTP outbound messaging.
 */
public class OutboundQueueSession extends OutboundSession implements jakarta.jms.QueueSession {
  private static final Logger logger = Debug.getLogger(OutboundQueueSession.class.getName());
  
  /**
   * Constructs an <code>OutboundQueueSession</code> instance.
   */
  OutboundQueueSession(Session sess, OutboundConnection cnx) {
    super(sess, cnx);
    
    if (logger.isLoggable(BasicLevel.DEBUG))
      logger.log(BasicLevel.DEBUG, "OutboundQueueSession(" + sess + ", " + cnx + ")");
  }

  /**
   * Constructs an <code>OutboundQueueSession</code> instance.
   */
  OutboundQueueSession(Session sess, OutboundConnection cnx, boolean transacted) {
    super(sess, cnx, transacted);
    
    if (logger.isLoggable(BasicLevel.DEBUG))
      logger.log(BasicLevel.DEBUG, "OutboundQueueSession(" + sess + ", " + cnx + ")");
  }

  // ************************************************************
  // QueueSession implementation
  // ************************************************************

  /**
   * Delegates the call to the wrapped JMS session.
   */
  @Override
  public jakarta.jms.QueueSender createSender(Queue queue)
    throws JMSException {
    if (logger.isLoggable(BasicLevel.DEBUG))
      logger.log(BasicLevel.DEBUG, this + " createSender(" + queue + ")");

    checkValidity();
    return new OutboundSender(sess.createProducer(queue), this);
  }

  /**
   * Delegates the call to the wrapped JMS session.
   */
  @Override
  public jakarta.jms.QueueReceiver createReceiver(Queue queue, String selector)
    throws JMSException {
    if (logger.isLoggable(BasicLevel.DEBUG))
      logger.log(BasicLevel.DEBUG, this + " createReceiver(" + queue + ", " + selector + ")");

    checkValidity();
    return new OutboundReceiver(queue,
                                sess.createConsumer(queue, selector),
                                this);
  }

  /**
   * Delegates the call to the wrapped JMS session.
   */
  @Override
  public jakarta.jms.QueueReceiver createReceiver(Queue queue)
    throws JMSException {
    if (logger.isLoggable(BasicLevel.DEBUG))
      logger.log(BasicLevel.DEBUG, this + " createReceiver(" + queue + ")");

    checkValidity();
    return new OutboundReceiver(queue, sess.createConsumer(queue), this);
  }

  /** 
   * API method.
   *
   * @exception jakarta.jms.IllegalStateException  Systematically.
   */
  @Override
  public jakarta.jms.TopicSubscriber
      createDurableSubscriber(jakarta.jms.Topic topic, 
                              String name,
                              String selector,
                              boolean noLocal) 
    throws JMSException  {
    throw new jakarta.jms.IllegalStateException("Forbidden call on a OutboundQueueSession.");
  }

  /** 
   * API method.
   *
   * @exception jakarta.jms.IllegalStateException  Systematically.
   */
  @Override
  public jakarta.jms.TopicSubscriber
         createDurableSubscriber(jakarta.jms.Topic topic, 
                                 String name)
         throws JMSException {
    throw new jakarta.jms.IllegalStateException("Forbidden call on a OutboundQueueSession.");
  }

  /**
   * API method.
   *
   * @exception jakarta.jms.IllegalStateException  Systematically.
   */
  @Override
  public jakarta.jms.Topic createTopic(String topicName) 
    throws JMSException {
    throw new jakarta.jms.IllegalStateException("Forbidden call on a OutboundQueueSession.");
  }

  /**
   * API method.
   *
   * @exception jakarta.jms.IllegalStateException  Systematically.
   */
  @Override
  public jakarta.jms.TemporaryTopic createTemporaryTopic() 
    throws JMSException {
    throw new jakarta.jms.IllegalStateException("Forbidden call on a OutboundQueueSession.");
  }

  /**
   * API method.
   *
   * @exception jakarta.jms.IllegalStateException  Systematically.
   */
  @Override
  public void unsubscribe(String name) 
    throws JMSException {
    throw new jakarta.jms.IllegalStateException("Forbidden call on a OutboundQueueSession.");
  }    
}
