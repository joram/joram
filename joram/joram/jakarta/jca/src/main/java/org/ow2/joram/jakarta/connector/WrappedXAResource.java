/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C) 2019 - 2022 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 */
package org.ow2.joram.jakarta.connector;

import javax.transaction.xa.XAException;
import javax.transaction.xa.XAResource;
import javax.transaction.xa.Xid;

import jakarta.jms.XASession;

/**
 * Currently unused.
 */
public class WrappedXAResource implements javax.transaction.xa.XAResource {
  ManagedConnectionImpl managedCx = null;
  
  /**
   * Constructs an XA resource representing a given session.
   */
  public WrappedXAResource(ManagedConnectionImpl managedCx) {
    this.managedCx = managedCx;
  }

  @Override
  public void commit(Xid xid, boolean onePhase) throws XAException {
    ((XASession) managedCx.getSession()).getXAResource().commit(xid, onePhase);
  }

  @Override
  public void end(Xid xid, int flags) throws XAException {
    ((XASession) managedCx.getSession()).getXAResource().end(xid, flags);
  }

  @Override
  public void forget(Xid xid) throws XAException {
    ((XASession) managedCx.getSession()).getXAResource().forget(xid);
  }

  @Override
  public int getTransactionTimeout() throws XAException {
    return ((XASession) managedCx.getSession()).getXAResource().getTransactionTimeout();
  }

  @Override
  public boolean isSameRM(XAResource xares) throws XAException {
    return ((XASession) managedCx.getSession()).getXAResource().isSameRM(xares);
  }

  @Override
  public int prepare(Xid xid) throws XAException {
    return ((XASession) managedCx.getSession()).getXAResource().prepare(xid);
  }

  @Override
  public Xid[] recover(int flag) throws XAException {
    return ((XASession) managedCx.getSession()).getXAResource().recover(flag);
  }

  @Override
  public void rollback(Xid xid) throws XAException {
    ((XASession) managedCx.getSession()).getXAResource().rollback(xid);
  }

  @Override
  public boolean setTransactionTimeout(int seconds) throws XAException {
    return ((XASession) managedCx.getSession()).getXAResource().setTransactionTimeout(seconds);
  }

  @Override
  public void start(Xid xid, int flags) throws XAException {
    ((XASession) managedCx.getSession()).getXAResource().start(xid, flags);
  }

}
