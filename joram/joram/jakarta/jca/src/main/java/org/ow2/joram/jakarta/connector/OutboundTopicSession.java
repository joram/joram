/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C) 2012 - 2022 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 */
package org.ow2.joram.jakarta.connector;

import jakarta.jms.JMSException;
import jakarta.jms.MessageConsumer;
import jakarta.jms.Session;
import jakarta.jms.Topic;
import jakarta.jms.TopicSubscriber;

import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

import fr.dyade.aaa.common.Debug;

/**
 * An <code>OutboundTopicSession</code> instance wraps a JMS TopicSession
 * (XA or not) for a component involved in PubSub outbound messaging.
 */
public class OutboundTopicSession extends OutboundSession implements jakarta.jms.TopicSession {
  private static final Logger logger = Debug.getLogger(OutboundTopicSession.class.getName());
  
  /**
   * Constructs an <code>OutboundTopicSession</code> instance.
   */
  OutboundTopicSession(Session sess, OutboundConnection cnx) {
    super(sess, cnx);

    if (logger.isLoggable(BasicLevel.DEBUG))
      logger.log(BasicLevel.DEBUG, "OutboundTopicSession(" + sess + ", " + cnx + ")");
  }

  /**
   * Constructs an <code>OutboundTopicSession</code> instance.
   */
  OutboundTopicSession(Session sess,  OutboundConnection cnx, boolean transacted) {
    super(sess, cnx, transacted);

    if (logger.isLoggable(BasicLevel.DEBUG))
      logger.log(BasicLevel.DEBUG, "OutboundTopicSession(" + sess + ", " + cnx + ")");
  }

  /**
   * Delegates the call to the wrapped JMS session.
   */
//  public TopicSubscriber createSubscriber(Topic topic, String selector) throws JMSException {
//    if (logger.isLoggable(BasicLevel.DEBUG))
//      logger.log(BasicLevel.DEBUG, this + " createSubscriber(" + topic + ", " + selector + ")");
//
//    checkValidity();
//    MessageConsumer cons = sess.createConsumer(topic, selector);
//    return new OutboundSubscriber(topic, false, cons, this);
//  }

  // ************************************************************
  // TopicSession implementation
  // ************************************************************

  /**
   * Delegates the call to the wrapped JMS session.
   */
  @Override
  public jakarta.jms.TopicPublisher createPublisher(Topic topic)
    throws JMSException {
    if (logger.isLoggable(BasicLevel.DEBUG))
      logger.log(BasicLevel.DEBUG, this + " createPublisher(" + topic + ")");

    checkValidity();
    return new OutboundPublisher(sess.createProducer(topic), this);
  }

  /**
   * Delegates the call to the wrapped JMS session.
   */
  @Override
  public TopicSubscriber createSubscriber(Topic topic, String selector, boolean noLocal)
    throws JMSException {
    if (logger.isLoggable(BasicLevel.DEBUG))
      logger.log(BasicLevel.DEBUG, this + " createSubscriber(" + topic + ", " + selector + ", " + noLocal + ")");

    checkValidity();
    MessageConsumer cons = sess.createConsumer(topic, selector, noLocal);
    return new OutboundSubscriber(topic, noLocal, cons, this);
  }

  /**
   * Delegates the call to the wrapped JMS session.
   */
  @Override
  public TopicSubscriber createSubscriber(Topic topic) 
    throws JMSException {
    if (logger.isLoggable(BasicLevel.DEBUG))
      logger.log(BasicLevel.DEBUG, this + " createSubscriber(" + topic + ")");

    checkValidity();
    return new OutboundSubscriber(topic, false, sess.createConsumer(topic), this);
  }

  /**
   * API method.
   *
   * @exception jakarta.jms.IllegalStateException  Systematically.
   */
  @Override
  public jakarta.jms.QueueBrowser
      createBrowser(jakarta.jms.Queue queue,  String selector) throws JMSException {
    throw new jakarta.jms.IllegalStateException("Forbidden call on a TopicSession.");
  }
  
  /**
   * API method.
   *
   * @exception jakarta.jms.IllegalStateException  Systematically.
   */
  public jakarta.jms.QueueBrowser createBrowser(jakarta.jms.Queue queue)
    throws JMSException {
    throw new jakarta.jms.IllegalStateException("Forbidden call on a TopicSession.");
  }
  
  /**
   * API method.
   *
   * @exception jakarta.jms.IllegalStateException  Systematically.
   */
  public jakarta.jms.Queue createQueue(String queueName) 
    throws JMSException {
    throw new jakarta.jms.IllegalStateException("Forbidden call on a TopicSession.");
  }
  
  /**
   * API method.
   *
   * @exception jakarta.jms.IllegalStateException  Systematically.
   */
  public jakarta.jms.TemporaryQueue createTemporaryQueue() 
    throws JMSException {
    throw new jakarta.jms.IllegalStateException("Forbidden call on a TopicSession.");
  }
}
