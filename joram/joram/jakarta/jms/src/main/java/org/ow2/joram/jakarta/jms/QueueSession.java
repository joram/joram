/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C) 2001 - 2019 ScalAgent Distributed Technologies
 * Copyright (C) 1996 - 2000 Dyade
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): Frederic Maistre (INRIA)
 * Contributor(s):
 */
package org.ow2.joram.jakarta.jms;

import org.ow2.joram.jakarta.jms.connection.RequestMultiplexer;

import jakarta.jms.IllegalStateException;
import jakarta.jms.JMSException;

/**
 * Implements the <code>jakarta.jms.QueueSession</code> interface.
 */
public class QueueSession extends Session implements jakarta.jms.QueueSession {
  /**
   * Constructs a queue session.
   *
   * @param cnx  The connection the session belongs to.
   * @param transacted  <code>true</code> for a transacted session.
   * @param acknowledgeMode  1 (auto), 2 (client) or 3 (dups ok).
   *
   * @exception JMSException  In case of an invalid acknowledge mode.
   */
  QueueSession(Connection cnx, 
               boolean transacted,
               int acknowledgeMode,
               RequestMultiplexer mtpx) throws JMSException {
    super(cnx, transacted, acknowledgeMode, mtpx);
  }

  /**
   * API method.
   * Creates a QueueSender object to send messages to the specified queue.
   * 
   * @param queue the queue to send to, or null if this is a sender which does
   *              not have a specified destination.
   * @return the created QueueSender object.
   *
   * @exception IllegalStateException  If the session is closed or if the 
   *              connection is broken.
   * @exception JMSException  If the creation fails for any other reason.
   */
  public synchronized jakarta.jms.QueueSender createSender(jakarta.jms.Queue queue) throws JMSException {
    checkClosed();
    QueueSender qc = new QueueSender(this, (Destination) queue);
    addProducer(qc);
    return qc;
  }

  /**
   * API method.
   * Creates a QueueReceiver object to receive messages from the specified queue using a
   * message selector.
   * 
   * @param queue     the queue to access.
   * @param selector  The selector allowing to filter messages.
   * @return the created QueueReceiver object.
   *
   * @exception IllegalStateException  If the session is closed or if the
   *              connection is broken.
   * @exception JMSException  If the creation fails for any other reason.
   */
  public jakarta.jms.QueueReceiver createReceiver(jakarta.jms.Queue queue, String selector) throws JMSException {
    checkClosed();
    QueueReceiver qr = new QueueReceiver(this, (Destination) queue, selector);
    addConsumer(qr);
    return qr;
  }

  /**
   * API method.
   * Creates a QueueReceiver object to receive messages from the specified queue.
   * 
   * @param queue     the queue to access.
   * @return the created QueueReceiver object.
   *
   * @exception IllegalStateException  If the session is closed or if the
   *              connection is broken.
   * @exception JMSException  If the creation fails for any other reason.
   */
  public jakarta.jms.QueueReceiver createReceiver(jakarta.jms.Queue queue) throws JMSException {
    checkClosed(); 
    QueueReceiver qr = new QueueReceiver(this, (Destination) queue, null);
    addConsumer(qr);
    return qr;
  }

  /** 
   * API method.
   *
   * @exception IllegalStateException  Systematically.
   */
  public jakarta.jms.TopicSubscriber
         createDurableSubscriber(jakarta.jms.Topic topic, String name,
                                 String selector,
                                 boolean noLocal) throws JMSException {
    throw new IllegalStateException("Forbidden call on a QueueSession.");
  }

  /** 
   * API method.
   *
   * @exception IllegalStateException  Systematically.
   */
  public jakarta.jms.TopicSubscriber
         createDurableSubscriber(jakarta.jms.Topic topic, String name) throws JMSException{
    throw new IllegalStateException("Forbidden call on a QueueSession.");
  }

  /**
   * API method.
   *
   * @exception IllegalStateException  Systematically.
   */
  public jakarta.jms.Topic createTopic(String topicName) throws JMSException {
    throw new IllegalStateException("Forbidden call on a QueueSession.");
  }

  /**
   * API method.
   *
   * @exception IllegalStateException  Systematically.
   */
  public jakarta.jms.TemporaryTopic createTemporaryTopic() throws JMSException {
    throw new IllegalStateException("Forbidden call on a QueueSession.");
  }

  /**
   * API method.
   *
   * @exception IllegalStateException  Systematically.
   */
  public void unsubscribe(String name) throws JMSException {
    throw new IllegalStateException("Forbidden call on a QueueSession.");
  }
  

  /**
   * API 2.0 method.
   */
  public jakarta.jms.MessageConsumer createSharedConsumer(jakarta.jms.Topic topic,
                                                        String name) throws JMSException {
    throw new IllegalStateException("Forbidden call on a QueueSession.");
  }

  /**
   * API 2.0 method.
   */
  public jakarta.jms.MessageConsumer createSharedConsumer(jakarta.jms.Topic topic,
                                                        String name,
                                                        String selector) throws JMSException {
    throw new IllegalStateException("Forbidden call on a QueueSession.");
  }

  /**
   * API 2.0 method.
   */
  public jakarta.jms.MessageConsumer createDurableConsumer(jakarta.jms.Topic topic,
                                                         String name) throws JMSException {
    throw new IllegalStateException("Forbidden call on a QueueSession.");
  }
  
  /**
   * API 2.0 method.
   */
  public jakarta.jms.MessageConsumer createDurableConsumer(jakarta.jms.Topic topic,
                                                         String name,
                                                         String messageSelector,
                                                         boolean noLocal) throws JMSException {
    throw new IllegalStateException("Forbidden call on a QueueSession.");
  }

  /**
   * API 2.0 method.
   */
  public jakarta.jms.MessageConsumer createSharedDurableConsumer(jakarta.jms.Topic topic,
                                                               String name) throws JMSException {
    throw new IllegalStateException("Forbidden call on a QueueSession.");
  }

  /**
   * API 2.0 method.
   */
  public jakarta.jms.MessageConsumer createSharedDurableConsumer(jakarta.jms.Topic topic,
                                                               String name,
                                                               String selector) throws JMSException {
    throw new IllegalStateException("Forbidden call on a QueueSession.");
  }

}
