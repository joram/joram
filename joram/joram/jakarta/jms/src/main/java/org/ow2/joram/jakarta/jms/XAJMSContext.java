package org.ow2.joram.jakarta.jms;

import java.io.Serializable;

import jakarta.jms.TransactionInProgressRuntimeException;

public class XAJMSContext implements jakarta.jms.XAJMSContext {
  /** Embedded JMSContext object associated with this XAJMSContext. */
  private JMSContext context = null;
  /** The XA resource representing the session to the transaction manager. */
  private XAResource xaResource;

  /**
   * Creates a new Context using a newly created JMS connection.
   * 
   * @param connection  the created JMS connection.
   */
  public XAJMSContext(XAConnection cnx) {
    context = new JMSContext(cnx, Session.SESSION_TRANSACTED);
    xaResource = new XAResource(cnx.getXAResourceMngr(), context.getSession());
  }

  public jakarta.jms.JMSContext createContext(int sessionMode) {
    return context.createContext(sessionMode);
  }

  public jakarta.jms.JMSProducer createProducer() {
    return context.createProducer();
  }

  public String getClientID() {
    return context.getClientID();
  }

  public void setClientID(String clientID) {
    context.setClientID(clientID);
  }

  public jakarta.jms.ConnectionMetaData getMetaData() {
    return context.getMetaData();
  }

  public jakarta.jms.ExceptionListener getExceptionListener() {
    return context.getExceptionListener();
  }

  public void setExceptionListener(jakarta.jms.ExceptionListener listener) {
    context.setExceptionListener(listener);
  }

  public void start() {
    context.start();
  }

  public void stop() {
    context.stop();
  }

  public void setAutoStart(boolean autoStart) {
    context.setAutoStart(autoStart);
  }

  public boolean getAutoStart() {
    return context.getAutoStart();
  }

  public void close() {
    context.close();
  }

  public jakarta.jms.BytesMessage createBytesMessage() {
    return context.createBytesMessage();
  }

  public jakarta.jms.MapMessage createMapMessage() {
    return context.createMapMessage();
  }

  public jakarta.jms.Message createMessage() {
    return context.createMessage();
  }

  public jakarta.jms.ObjectMessage createObjectMessage() {
    return context.createObjectMessage();
  }

  public jakarta.jms.ObjectMessage createObjectMessage(Serializable object) {
    return context.createObjectMessage();
  }

  public jakarta.jms.StreamMessage createStreamMessage() {
    return context.createStreamMessage();
  }

  public jakarta.jms.TextMessage createTextMessage() {
    return context.createTextMessage();
  }

  public jakarta.jms.TextMessage createTextMessage(String text) {
    return context.createTextMessage();
  }

  public int getSessionMode() {
    return context.getSessionMode();
  }

  public void recover() {
    context.recover();
  }

  public jakarta.jms.JMSConsumer createConsumer(jakarta.jms.Destination destination) {
    return context.createConsumer(destination);
  }

  public jakarta.jms.JMSConsumer createConsumer(jakarta.jms.Destination destination,
                                              String selector) {
    return context.createConsumer(destination, selector);
  }

  public jakarta.jms.JMSConsumer createConsumer(jakarta.jms.Destination destination,
                                              String selector,
                                              boolean noLocal) {
    return context.createConsumer(destination, selector, noLocal);
  }

  public jakarta.jms.Queue createQueue(String name) {
    return context.createQueue(name);
  }

  public jakarta.jms.Topic createTopic(String name) {
    return context.createTopic(name);
  }

  public jakarta.jms.JMSConsumer createDurableConsumer(jakarta.jms.Topic topic, String name) {
    return context.createDurableConsumer(topic, name);
  }

  public jakarta.jms.JMSConsumer createDurableConsumer(jakarta.jms.Topic topic,
                                                     String name,
                                                     String selector,
                                                     boolean noLocal) {
    return context.createConsumer(topic, selector, noLocal);
  }

  public jakarta.jms.JMSConsumer createSharedDurableConsumer(jakarta.jms.Topic topic,
                                                           String name) {
    return context.createSharedDurableConsumer(topic, name);
  }

  public jakarta.jms.JMSConsumer createSharedDurableConsumer(jakarta.jms.Topic topic,
                                                           String name,
                                                           String selector) {
    return context.createSharedDurableConsumer(topic, name, selector);
  }

  public jakarta.jms.JMSConsumer createSharedConsumer(jakarta.jms.Topic topic,
                                                    String name) {
    return context.createSharedConsumer(topic, name);
  }

  public jakarta.jms.JMSConsumer createSharedConsumer(jakarta.jms.Topic topic,
                                                    String name,
                                                    String selector) {
    return context.createSharedConsumer(topic, name, selector);
  }

  public jakarta.jms.QueueBrowser createBrowser(jakarta.jms.Queue queue) {
    return context.createBrowser(queue);
  }

  public jakarta.jms.QueueBrowser createBrowser(jakarta.jms.Queue queue, String selector) {
    return context.createBrowser(queue, selector);
  }

  public jakarta.jms.TemporaryQueue createTemporaryQueue() {
    return context.createTemporaryQueue();
  }

  public jakarta.jms.TemporaryTopic createTemporaryTopic() {
    return context.createTemporaryTopic();
  }

  public void unsubscribe(String name) {
    context.unsubscribe(name);
  }

  public void acknowledge() {
    context.acknowledge();
  }

  public jakarta.jms.JMSContext getContext() {
    return context;
  }

  public XAResource getXAResource() {
    return xaResource;
  }

  public boolean getTransacted() {
    return true;
  }

  public void commit() {
    throw new TransactionInProgressRuntimeException("Unable to commit a XAJMSContext");
  }

  public void rollback() {
    throw new TransactionInProgressRuntimeException("Unable to rollback a XAJMSContext");
  }

}
