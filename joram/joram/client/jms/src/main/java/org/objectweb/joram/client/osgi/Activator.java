/*
 * Copyright (C) 2009 - 2023 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): ScalAgent Distributed Technologies
 * Contributor(s): 
 */
package org.objectweb.joram.client.osgi;

import javax.naming.spi.ObjectFactory;

import org.objectweb.joram.client.jms.admin.AdminModule;
import org.objectweb.joram.client.jms.admin.JoramAdminConnect;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import fr.dyade.aaa.common.Debug;

public class Activator implements BundleActivator {
  public static final Logger logmon = Debug.getLogger(Activator.class.getName());
  
  public static final String ADMIN_XML_PATH = "joram.adminXML";
  private JoramAdminConnect joramAdminConnect;

  @Override
  public void start(BundleContext context) throws Exception {
    if (logmon.isLoggable(BasicLevel.DEBUG))
      logmon.log(BasicLevel.DEBUG, "Activator.start(" + context + ')');
    
  	joramAdminConnect = new JoramAdminConnect();
  	joramAdminConnect.registerMBean();

  	ClassLoader myClassLoader = getClass().getClassLoader();
  	ClassLoader originalContextClassLoader = Thread.currentThread().getContextClassLoader();
  	try {
  	  Thread.currentThread().setContextClassLoader(myClassLoader);
  	  String adminXmlFile = context.getProperty(ADMIN_XML_PATH);
  	  if (adminXmlFile != null) {
  	    ServiceReference<ObjectFactory> ref = context.getServiceReference(javax.naming.spi.ObjectFactory.class);
  	    if (ref == null) {
  	      if (logmon.isLoggable(BasicLevel.ERROR))
  	        logmon.log(BasicLevel.ERROR, "No ServiceReference for javax.naming.spi.ObjectFactory, start jndi client bundle to use jndi.");
  	    }
  	    AdminModule.executeXMLAdmin(adminXmlFile);
  	  }
  	} finally {
  	  Thread.currentThread().setContextClassLoader(originalContextClassLoader);
  	}
  }

  @Override
  public void stop(BundleContext context) throws Exception {
    joramAdminConnect.unregisterMBean();
  }

}
