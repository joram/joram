/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C) 2020 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): ScalAgent Distributed Technologies
 */
package org.objectweb.joram.mom.proxies.tcp;

public interface SSLTcpProxyServiceMBean extends TcpProxyServiceMBean {
  /**
   * Returns the actual configuration for client authentication:<ul>
   * <li>WANT:client authentication required</li>
   * <li>NEED:client authentication requested</li>
   * <li>NONNEno client authentication desired</li>
   * </ul>
   * 
   * @return the actual configuration for client authentication.
   */
  public String getClientAuth() ;
  
  /**
   * Controls whether accepted server-mode SSLSockets will be initially configured to require or not client authentication.
   * A socket's client authentication setting is one of the following:<ul>
   * <li>WANT:client authentication required</li>
   * <li>NEED:client authentication requested</li>
   * <li>NONNEno client authentication desired</li>
   * </ul>
   * 
   * The initial value of this parameter depends of the org.objectweb.joram.clientAuth configuration property.
   * It can be overloaded for new connections  using the corresponding MBean.
   * 
   * @param clientAuth  "WANT", "NEED" or "NONE".
   */
  public void setClientAuth(String clientAuth);
}
