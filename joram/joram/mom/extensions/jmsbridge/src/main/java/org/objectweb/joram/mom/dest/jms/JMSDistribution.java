/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C) 2011 - 2023 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): ScalAgent Distributed Technologies
 * Contributor(s): 
 */
package org.objectweb.joram.mom.dest.jms;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import jakarta.jms.Connection;
import jakarta.jms.Destination;
import jakarta.jms.JMSException;
import jakarta.jms.MessageProducer;
import jakarta.jms.Session;

import org.objectweb.joram.mom.dest.DistributionHandler;
import org.objectweb.joram.shared.messages.Message;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

import fr.dyade.aaa.common.Debug;

/**
 * Distribution handler for the JMS distribution bridge.
 */
public class JMSDistribution implements DistributionHandler {

  private static final Logger logger = Debug.getLogger(JMSDistribution.class.getName());

  private static final String DESTINATION_NAME_PROP = "jms.DestinationName";

  private static final String UPDATE_PERIOD_PROP = "jms.ConnectionUpdatePeriod";

  private static final String ROUTING_PROP = "jms.Routing";

  // LRU (Least Recently Used) Map.
  private LinkedHashMap<String, SessionAndProducer> sessions = new LinkedHashMap<String, SessionAndProducer>(
      16, 0.75f, true);

  private List<String> connectionNames = null;

  /** Destination JNDI name. */
  private String destName;

  private long lastUpdate = 0;
  
  private long updatePeriod = 5000L;

  public void init(Properties properties, boolean firstTime) {
    destName = properties.getProperty(DESTINATION_NAME_PROP);
    if (destName == null) {
      throw new IllegalArgumentException("Missing Destination JNDI name.");
    }
    try {
      if (properties.containsKey(UPDATE_PERIOD_PROP)) {
        updatePeriod = Long.parseLong(properties.getProperty(UPDATE_PERIOD_PROP));
      }
    } catch (NumberFormatException nfe) {
      logger.log(BasicLevel.ERROR,
                 "Property " + UPDATE_PERIOD_PROP + "could not be parsed properly, use default value.", nfe);
    }
    if (properties.containsKey(ROUTING_PROP)) {
      connectionNames = JMSConnectionService.convertToList(properties.getProperty(ROUTING_PROP));
    }
  }

  long lastwarn = 0;
  
  public void distribute(Message message) throws Exception {
  	if (logger.isLoggable(BasicLevel.DEBUG)) {
      logger.log(BasicLevel.DEBUG, "JMSDistribution.distribute(" + message + ')');
  	}
  	
    List<String> connectionNames = this.connectionNames;
    if (message.properties != null) {
      Object customRouting = message.properties.get(ROUTING_PROP);
      if (customRouting != null && customRouting instanceof String) {
        connectionNames = JMSConnectionService.convertToList((String) customRouting);
      }
    }
    if (logger.isLoggable(BasicLevel.DEBUG)) {
      logger.log(BasicLevel.DEBUG, "JMSDistribution.distribute: connectionNames = " + connectionNames);
    }
      
    // Update sessions if necessary
    long now = System.currentTimeMillis();
    if (now - lastUpdate > updatePeriod) {
      if (logger.isLoggable(BasicLevel.DEBUG)) {
        logger.log(BasicLevel.DEBUG, "Updating sessions.");
      }
      List<JMSModule> connections = JMSConnectionService.getInstance().getConnections();
      if (logger.isLoggable(BasicLevel.DEBUG)) {
        logger.log(BasicLevel.DEBUG, "JMSDistribution.distribute: connections=" + connections.size());
      } else if (logger.isLoggable(BasicLevel.INFO) && connections.isEmpty() && ((now - lastwarn) > 60000L)) {
        logger.log(BasicLevel.INFO, "JMSDistribution.distribute: no available connection");
        lastwarn = now;
      }
            
      for (final JMSModule connection : connections) {
        SessionAndProducer sap = sessions.get(connection.getName());
        if (sap != null) {
          // Verify that the connection still valid
          if (sap.connection != connection.getCnx())  {
            if (logger.isLoggable(BasicLevel.INFO))
              logger.log(BasicLevel.INFO,
                         "JMSDistribution.distribute: remove outdated connection " + connection.getName());
            sessions.remove(connection.getName());
            sap = null;
          }
        }
        
        if (sap == null) { // !sessions.containsKey(connection.getCnxFactName()))
          if (logger.isLoggable(BasicLevel.INFO))
            logger.log(BasicLevel.INFO,
                       "JMSDistribution.distribute: Creates new connection for distribution, cf = " + connection.getName());
          // /!\ Be careful, do not close session, or producer. They are use through the SessionAndProducer
          // object beyond this method call.
          try {
            Session session = connection.getCnx().createSession(false, Session.AUTO_ACKNOWLEDGE);
            Destination dest = (Destination) connection.retrieveJndiObject(destName);
            MessageProducer producer = session.createProducer(dest);
            sessions.put(connection.getName(), new SessionAndProducer(connection.getCnx(), session, producer));
            if (logger.isLoggable(BasicLevel.INFO))
              logger.log(BasicLevel.INFO, "JMSDistribution.distribute: New connection available.");
          } catch (Exception exc) {
            if (logger.isLoggable(BasicLevel.INFO)) {
              logger.log(BasicLevel.INFO, "Connection still not usable.", exc);
            }
          }
        }
      }
      lastUpdate = now;
    }

    // Send the message
    Iterator<Map.Entry<String, SessionAndProducer>> iter = sessions.entrySet().iterator();
    if (logger.isLoggable(BasicLevel.DEBUG))
      logger.log(BasicLevel.DEBUG, "Try to send message, sessions available: " + sessions.size());
    while (iter.hasNext()) {
      Map.Entry<String, SessionAndProducer> entry = iter.next();
      try {
        SessionAndProducer session = entry.getValue();
        String cnxName = entry.getKey();
        if (logger.isLoggable(BasicLevel.DEBUG))
          logger.log(BasicLevel.DEBUG, "Try session: " + cnxName);
        if (connectionNames != null && !connectionNames.contains(cnxName)) {
          continue;
        }
        if (logger.isLoggable(BasicLevel.DEBUG)) {
          logger.log(BasicLevel.DEBUG, "Sending message using " + cnxName);
        }

        // With a Joram session we need to convert the message because it is modified on session send, and if
        // we have an exception we must keep the original message.
        // JORAM-308: It seems that the original message could also be modified by the provider even if this one is 
        // not Joram. So we have always to create a new message from the target session and initialize it from the
        // original message.
       	session.producer.send(org.ow2.joram.jakarta.jms.Message.convertJMSMessage(org.ow2.joram.jakarta.jms.Message.wrapMomMessage(null, message)));

        // TODO (AF): /!\ This call below has no explicit effect!!
        sessions.get(cnxName); // Access the used connection to update the LRU map
        return;
      } catch (JMSException exc) {
        if (logger.isLoggable(BasicLevel.WARN)) {
          logger.log(BasicLevel.WARN, "Session is not usable, remove from table.", exc);
        }
        // TODO (AF): We should indicate that the corresponding connection is no longer
        // available calling JMSModule.onException().
        iter.remove();
      }
    }

    if (logger.isLoggable(BasicLevel.DEBUG))
      logger.log(BasicLevel.DEBUG,
                 "Message could not be sent, no usable connection/session found - " + sessions.size());

    throw new Exception("Message could not be sent, no usable connection/session found - " + sessions.size());
  }

  /**
   * Closes the JMS distribution module and all associated resources.
   * Called by generic behavior during the destination finalization.
   */
  @Override
  public void close() {
    for (SessionAndProducer session : sessions.values()) {
      try {
        session.producer.close();
      } catch (JMSException exc) {
        if (logger.isLoggable(BasicLevel.DEBUG)) {
          logger.log(BasicLevel.DEBUG, "Error while stopping JmsDistribution.", exc);
        }
      }
      try {
        session.session.close();
      } catch (JMSException exc) {
        if (logger.isLoggable(BasicLevel.DEBUG)) {
          logger.log(BasicLevel.DEBUG, "Error while stopping JmsDistribution.", exc);
        }
      }
    }
    sessions.clear();
  }
  
  private class SessionAndProducer {
    Connection connection;
    Session session;
    MessageProducer producer;

    public SessionAndProducer(Connection connection, Session session, MessageProducer producer) {
      super();
      this.connection = connection;
      this.session = session;
      this.producer = producer;
    }
    
    public boolean isJoramSession() {
    	return (session instanceof org.ow2.joram.jakarta.jms.Session);
    }
  }

}
