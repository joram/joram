/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C) 2023 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): ScalAgent Distributed Technologies
 * Contributor(s): 
 */
package com.scalagent.joram.mom.dest.rest;

/**
 * This exception must be thrown when an issue occurs during message decoding.
 */
public final class MessageFormatException extends Exception {
  /**
   * Explicitly set serialVersionUID.
   */
  private static final long serialVersionUID = 1;

  /**
   * {@code Exception} reference.
   **/
  private Exception linkedException;

  /**
   * Constructs a {@code MessageFormatException} with the specified reason. The error code defaults to null.
   *
   * @param reason a description of the exception
   **/
  public MessageFormatException(String reason) {
      super(reason);
      linkedException = null;
  }
  
  /**
   * Gets the exception linked to this one.
   *
   * @return the linked {@code Exception}, null if none
   **/
  public Exception getLinkedException() {
      return linkedException;
  }

  /**
   * Adds a linked {@code Exception}.
   *
   * @param ex the linked {@code Exception}
   **/
  public void setLinkedException(Exception exc) {
      linkedException = exc;
  }
}
