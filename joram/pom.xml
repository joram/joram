<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <groupId>org.ow2.joram</groupId>
  <artifactId>parent</artifactId>
  <packaging>pom</packaging>
  <!-- version : major.minor.build -->
  <version>5.23.0-SNAPSHOT</version>
  <name>JORAM</name>
  <url>https://joram.ow2.io/</url>
  <description>Builds the Joram project.</description>
 
  <parent>
    <groupId>org.ow2</groupId>
    <artifactId>ow2</artifactId>
    <version>3.0.0</version>
  </parent>
  
  <modules>
    <module>a3</module>
    <module>jndi</module>
    <module>joram</module>
    <module>licenses</module>
    <module>conf</module>
    <module>assembly</module>
    <module>samples</module>
    <module>shell</module>
  </modules>

  <profiles>
    <!-- Profile release -->
    <profile>
      <id>release</id>
      <modules>
        <module>release</module>
      </modules>
      <build>
        <plugins>
          <!--  Attach the source to the project      -->
          <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-source-plugin</artifactId>
            <version>${maven-source-plugin.version}</version>
            <executions>
              <execution>
                <id>attach-sources</id>
                <goals>
                  <goal>jar-no-fork</goal>
                </goals>
              </execution>
            </executions>
          </plugin>

          <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-release-plugin</artifactId>
            <version>${maven-release-plugin.version}</version>
            <configuration>
              <tagBase>svn+ssh://svn.forge.objectweb.org/svnroot/joram/tags</tagBase>
              <autoVersionSubmodules>true</autoVersionSubmodules>
              <addSchema>false</addSchema>
            </configuration>
          </plugin>
        </plugins>
      </build>
    </profile>
    
    <!-- Profile sign -->
    <profile>
      <id>sign</id>
      <modules>
        <module>release</module>
      </modules>
      <build>
        <plugins>
          <!-- - - - - - - - - - - - - - - - - - - - - -->
          <!--  Sign all the artifacts                 -->
          <!-- - - - - - - - - - - - - - - - - - - - - -->
          <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-gpg-plugin</artifactId>
            <version>${maven-gpg-plugin.version}</version>
            <executions>
              <execution>
                <id>sign-artifacts</id>
                <phase>verify</phase>
                <goals>
                  <goal>sign</goal>
                </goals>
              </execution>
            </executions>
          </plugin>
        </plugins>
      </build>
    </profile>
    
    <!-- Profile javadoc -->
    <profile>
      <id>doc</id>
      <properties>
        <javadoc.opts>-Xdoclint:none</javadoc.opts>
      </properties>
      <build>
        <plugins>
        
          <!--  Attach the javadoc to the project      -->
          <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-javadoc-plugin</artifactId>
            <version>${maven-javadoc-plugin.version}</version>
            <configuration>
              <failOnError>false</failOnError>
              <show>private</show>
              <nohelp>true</nohelp>
              <links>
                <link>https://javaee.github.io/javaee-spec/javadocs/</link>
              </links>
            </configuration>
            <executions>
              <execution>
                <id>attach-javadocs</id>
                <phase>package</phase>
                <goals>
                  <goal>jar</goal>
                  <goal>aggregate-jar</goal>
                </goals>

                <configuration>
                  <additionalparam>${javadoc.opts}</additionalparam>
                </configuration>
              </execution>
            </executions>
          </plugin>
        </plugins>
      </build>
      
      <!-- reporting -->
      <reporting>
        <plugins>
          <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-javadoc-plugin</artifactId>
            <configuration>
              <!-- <stylesheetfile>${basedir}/src/main/javadoc/stylesheet.css</stylesheetfile> -->
              <show>private</show>
            </configuration>
          </plugin>
        </plugins>
      </reporting>
    </profile>

    <profile>
      <id>owasp-dependency-check</id>
      <build>
        <!-- Create the dependency-check-report.html in the target directory using verify target -->
        <plugins>
          <plugin>
            <groupId>org.owasp</groupId>
            <artifactId>dependency-check-maven</artifactId>
            <version>8.1.0</version>
            <executions>
              <execution>
                <goals>
                  <goal>check</goal>
                </goals>
              </execution>
            </executions>
          </plugin>
        </plugins>
      </build>
      <reporting>
        <!-- Create the dependency-check-report.html in the target/site directory using site target -->
        <plugins>
          <plugin>
            <groupId>org.owasp</groupId>
            <artifactId>dependency-check-maven</artifactId>
            <version>8.1.0</version>
            <reportSets>
              <reportSet>
                <reports>
                  <report>aggregate</report>
                </reports>
              </reportSet>
            </reportSets>
          </plugin>
        </plugins>
      </reporting>
    </profile>
  </profiles>

  <!-- Source code management -->
  
  <build>
    <extensions>
      <extension>
        <groupId>org.apache.maven.wagon</groupId>
        <artifactId>wagon-ssh</artifactId>
        <version>1.0</version>
      </extension>
    </extensions>

    <!-- Plugins version to be used -->
    <pluginManagement>
      <plugins>
        <!--This plugin's configuration is used to store Eclipse m2e settings only. It has no influence on the Maven build itself.-->
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-assembly-plugin</artifactId>
          <version>${maven-assembly-plugin.version}</version>
        </plugin>
      </plugins>
    </pluginManagement>

  <plugins>
    <plugin>
      <groupId>org.apache.maven.plugins</groupId>
      <artifactId>maven-compiler-plugin</artifactId>
      <version>${maven-compiler-plugin.version}</version>
      <configuration>
        <!-- Needs JDK 11 -->
        <source>11</source>
        <target>11</target>
        <release>11</release>
      </configuration>
      <dependencies>
        <dependency>
          <groupId>org.ow2.asm</groupId>
          <artifactId>asm</artifactId>
          <version>6.2</version> <!-- Use newer version of ASM -->
        </dependency>
      </dependencies>
    </plugin>

    <plugin>
      <groupId>org.apache.maven.plugins</groupId>
      <artifactId>maven-surefire-plugin</artifactId>
      <version>${maven-surefire-plugin.version}</version>
    </plugin>

  </plugins>
  </build>
  
  <properties>
    <!-- joram protocol -->
    <joram.protocol>59</joram.protocol>
    
    <!--  dependencies versions -->
    
    <jcup.version>5.3.1</jcup.version>
    <jftp.version>1.52</jftp.version>
    <jakarta.jms-api.version>3.0.0</jakarta.jms-api.version>
    <jakarta.resource-api.version>2.0.0</jakarta.resource-api.version>
    <jakarta.transaction-api.version>2.0.0</jakarta.transaction-api.version>
    <ow2-jms-2.0-spec.version>1.0.0</ow2-jms-2.0-spec.version>
    <ow2-jta-1.1-spec.version>1.0.13</ow2-jta-1.1-spec.version>
    
    <!--  OSGi specifications -->
    <osgi.core.version>8.0.0</osgi.core.version>
    <osgi.cmpn.version>7.0.0</osgi.cmpn.version>
    
    <!-- Only used with Mail destinations -->
    <activation.version>1.1.1</activation.version>
    <javax.mail-api.version>1.6.2</javax.mail-api.version>
    
    <connector.version>1.0.13</connector.version>
    <felix.main.version>7.0.5</felix.main.version>
    <!-- Needed by Jolokia -->
    <felix.log.version>1.3.0</felix.log.version>
    <!-- These 3 components are outdated, they should be replaced by gogo -->
    <felix.shell.version>1.4.3</felix.shell.version>
    <felix.remote.version>1.2.0</felix.remote.version>
    <felix.tui.version>1.4.1</felix.tui.version>
    <!-- Gogo Shell -->
    <felix.gogo.runtime.version>1.1.6</felix.gogo.runtime.version>
    <felix.gogo.command.version>1.1.2</felix.gogo.command.version>
    <felix.gogo.shell.version>1.1.4</felix.gogo.shell.version>
    
    <!-- Most plugin version are inherited from org.ow2:ow2:3.0.0 -->
    <maven-surefire-report-plugin.version>${maven-surefire-plugin.version}</maven-surefire-report-plugin.version>
    
    <maven.bundle.plugin.version>5.1.9</maven.bundle.plugin.version>
    <maven-war-plugin.version>3.3.2</maven-war-plugin.version>
    <exec-maven-plugin.version>3.3.0</exec-maven-plugin.version>
    <maven-antrun-plugin.version>3.1.0</maven-antrun-plugin.version>
    <maven-jxr-plugin.version>3.4.0</maven-jxr-plugin.version>
    <cobertura-maven-plugin.version>2.7</cobertura-maven-plugin.version>
    <!-- Use in JMQ tests -->
    <com.google.code.maven-replacer-plugin.version>1.5.3</com.google.code.maven-replacer-plugin.version>
 
    <!-- to prevent [WARNING] Using platform encoding -->
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    
    <stomp.version>1.0</stomp.version>
    <commons-io.version>2.16.1</commons-io.version>
    <ow2-util-substitution.version>2.0.0</ow2-util-substitution.version>

    <junit.version>4.13.1</junit.version>
    <junit.jupiter.version>5.0.0</junit.jupiter.version>
    <junit.vintage.version>${junit.version}.0</junit.vintage.version>
    <junit.platform.version>1.0.0</junit.platform.version>
    
    <slf4j.version>1.7.36</slf4j.version>

    <!-- 3.1.3 => Jakarta EE10 compatible -->
    <jersey.version>3.1.8</jersey.version>
    <!-- Jetty (already included in Felix HTTP service) -->
    <jetty.version>11.0.24</jetty.version>
    <felix.http.servlet-api.version>3.0.0</felix.http.servlet-api.version> 
    <!-- Felix HttpService implementation based on Jetty (includes Jetty 11.0.22) -->
    <felix.http.jetty.version>5.1.26</felix.http.jetty.version>
    <gson.version>2.11.0</gson.version>
    <jakarta.inject.version>2.6.1</jakarta.inject.version>
    <jakarta.activation.version>2.1.3</jakarta.activation.version>
    
    <!--  Dependencies for JBoss naming (needed by jmstool and clientjms tools for eFluid) -->
    <jboss-logging.version>3.3.3.Final</jboss-logging.version>
    <org.jboss.naming.version>5.0.5.Final</org.jboss.naming.version>
  </properties>

  <organization>
    <name>ScalAgent D.T.</name>
    <url>https://joram.ow2.io/</url>
  </organization>

 <!-- Who worked on this project ? -->
  <developers>
    <developer>
      <id>afreyssin</id>
      <name>Andre Freyssinet</name>
      <email>Andre.Freyssinet@scalagent.com</email>
    </developer>
    <developer>
      <id>tachker</id>
      <name>Nicolas Tachker</name>
      <email>nicolas.tachker@scalagent.com</email>
    </developer>
    <developer>
      <id>lacourte</id>
      <name>Serge Lacourte</name>
      <email>serge.lacourte@scalagent.com</email>
    </developer>
  </developers>
  
  <!-- Preferred dependencies version -->
  <dependencyManagement>
    <dependencies>
      <dependency>
        <groupId>org.osgi</groupId>
        <artifactId>osgi.core</artifactId>
        <version>${osgi.core.version}</version>
        <type>jar</type>
      </dependency>
      <dependency>
        <groupId>org.osgi</groupId>
        <artifactId>osgi.cmpn</artifactId>
        <version>${osgi.cmpn.version}</version>
        <type>jar</type>
      </dependency>
      
      <dependency>
        <groupId>org.objectweb.joram</groupId>
        <artifactId>jcup</artifactId>
        <version>${jcup.version}</version>
        <type>jar</type>
      </dependency>
      
      <dependency>
      	<groupId>javax.mail</groupId>
      	<artifactId>javax.mail-api</artifactId>
      	<version>${javax.mail-api.version}</version>
   	  </dependency>
      <dependency>
        <groupId>javax.activation</groupId>
        <artifactId>activation</artifactId>
        <version>${activation.version}</version>
        <type>jar</type>
      </dependency>
      
	  <!-- Not needed, Jakarta JMS uses javax.transaction API
      <dependency>
        <groupId>jakarta.transaction</groupId>
        <artifactId>jakarta.transaction-api</artifactId>
        <version>${jakarta.transaction-api.version}</version>
      </dependency> -->
      <dependency>
        <groupId>jakarta.resource</groupId>
        <artifactId>jakarta.resource-api</artifactId>
        <version>${jakarta.resource-api.version}</version>
        <!-- <version>2.0.0</version> -->
      </dependency>
      <dependency>
        <groupId>jakarta.jms</groupId>
        <artifactId>jakarta.jms-api</artifactId>
        <version>${jakarta.jms-api.version}</version>
      </dependency>
      <dependency>
        <groupId>org.ow2.spec.ee</groupId>
        <artifactId>ow2-jms-2.0-spec</artifactId>
        <version>${ow2-jms-2.0-spec.version}</version>
      </dependency>
      <dependency>
        <groupId>org.ow2.spec.ee</groupId>
        <artifactId>ow2-jta-1.1-spec</artifactId>
        <version>${ow2-jta-1.1-spec.version}</version>
      </dependency>
      <dependency>
        <groupId>org.ow2.spec.ee</groupId>
        <artifactId>ow2-connector-1.5-spec</artifactId>
        <version>${connector.version}</version>
      </dependency>
      <dependency>
        <groupId>org.objectweb.joram</groupId>
        <artifactId>jftp</artifactId>
        <version>${jftp.version}</version>
        <type>jar</type>
      </dependency>
      <dependency>
        <groupId>org.codehaus.stomp</groupId>
        <artifactId>stompconnect</artifactId>
        <version>${stomp.version}</version>
        <type>jar</type>
      </dependency>
      <!-- Needed by AMQP service joram-mom-extensions-amqp at execution (needs to be in bundle list) -->
      <dependency>
        <groupId>commons-io</groupId>
        <artifactId>commons-io</artifactId>
        <version>${commons-io.version}</version>
      </dependency>
      <dependency>
        <groupId>org.ow2.util.substitution</groupId>
        <artifactId>substitution</artifactId>
        <version>${ow2-util-substitution.version}</version>
      </dependency>
      <!-- inherited from OW2 parent, To remove?
      <dependency>
        <groupId>junit</groupId>
        <artifactId>junit</artifactId>
        <version>${junit.version}</version>
      </dependency> -->
    	<dependency>
		    <groupId>org.junit.jupiter</groupId>
  		  <artifactId>junit-jupiter-api</artifactId>
	   	  <version>5.5.2</version>
		    <scope>test</scope>
	    </dependency>

      <!--  Replaces javax.servlet API (geronimo-servlet_3.0_spec). This bundle is explicitly needed by Felix HttpService component -->	    
	    <dependency>
        <groupId>org.apache.felix</groupId>
        <artifactId>org.apache.felix.http.servlet-api</artifactId>
        <version>${felix.http.servlet-api.version}</version>
      </dependency>

      <!-- https://mvnrepository.com/artifact/javax.xml.bind/jaxb-api -->
      <dependency>
      <groupId>javax.xml.bind</groupId>
      <artifactId>jaxb-api</artifactId>
      <version>2.3.1</version>
      </dependency>
      
      <!-- https://mvnrepository.com/artifact/org.slf4j/slf4j-api -->
      <dependency>
        <groupId>org.slf4j</groupId>
        <artifactId>slf4j-api</artifactId>
        <version>${slf4j.version}</version>
      </dependency>
      <dependency>
        <groupId>org.slf4j</groupId>
        <artifactId>slf4j-simple</artifactId>
        <version>${slf4j.version}</version>
      </dependency>
	    
      <!-- Jersey -->
      <dependency>
        <groupId>org.glassfish.jersey.core</groupId>
        <artifactId>jersey-server</artifactId>
        <version>${jersey.version}</version>
      </dependency>
      <dependency>
        <groupId>org.glassfish.jersey.containers</groupId>
        <artifactId>jersey-container-jdk-http</artifactId>
        <version>${jersey.version}</version>
      </dependency>
    	<dependency>
    		<groupId>org.glassfish.jersey.containers</groupId>
    		<artifactId>jersey-container-servlet-core</artifactId>
    		<version>${jersey.version}</version>
	    </dependency>
	    <dependency>
	    	<groupId>org.glassfish.jersey.core</groupId>
	    	<artifactId>jersey-common</artifactId>
	    	<version>${jersey.version}</version>
	    </dependency>
	    <dependency>
        <groupId>org.glassfish.jersey.core</groupId>
        <artifactId>jersey-client</artifactId>
        <version>${jersey.version}</version>
      </dependency>
	    <!-- Needed? -->
	    <dependency>
	    	<groupId>org.glassfish.jersey.inject</groupId>
	    	<artifactId>jersey-hk2</artifactId>
	    	<version>${jersey.version}</version>
	    </dependency>
      <dependency>
        <groupId>org.glassfish.hk2.external</groupId>
       	<artifactId>jakarta.inject</artifactId>
        <version>${jakarta.inject.version}</version>
      </dependency>
      <dependency>
      <groupId>jakarta.activation</groupId>
        <artifactId>jakarta.activation-api</artifactId>
        <version>${jakarta.activation.version}</version>
      </dependency>
      
      <!-- Jetty (already included in Felix HTTP service) -->
      <dependency>
        <groupId>org.eclipse.jetty</groupId>
        <artifactId>jetty-http</artifactId>
        <version>${jetty.version}</version>
      </dependency>
      <!-- No longer existing in Jetty 10+, use Servlet 3.0 APIs if needed.
           https://github.com/eclipse/jetty.project/issues/4934
      <dependency>
        <groupId>org.eclipse.jetty</groupId>
        <artifactId>jetty-continuation</artifactId>
        <version>${jetty.version}</version>
      </dependency> -->
      <dependency>
        <groupId>org.eclipse.jetty</groupId>
        <artifactId>jetty-io</artifactId>
        <version>${jetty.version}</version>
      </dependency>
      <dependency>
        <groupId>org.eclipse.jetty</groupId>
        <artifactId>jetty-util</artifactId>
        <version>${jetty.version}</version>
      </dependency>
      <dependency>
        <groupId>org.eclipse.jetty</groupId>
        <artifactId>jetty-util-ajax</artifactId>
        <version>${jetty.version}</version>
      </dependency>
      <dependency>
        <groupId>org.eclipse.jetty</groupId>
        <artifactId>jetty-security</artifactId>
        <version>${jetty.version}</version>
      </dependency>
      <dependency>
        <groupId>org.eclipse.jetty</groupId>
        <artifactId>jetty-server</artifactId>
        <version>${jetty.version}</version>
      </dependency>
    	<dependency>
    		<groupId>org.eclipse.jetty</groupId>
    		<artifactId>jetty-servlet</artifactId>
    		<version>${jetty.version}</version>
    	</dependency>
    	
      <!-- Felix HttpService implementation based on Jetty (includes Jetty) -->
      <dependency>
        <groupId>org.apache.felix</groupId>
        <artifactId>org.apache.felix.http.jetty</artifactId>
        <version>${felix.http.jetty.version}</version>
      </dependency>
      
    	<dependency>
        <groupId>com.google.code.gson</groupId>
        <artifactId>gson</artifactId>
        <version>${gson.version}</version>
      </dependency>
      
      <!-- Felix main, log and shell -->
      <dependency>
        <groupId>org.apache.felix</groupId>
        <artifactId>org.apache.felix.main</artifactId>
        <version>${felix.main.version}</version>
      </dependency>
      <dependency>
        <groupId>org.apache.felix</groupId>
        <artifactId>org.apache.felix.log</artifactId>
        <version>${felix.log.version}</version>
      </dependency>
      <dependency>
        <groupId>org.apache.felix</groupId>
        <artifactId>org.apache.felix.shell</artifactId>
        <version>${felix.shell.version}</version>
      </dependency>
      <dependency>
        <groupId>org.apache.felix</groupId>
        <artifactId>org.apache.felix.shell.remote</artifactId>
        <version>${felix.remote.version}</version>
      </dependency>
      <dependency>
        <groupId>org.apache.felix</groupId>
        <artifactId>org.apache.felix.shell.tui</artifactId>
        <version>${felix.tui.version}</version>
      </dependency>
      
      <!-- Felix Gogo Shell -->
      <dependency>
        <groupId>org.apache.felix</groupId>
        <artifactId>org.apache.felix.gogo.runtime</artifactId>
        <version>${felix.gogo.runtime.version}</version>
      </dependency>
      <dependency>
        <groupId>org.apache.felix</groupId>
        <artifactId>org.apache.felix.gogo.command</artifactId>
        <version>${felix.gogo.command.version}</version>
      </dependency>
      <dependency>
        <groupId>org.apache.felix</groupId>
        <artifactId>org.apache.felix.gogo.shell</artifactId>
        <version>${felix.gogo.shell.version}</version>
      </dependency>
    </dependencies>
  </dependencyManagement>

  <dependencies>
    <dependency>
      <groupId>org.osgi</groupId>
      <artifactId>osgi.core</artifactId>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>org.osgi</groupId>
      <artifactId>osgi.cmpn</artifactId>
      <scope>provided</scope>
    </dependency>
  </dependencies>
</project>
