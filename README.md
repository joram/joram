JORAM is an open source Java implementation of JMS (Java Message Service) API specification (compliance with Java 11 to 21 and JMS 1.1, 2.0 and 3.0).

JORAM is a mature project started in 1999, it is released under the LGPL license since May 2000.
It provides a production ready distributed MOM already used within many critical operational applications.